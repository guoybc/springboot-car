

# 技术介绍

| 工具/技术       | 版本(version) | 作用         |
| --------------- | ------------- | ------------ |
| jdk             | 1.8           |              |
| IDEA            | 2019.3.3      |              |
| MYSQL           | 5.7           |              |
| SSM             | 整合版本      |              |
| Springboot      | 2.2.9.RELEASE |              |
| Bootstrap       | 2             | 前端         |
| Jquery          | 忽略          | 操作Dom      |
| redis           | 忽略          | 缓存         |
| themlef         | 忽略          | 模板引擎     |
| ajaxfileupload1 | 忽略          | 文件上传     |
| layer           | 忽略          | 弹窗显示图片 |

# 项目介绍

​	项目的主要目的是将会员洗车进行统计，将数据进行有序化。

# 数据库

<img src="https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316202625830.png" alt="image-20210316202625830" style="zoom:150%;" />

# 1.字典数据

## 1.1会员登记



![image-20210309133533114](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309133533114.png)

## 1.2产品类别设置

![image-20210316193249791](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193249791.png)

## 1.3服务类型设计

![image-20210316193316551](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193316551.png)

## 1.4制定客户凭证

![image-20210316193347402](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193347402.png)

## 1.5制定汽车品牌

用来记录VIP汽车品牌的数据字典

![image-20210316193428489](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193428489.png)

## 1.6定制汽车系列

![image-20210316193448821](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193448821.png)

## 1.7优惠活动

优惠活动进行充值返现的操作，将返现的金额记录到数据库对应VIP上面

![image-20210316193507938](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193507938.png)

### 1.7.1增加优惠活动

![image-20210316193621797](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193621797.png)

## 1.8操作设置

管理管理员用户

![image-20210316193645081](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193645081.png)

### 1.8.1增加管理员

![image-20210316193810099](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193810099.png)

### 1.8.2修改管理员信息

![image-20210316193912844](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316193912844.png)

# 2.会员管理

## 2.1管理会员信息

分为3个状态：`正常` 和 `失效`，失效用户将不会再有优惠政策

### 2.1.1 查看用户图片信息

![image-20210309133751463](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309133751463.png)



![image-20210309133641559](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309133641559.png)

### 2.1.2 增加VIP用户

![image-20210316194140906](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316194140906.png)

### 2.1.3 修改用户信息

前端点击当前用户，然后通过Ajax传输这个对应的id给后端回显回来一个json数据，最后通过Jquery里面的val（）方法将数据填充到页面

![image-20210316194222862](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316194222862.png)

## 2.2会员充值

会员充值记录

![image-20210316195915142](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316195915142.png)

## 2.3会员账号充值

输入卡号进行查询会员信息

![image-20210316200002857](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200002857.png)

查询客户的信息`不可修改`

![image-20210316200106373](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200106373.png)

![image-20210316200159809](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200159809.png)

选择优惠活动，然后输入充值金额最后上面会显示赠送的金额，然后会显示最终金额

![image-20210316200318684](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200318684.png)



# 3.库存管理

显示仓库里面数据

![image-20210309134025619](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134025619.png)

## 1、增加商品库存

![image-20210309134035268](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134035268.png)

## 2、修改产品信息

只能修改 产品名称、产品类型、产品单位、产品图片

![image-20210309134053526](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134053526.png)

## 3、库存管理

显示出库和入库信息

![image-20210316200624206](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200624206.png)

### 3.1入库信息

显示当前产品进货情况

![image-20210316200740204](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200740204.png)

#### 3.1.1 增加入库

输入入库数量

![image-20210316200828097](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200828097.png)

### 3.2出库信息

出售商品给会员的记录

![image-20210316200924977](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316200924977.png)

出售商品给散客的记录

![image-20210316201018740](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316201018740.png)

# 4.消费管理

## 1、会员消费管理

输入卡号显示会员信息和会员购买的商品

![image-20210309134212875](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134212875.png)

显示下面的个人信息和需要购买的商品

![image-20210316201143560](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316201143560.png)

### 1.1 结账

显示库存数量，然后判断库存是否充足

提交订单后会自动扣仓库中的数据

![image-20210316201245842](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316201245842.png)



## 2、散客消费管理

非VIP用户的消费情况

![image-20210309134345305](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134345305.png)

### 2.1 购买商品

![image-20210316201438309](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316201438309.png)

## 3、计次消费管理

做消费数据统计，主要是统计给会员服务的营收金额

![image-20210309134445697](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134445697.png)

### 3.1购买商品

自动聚焦到 消费卡号-》输入卡号

![image-20210316201545665](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316201545665.png)

如果卡号存在就显示信息

![image-20210316201639449](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210316201639449.png)

# 5.礼品兑换

## 1、礼品信息

也相当于个一个字典表，选哟对应的积分来兑换

![image-20210309134545540](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134545540.png)

## 2、积分兑换

![image-20210309134631902](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134631902.png)

如果没有输入消费卡号会进行判断

![image-20210309134714456](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134714456.png)

# 6.图形报表

显示客户登记所占比

下面的 tab 进行等级切换 submenu里面的 li 绑定对应的 id 进行 传输给后台，后台回显给前端

![image-20210309134741242](https://gitee.com/wyxhunk/blog-img/raw/master/img/image-20210309134741242.png)