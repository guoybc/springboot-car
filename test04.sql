/*
 Navicat Premium Data Transfer

 Source Server         : 笑哥
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : test04

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 09/03/2021 12:27:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cartype
-- ----------------------------
DROP TABLE IF EXISTS `cartype`;
CREATE TABLE `cartype`  (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `aname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`aid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cartype
-- ----------------------------
INSERT INTO `cartype` VALUES (1, '宝马');
INSERT INTO `cartype` VALUES (2, '奔驰');
INSERT INTO `cartype` VALUES (3, '大众');
INSERT INTO `cartype` VALUES (4, '奇瑞');
INSERT INTO `cartype` VALUES (5, '开瑞');
INSERT INTO `cartype` VALUES (6, '路虎');
INSERT INTO `cartype` VALUES (7, '法拉利');
INSERT INTO `cartype` VALUES (8, '保时捷');
INSERT INTO `cartype` VALUES (9, '东风风行');
INSERT INTO `cartype` VALUES (11, '雷洛');

-- ----------------------------
-- Table structure for carxl
-- ----------------------------
DROP TABLE IF EXISTS `carxl`;
CREATE TABLE `carxl`  (
  `xid` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `xname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`xid`) USING BTREE,
  INDEX `fk_carxlaid`(`aid`) USING BTREE,
  CONSTRAINT `fk_carxlaid` FOREIGN KEY (`aid`) REFERENCES `cartype` (`aid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of carxl
-- ----------------------------
INSERT INTO `carxl` VALUES (1, 1, '宝马-x1');
INSERT INTO `carxl` VALUES (2, 2, '奔驰-x1');
INSERT INTO `carxl` VALUES (3, 3, '大众-x1');
INSERT INTO `carxl` VALUES (6, 6, '路虎-x2');
INSERT INTO `carxl` VALUES (7, 1, '宝马-x2');
INSERT INTO `carxl` VALUES (8, 6, '路虎-x1');
INSERT INTO `carxl` VALUES (9, 7, '法拉利-x1');
INSERT INTO `carxl` VALUES (10, 8, '保时捷-x1');
INSERT INTO `carxl` VALUES (11, 9, '东风风行-x1');
INSERT INTO `carxl` VALUES (12, 3, '大众-x2');
INSERT INTO `carxl` VALUES (13, 4, '奇瑞-x1');

-- ----------------------------
-- Table structure for chong
-- ----------------------------
DROP TABLE IF EXISTS `chong`;
CREATE TABLE `chong`  (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `omoney` float NOT NULL,
  `yid` int(11) NOT NULL,
  `osmoney` float NOT NULL,
  `olastmoney` float NOT NULL,
  `uid` int(11) NOT NULL,
  `oremark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `otime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`oid`) USING BTREE,
  INDEX `fk_chongrid`(`rid`) USING BTREE,
  INDEX `fk_chongyid`(`yid`) USING BTREE,
  INDEX `fk_chonguid`(`uid`) USING BTREE,
  CONSTRAINT `fk_chongrid` FOREIGN KEY (`rid`) REFERENCES `member` (`rid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_chonguid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_chongyid` FOREIGN KEY (`yid`) REFERENCES `youhui` (`yid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chong
-- ----------------------------
INSERT INTO `chong` VALUES (1, 1, 200, 1, 100, 300, 1, '很好', '2019-03-07');
INSERT INTO `chong` VALUES (3, 1, 100, 1, 100, 200, 1, '很好啊', '2019-03-11');
INSERT INTO `chong` VALUES (4, 1, 400, 2, 200, 600, 1, '非常好', '2019-03-11');
INSERT INTO `chong` VALUES (5, 1, 500, 3, 1000, 1500, 1, 'OP', '2019-03-11');
INSERT INTO `chong` VALUES (6, 1, 100, 1, 100, 200, 1, '很好', '2019-03-13');
INSERT INTO `chong` VALUES (7, 1, 200, 2, 200, 400, 1, '很好', '2019-03-13');
INSERT INTO `chong` VALUES (8, 6, 1000, 4, 500, 1500, 1, 'OK', '2019-06-06');
INSERT INTO `chong` VALUES (10, 1, 100, 1, 100, 200, 1, '好', '2020-11-15 17:13:43');
INSERT INTO `chong` VALUES (11, 13, 200, 2, 200, 400, 1, '1111', '2020-11-15 17:15:03');
INSERT INTO `chong` VALUES (12, 1, 200, 2, 200, 400, 1, '好', '2020-11-17 10:14:47');
INSERT INTO `chong` VALUES (13, 6, 600, 3, 500, 1100, 1, '好', '2020-11-17 10:27:08');
INSERT INTO `chong` VALUES (14, 6, 5000, 5, 1500, 6500, 1, '好', '2020-11-17 10:28:22');
INSERT INTO `chong` VALUES (15, 6, 600, 3, 500, 1100, 1, '好', '2020-11-17 10:29:05');
INSERT INTO `chong` VALUES (16, 6, 100, 1, 100, 200, 2, '小王', '2020-11-17 10:34:16');
INSERT INTO `chong` VALUES (17, 13, 5000, 5, 1500, 6500, 1, '好', '2020-11-19 20:44:47');
INSERT INTO `chong` VALUES (18, 13, 1500000, 5, 1500, 1501500, 1, '太好了', '2020-11-19 20:46:15');

-- ----------------------------
-- Table structure for cptype
-- ----------------------------
DROP TABLE IF EXISTS `cptype`;
CREATE TABLE `cptype`  (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cptype
-- ----------------------------
INSERT INTO `cptype` VALUES (1, '零件');
INSERT INTO `cptype` VALUES (2, '坐垫');
INSERT INTO `cptype` VALUES (3, '装饰');
INSERT INTO `cptype` VALUES (4, '机油');
INSERT INTO `cptype` VALUES (5, '内饰');

-- ----------------------------
-- Table structure for dj
-- ----------------------------
DROP TABLE IF EXISTS `dj`;
CREATE TABLE `dj`  (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `dname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `djf` int(11) NOT NULL,
  `dmoneyBl` double NOT NULL,
  `dzk` double NOT NULL,
  PRIMARY KEY (`did`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dj
-- ----------------------------
INSERT INTO `dj` VALUES (1, '青铜会员', 100, 0.1, 0.98);
INSERT INTO `dj` VALUES (2, '白银会员', 1000, 0.2, 0.9);
INSERT INTO `dj` VALUES (3, '黄金会员', 5000, 0.4, 0.75);
INSERT INTO `dj` VALUES (4, '白金会员', 10000, 0.5, 0.65);
INSERT INTO `dj` VALUES (6, '特级会员', 20000, 0.6, 0.5);
INSERT INTO `dj` VALUES (7, '总统会员', 1000000, 0.8, 0.4);

-- ----------------------------
-- Table structure for duihuan
-- ----------------------------
DROP TABLE IF EXISTS `duihuan`;
CREATE TABLE `duihuan`  (
  `hid` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `hcount` int(11) NOT NULL,
  `htime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`hid`) USING BTREE,
  INDEX `fk_duihuannid`(`nid`) USING BTREE,
  INDEX `fk_duihuanrid`(`rid`) USING BTREE,
  INDEX `fk_duihuanuid`(`uid`) USING BTREE,
  CONSTRAINT `fk_duihuannid` FOREIGN KEY (`nid`) REFERENCES `lipin` (`nid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_duihuanrid` FOREIGN KEY (`rid`) REFERENCES `member` (`rid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_duihuanuid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of duihuan
-- ----------------------------
INSERT INTO `duihuan` VALUES (1, 1, 1, 20, '2019-03-15 15:40:21', 1);
INSERT INTO `duihuan` VALUES (4, 1, 1, 50, '2019-03-15 17:03:53', 1);
INSERT INTO `duihuan` VALUES (5, 1, 1, 1, '2019-03-15 17:03:27', 1);
INSERT INTO `duihuan` VALUES (6, 2, 1, 50, '2019-03-15 17:03:19', 1);
INSERT INTO `duihuan` VALUES (7, 1, 1, 1, '2020-11-28 15:26:56', 1);
INSERT INTO `duihuan` VALUES (8, 1, 1, 1, '2020-11-28 15:27:14', 1);

-- ----------------------------
-- Table structure for getcp
-- ----------------------------
DROP TABLE IF EXISTS `getcp`;
CREATE TABLE `getcp`  (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `gcount` int(11) NOT NULL,
  `gtime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`gid`) USING BTREE,
  INDEX `fk_getcpfid`(`fid`) USING BTREE,
  INDEX `fk_getcpuid`(`uid`) USING BTREE,
  CONSTRAINT `fk_getcpfid` FOREIGN KEY (`fid`) REFERENCES `mytf` (`fid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_getcpuid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of getcp
-- ----------------------------
INSERT INTO `getcp` VALUES (1, 1, 20, '2019-03-08 09:17:49', 1);
INSERT INTO `getcp` VALUES (2, 1, 30, '2019-03-08 09:18:08', 1);
INSERT INTO `getcp` VALUES (3, 1, 50, '2019-03-08 09:26:05', 1);
INSERT INTO `getcp` VALUES (4, 1, 20, '2019-03-08 09:40:48', 1);
INSERT INTO `getcp` VALUES (5, 1, 200, '2019-03-12 01:40:36', 1);
INSERT INTO `getcp` VALUES (6, 1, 20, '2019-03-13 01:06:32', 1);
INSERT INTO `getcp` VALUES (7, 1, 20, '2019-03-13 01:07:17', 1);
INSERT INTO `getcp` VALUES (8, 1, 123, '2019-03-13 13:09:02', 1);
INSERT INTO `getcp` VALUES (10, 2, 100, '2020-11-16 22:51:34', 1);
INSERT INTO `getcp` VALUES (11, 3, 1000, '2020-11-16 22:51:50', 1);
INSERT INTO `getcp` VALUES (12, 1, 100, '2020-11-16 22:53:11', 1);
INSERT INTO `getcp` VALUES (13, 6, 2020, '2020-11-16 22:55:44', 1);
INSERT INTO `getcp` VALUES (14, 1, 100000, '2020-11-19 21:12:34', 1);
INSERT INTO `getcp` VALUES (15, 2, 10000, '2020-11-19 21:12:47', 1);
INSERT INTO `getcp` VALUES (16, 3, 10000, '2020-11-19 21:13:04', 1);
INSERT INTO `getcp` VALUES (17, 12, 200, '2020-11-21 22:08:50', 1);

-- ----------------------------
-- Table structure for jici
-- ----------------------------
DROP TABLE IF EXISTS `jici`;
CREATE TABLE `jici`  (
  `jid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `jtime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sid` int(11) NOT NULL,
  `jmoney` float NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`jid`) USING BTREE,
  INDEX `fk_jicirid`(`rid`) USING BTREE,
  INDEX `fk_jicisid`(`sid`) USING BTREE,
  INDEX `fk_jiciuid`(`uid`) USING BTREE,
  CONSTRAINT `fk_jicirid` FOREIGN KEY (`rid`) REFERENCES `member` (`rid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_jicisid` FOREIGN KEY (`sid`) REFERENCES `servicetype` (`sid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_jiciuid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jici
-- ----------------------------
INSERT INTO `jici` VALUES (1, 1, '2019-03-15 09:17:05', 2, 200, 1);
INSERT INTO `jici` VALUES (2, 1, '2019-03-15 10:56:55', 1, 200, 1);
INSERT INTO `jici` VALUES (3, 1, '2020-11-24 14:51:23', 1, 800, 1);
INSERT INTO `jici` VALUES (4, 1, '2020-11-24 14:54:51', 1, 500, 1);
INSERT INTO `jici` VALUES (5, 3, '2020-11-24 14:55:37', 2, 1000, 1);
INSERT INTO `jici` VALUES (6, 3, '2020-11-24 15:01:51', 1, 1000, 1);
INSERT INTO `jici` VALUES (7, 3, '2020-11-24 15:03:33', 1, 1000, 1);
INSERT INTO `jici` VALUES (8, 3, '2020-11-24 15:11:33', 1, 2222, 1);

-- ----------------------------
-- Table structure for lipin
-- ----------------------------
DROP TABLE IF EXISTS `lipin`;
CREATE TABLE `lipin`  (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `nname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `njf` int(11) NOT NULL,
  `ncount` int(11) NOT NULL,
  `nncount` int(11) NOT NULL,
  PRIMARY KEY (`nid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lipin
-- ----------------------------
INSERT INTO `lipin` VALUES (1, '苹果', 'apple.jpg', 12, 200, 147);
INSERT INTO `lipin` VALUES (2, '橘子', 'orange.jpg', 10, 150, 100);
INSERT INTO `lipin` VALUES (3, '小鸡', '2020112701391768837864.png', 123, 123, 123);

-- ----------------------------
-- Table structure for look
-- ----------------------------
DROP TABLE IF EXISTS `look`;
CREATE TABLE `look`  (
  `kid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `ktime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kremark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`kid`) USING BTREE,
  INDEX `fk_lookrid`(`rid`) USING BTREE,
  INDEX `fk_lookuid`(`uid`) USING BTREE,
  CONSTRAINT `fk_lookrid` FOREIGN KEY (`rid`) REFERENCES `member` (`rid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_lookuid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of look
-- ----------------------------
INSERT INTO `look` VALUES (1, 1, '2019-03-15', '上门聊天', 1);
INSERT INTO `look` VALUES (2, 1, '2019-03-15', '喝酒', 1);

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `rcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rpsw` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rtel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rsex` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `rbirthday` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rstatus` int(11) NOT NULL,
  `rjf` double NOT NULL,
  `rcarnum` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `xid` int(11) NOT NULL,
  `rcolor` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rway` float NULL DEFAULT NULL,
  `zid` int(11) NOT NULL,
  `rnum` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `raddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rremark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rtime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rmoney` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rflag` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`rid`) USING BTREE,
  INDEX `fk_memberdid`(`did`) USING BTREE,
  INDEX `fk_memberxid`(`xid`) USING BTREE,
  INDEX `fk_memberzid`(`zid`) USING BTREE,
  CONSTRAINT `fk_memberdid` FOREIGN KEY (`did`) REFERENCES `dj` (`did`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_memberxid` FOREIGN KEY (`xid`) REFERENCES `carxl` (`xid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_memberzid` FOREIGN KEY (`zid`) REFERENCES `pz` (`zid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES (1, 'A030746', '陈德帅', '666', '123.jpg', '13436651254', 1, 4, '1998-06-27', 1, 38.5, '鄂00001', 1, '黑色', 500, 4, '421222222222222', '湖北', '很好', '2019-03-06', '837000.0', 1);
INSERT INTO `member` VALUES (3, 'A050538', '实销', '123', '123.jpg', '13138384381', 0, 2, '2019-03-07', 1, 32.22, '鄂B55112', 1, '黑色', 100, 2, 'X01666', '湖北', '很好', '2019-03-07', '4778.0', 1);
INSERT INTO `member` VALUES (4, '123e', '销毁', '123', '123.jpg', '15382828282', 1, 1, '2019-03-11', 1, 0, '鄂B00001', 1, '213', 123, 2, '123', '123', '312', '2019-03-11', '800000', 0);
INSERT INTO `member` VALUES (5, '1234', '123', '123', '123.jpg', '15827351911', 1, 3, '2019-03-13', 1, 0, '鄂B00002', 1, '123', 125, 1, '124124', '123', '123', '2019-03-13', '100000', 1);
INSERT INTO `member` VALUES (6, 'A001', '张大明', '123', '123.jpg', '13995668331', 1, 1, '2019-06-13', 1, 0, '鄂A00001', 2, '黑色', 10, 1, '41221541042', '武汉大学计算机学院', '此客户很有钱，经常开各种豪车出去玩！', '2019-06-06', '917640.8', 1);
INSERT INTO `member` VALUES (10, '123q', '123', '123', '2020111401035539387818.png', '18372801145', 1, 1, '2020-11-14', 1, 0, '123', 11, '123', 123, 2, '123', '123', '123', '2020-11-14', '100000', 1);
INSERT INTO `member` VALUES (12, 'AK1110', '1111', '11111', '2020111401132840044885.jpg', '18548788465', 1, 1, '2020-11-14', 1, 0, '鄂A22005', 11, '1111', 5555, 2, '1233', '4497', '13348', '2020-11-14', '100000', 1);
INSERT INTO `member` VALUES (13, '123w', '123', '123', '2020111404142979948168.jpg', '18372801114', 1, 1, '2020-11-21', 0, 0, '1234', 2, '123134', 1231230, 2, '12312314', '仙桃', '1231234', '2020-11-14', '5887.5200', 1);

-- ----------------------------
-- Table structure for mytf
-- ----------------------------
DROP TABLE IF EXISTS `mytf`;
CREATE TABLE `mytf`  (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `fdw` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `faddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `foutprice` float NOT NULL,
  `finprice` float NOT NULL,
  `fimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fcount` int(11) NOT NULL,
  PRIMARY KEY (`fid`) USING BTREE,
  INDEX `fk_mytfcid`(`cid`) USING BTREE,
  CONSTRAINT `fk_mytfcid` FOREIGN KEY (`cid`) REFERENCES `cptype` (`cid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mytf
-- ----------------------------
INSERT INTO `mytf` VALUES (1, '香蕉', 3, '箱', '武汉', 112, 10, '123.jpg', 98587);
INSERT INTO `mytf` VALUES (2, '坐垫', 3, '个', '武汉', 100, 10, '123', 8766);
INSERT INTO `mytf` VALUES (3, '螺母', 1, '个', '123', 10, 1, '2019031106594764581.jpg', 1000);
INSERT INTO `mytf` VALUES (4, 'N9K-螺母', 1, '个', '1234', 2000, 123, '20190311070026135501.jpg', 0);
INSERT INTO `mytf` VALUES (5, '123', 1, '箱', '12345', 200, 100, '2020111607094599554308.jpg', 0);
INSERT INTO `mytf` VALUES (6, '123', 1, '包', '456', 600, 200, '20190311070800774494.jpg', 2010);
INSERT INTO `mytf` VALUES (7, '1234', 2, '包', '12345', 555, 66, '20190311080430786775.jpg', 0);
INSERT INTO `mytf` VALUES (8, '123', 2, '包', '142', 14, 1, '20190311080734969321.jpg', 0);
INSERT INTO `mytf` VALUES (9, '123', 1, '包', '124', 124, 124, '20190311080919935216.jpg', 0);
INSERT INTO `mytf` VALUES (10, '123', 1, '厅', '123', 123, 11, '2020111611314780845741.jpg', 0);
INSERT INTO `mytf` VALUES (11, '123', 1, '箱', '123', 123, 120, '2020111603462496398667.png', 0);
INSERT INTO `mytf` VALUES (12, '项链', 2, '个', '仙桃', 200, 20, '202011211008242544098.png', 200);

-- ----------------------------
-- Table structure for outcp
-- ----------------------------
DROP TABLE IF EXISTS `outcp`;
CREATE TABLE `outcp`  (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `tcount` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `ttime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tflag` int(11) NOT NULL,
  PRIMARY KEY (`tid`) USING BTREE,
  INDEX `fk_outcprid`(`rid`) USING BTREE,
  INDEX `fk_outcpfid`(`fid`) USING BTREE,
  INDEX `fk_outcpuid`(`uid`) USING BTREE,
  CONSTRAINT `fk_outcpfid` FOREIGN KEY (`fid`) REFERENCES `mytf` (`fid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_outcprid` FOREIGN KEY (`rid`) REFERENCES `member` (`rid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_outcpuid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of outcp
-- ----------------------------
INSERT INTO `outcp` VALUES (1, 13, 2, 1533, 1, '2019-03-12 14:02:50', 1);
INSERT INTO `outcp` VALUES (2, 13, 1, 1333, 1, '2019-03-13 10:10:50', 1);
INSERT INTO `outcp` VALUES (3, 13, 3, 1800, 1, '2020-11-19 13:43:39', 1);
INSERT INTO `outcp` VALUES (4, 13, 7, 0, 1, '2020-11-19 20:58:56', 1);
INSERT INTO `outcp` VALUES (5, 13, 6, 10, 1, '2020-11-19 21:00:13', 1);
INSERT INTO `outcp` VALUES (6, 3, 3, 200, 1, '2020-11-19 21:05:58', 1);
INSERT INTO `outcp` VALUES (7, 3, 4, 0, 1, '2020-11-19 21:07:15', 1);
INSERT INTO `outcp` VALUES (8, 13, 3, 500, 1, '2020-11-19 21:17:03', 1);
INSERT INTO `outcp` VALUES (9, 13, 3, 9000, 1, '2020-11-19 21:19:19', 1);
INSERT INTO `outcp` VALUES (10, 13, 1, 100, 1, '2020-11-20 10:26:21', 1);
INSERT INTO `outcp` VALUES (11, 13, 1, 100, 1, '2020-11-20 10:29:09', 1);
INSERT INTO `outcp` VALUES (12, 13, 1, 100, 1, '2020-11-20 10:30:30', 1);
INSERT INTO `outcp` VALUES (13, 13, 1, 200, 1, '2020-11-20 10:31:15', 1);
INSERT INTO `outcp` VALUES (14, 13, 2, 100, 1, '2020-11-20 10:34:17', 1);
INSERT INTO `outcp` VALUES (15, 13, 1, 100, 1, '2020-11-20 10:35:45', 1);
INSERT INTO `outcp` VALUES (16, 13, 1, 20, 1, '2020-11-20 10:36:16', 1);
INSERT INTO `outcp` VALUES (17, 13, 1, 20, 1, '2020-11-20 10:39:06', 1);
INSERT INTO `outcp` VALUES (18, 13, 1, 10, 1, '2020-11-20 10:48:08', 1);
INSERT INTO `outcp` VALUES (19, 13, 1, 10, 1, '2020-11-20 10:49:46', 1);
INSERT INTO `outcp` VALUES (20, 13, 1, 10, 1, '2020-11-20 10:50:31', 1);
INSERT INTO `outcp` VALUES (21, 13, 1, 10, 1, '2020-11-20 10:51:13', 1);
INSERT INTO `outcp` VALUES (22, 6, 1, 200, 1, '2020-11-20 10:51:53', 1);
INSERT INTO `outcp` VALUES (23, 6, 2, 100, 1, '2020-11-20 11:02:05', 1);
INSERT INTO `outcp` VALUES (24, 6, 2, 1, 1, '2020-11-20 11:08:26', 1);
INSERT INTO `outcp` VALUES (32, 6, 3, 800, 1, '2020-11-21 19:38:05', 1);
INSERT INTO `outcp` VALUES (33, 6, 3, 800, 1, '2020-11-21 19:42:44', 1);
INSERT INTO `outcp` VALUES (34, 6, 3, 1200, 1, '2020-11-21 19:47:06', 0);
INSERT INTO `outcp` VALUES (35, 6, 2, 8866, 1, '2020-11-21 19:47:42', 0);
INSERT INTO `outcp` VALUES (36, 13, 3, 100, 1, '2020-11-24 14:46:53', 0);

-- ----------------------------
-- Table structure for outcp1
-- ----------------------------
DROP TABLE IF EXISTS `outcp1`;
CREATE TABLE `outcp1`  (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `wcount` int(11) NOT NULL,
  `wname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wtel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `wtime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`wid`) USING BTREE,
  INDEX `fk_outcp1fid`(`fid`) USING BTREE,
  INDEX `fk_outcp1uid`(`uid`) USING BTREE,
  CONSTRAINT `fk_outcp1fid` FOREIGN KEY (`fid`) REFERENCES `mytf` (`fid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_outcp1uid` FOREIGN KEY (`uid`) REFERENCES `userinfo` (`uid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of outcp1
-- ----------------------------
INSERT INTO `outcp1` VALUES (1, 1, 1, '李刚', '13585851313', 1, '2019-03-15 09:00:00');
INSERT INTO `outcp1` VALUES (2, 1, 83, '666', '15827151515', 1, '2019-03-15 09:02:49');
INSERT INTO `outcp1` VALUES (3, 1, 20, '123', '15385121211', 1, '2019-03-15 09:13:50');
INSERT INTO `outcp1` VALUES (4, 1, 100, '王逸潇', '18472806554', 1, '2020-11-21 20:39:00');
INSERT INTO `outcp1` VALUES (5, 3, 100, '小老儿', '18372804456', 1, '2020-11-21 22:15:12');

-- ----------------------------
-- Table structure for pz
-- ----------------------------
DROP TABLE IF EXISTS `pz`;
CREATE TABLE `pz`  (
  `zid` int(11) NOT NULL AUTO_INCREMENT,
  `zname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`zid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pz
-- ----------------------------
INSERT INTO `pz` VALUES (1, '学生证');
INSERT INTO `pz` VALUES (2, '身份证');
INSERT INTO `pz` VALUES (3, '房产证');
INSERT INTO `pz` VALUES (4, '港澳通行证');
INSERT INTO `pz` VALUES (5, '临时居住证');

-- ----------------------------
-- Table structure for servicetype
-- ----------------------------
DROP TABLE IF EXISTS `servicetype`;
CREATE TABLE `servicetype`  (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of servicetype
-- ----------------------------
INSERT INTO `servicetype` VALUES (1, '上门服务');
INSERT INTO `servicetype` VALUES (2, '修理服务');
INSERT INTO `servicetype` VALUES (3, '聊天服务');
INSERT INTO `servicetype` VALUES (4, '网上聊天');

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo`  (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `upsw` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `urealname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `utel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usex` int(11) NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES (1, 'admin', '123', '张得帅', '13838384385', 1);
INSERT INTO `userinfo` VALUES (2, 'xiaow', '123', '张小王', '13838384388', 0);
INSERT INTO `userinfo` VALUES (3, 'liliu', '123', '张溜溜', '13838384384', 0);
INSERT INTO `userinfo` VALUES (4, 'xiaoge', '1234', '笑佬二', '18372809415', 1);

-- ----------------------------
-- Table structure for youhui
-- ----------------------------
DROP TABLE IF EXISTS `youhui`;
CREATE TABLE `youhui`  (
  `yid` int(11) NOT NULL AUTO_INCREMENT,
  `ytitle` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ybegintime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `yendtime` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ymoney` double NULL DEFAULT NULL,
  `ylessmoney` double NULL DEFAULT NULL,
  PRIMARY KEY (`yid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of youhui
-- ----------------------------
INSERT INTO `youhui` VALUES (1, '冲一百送一百', '2018-09-20', '2018-09-21', 100, 100);
INSERT INTO `youhui` VALUES (2, '冲两百送二百', '2018-09-20', '2018-09-21', 200, 200);
INSERT INTO `youhui` VALUES (3, '冲六百送五百', '2018-09-20', '2018-09-21', 500, 600);
INSERT INTO `youhui` VALUES (4, '年终大放送', '2019-06-06', '2019-06-30', 500, 1000);
INSERT INTO `youhui` VALUES (5, '冲5000送1500', '2020-11-12', '2020-12-06', 1500, 5000);

SET FOREIGN_KEY_CHECKS = 1;
