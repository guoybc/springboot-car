package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.CarxlEntity;
import com.xiao.service.CarxlService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("carxl")
public class CarxlController {

	@Autowired
	private CarxlService carxlService;

	@RequestMapping("toCarxl")
	public String toCarxl() {
		return "carxl/carxl";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(CarxlEntity carxlEntity) {
		List<CarxlEntity> findAll = carxlService.findAllByPager(carxlEntity);
		int count = carxlService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				carxlService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "carxl/insert";
	}

	@RequestMapping("findOneByXname")
	@ResponseBody
	public int findOneByXname(String xname) {
		CarxlEntity carxlEntity = carxlService.findOneByXname(xname);
		if (carxlEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(CarxlEntity carxlEntity) {
		carxlService.insert(carxlEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int xid) {
		carxlService.delOne(xid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int xid, Model model) {
		CarxlEntity carxlEntity = carxlService.findOneByXid(xid);
		model.addAttribute("ar", carxlEntity);
		return "carxl/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(CarxlEntity carxlEntity) {
		carxlService.update(carxlEntity);
		return 1;
	}
	@RequestMapping("findAllByAid")
	@ResponseBody
	public List<CarxlEntity> findAllByAid(int aid) {
		List<CarxlEntity> ar =  carxlService.findAllByAid(aid);
		return ar;
	}



}
