package com.xiao.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.UserEntity;
import com.xiao.service.UserService;

@Controller
@RequestMapping("login")
public class LoginController {

	@Autowired
	private UserService userService;

	@RequestMapping("index")
	public String index() {
		return "index";
	}
	@RequestMapping("tologin")
	@ResponseBody
	public int tologin(UserEntity userEntity,HttpServletRequest request) {
		UserEntity user = userService.finOneByUser(userEntity);
		if(user!=null) {
			//创建Session
			HttpSession session = request.getSession();
			session.setAttribute("USER", user);
			return 1;
		}
		return 0;
	}

	@RequestMapping("getUserNname")
	@ResponseBody
	public String getUserNname(HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserEntity userEntity =(UserEntity) session.getAttribute("USER");
		return userEntity.getUrealname();
	}

}
