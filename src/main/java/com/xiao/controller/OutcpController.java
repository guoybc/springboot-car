package com.xiao.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.DjEntity;
import com.xiao.entity.MemberEntity;
import com.xiao.entity.MytfEntity;
import com.xiao.entity.OutcpEntity;
import com.xiao.entity.TotalEntity;
import com.xiao.entity.UserEntity;
import com.xiao.service.DjService;
import com.xiao.service.MemberService;
import com.xiao.service.MytfService;
import com.xiao.service.OutcpService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("outcp")
public class OutcpController {

	@Autowired
	private OutcpService outcpService;

	@Autowired
	private DjService djService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private MytfService mytfService;

	@RequestMapping("toOutcp")
	public String toOutcp() {
		return "outcp/outcp";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(OutcpEntity outcpEntity) {
		List<OutcpEntity> findAll = outcpService.findAllByPager(outcpEntity);
		int count = outcpService.findAllCount(outcpEntity.getRid());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("toInsert")
	public String toInsert(int rid, Model model) {
		model.addAttribute("rid", rid);
		return "outcp/insert";
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(OutcpEntity outcpEntity, HttpServletRequest request) {
		System.out.println(outcpEntity);
		HttpSession session = request.getSession();
		UserEntity attribute = (UserEntity) session.getAttribute("USER");
		int uid = attribute.getUid();
		outcpEntity.setUid(uid);
		// 获取操作时间
		outcpEntity.setTtime(XiaoUtils.getCurrentTimeMes());
		// 判断当前商品是否已经在购物车中
		OutcpEntity one = outcpService.findCountByOutcp(outcpEntity);
		if (one != null) {
			// 修改购物车中的数量
			outcpEntity.setTcount(one.getTcount() + outcpEntity.getTcount());
			outcpEntity.setTid(one.getTid());
			outcpService.update(outcpEntity);
		} else {
			// 写入数据库
			outcpService.insert(outcpEntity);
		}
		return 1;

	}

	@RequestMapping("toUpdate")
	public String toUpdate(int tid, Model model) {
		OutcpEntity outcpEntity = outcpService.findOneByTid(tid);
		model.addAttribute("ar", outcpEntity);
		return "outcp/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(OutcpEntity outcpEntity) {
		outcpService.update(outcpEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<OutcpEntity> findAllNoPager(int rid) {
		return outcpService.findAll(rid);
	}

	@RequestMapping("toEnd")
	public String toEnd(int rid, Model model) {
		model.addAttribute("rid", rid);
		return "outcp/end";
	}

	@RequestMapping("getTotal")
	@ResponseBody
	public TotalEntity getTotal(int rid) {
		// 找到客户信息
		MemberEntity mem = memberService.findOneByRid(rid);
		// 找到客户的等级
		DjEntity djEntity = djService.findOneByDid(mem.getDid());
		// 则扣
		Double dzk = djEntity.getDzk();
		// 计算金额
		TotalEntity totalEntity = new TotalEntity();
		double beTotal = 0.0;
		double endTotal = 0.0;
		List<OutcpEntity> ar = outcpService.findAll(rid);
		for (OutcpEntity outcpEntity : ar) {
			// 则扣前的金额
			beTotal += XiaoUtils.getFormatDouble((double) (outcpEntity.getTcount() * outcpEntity.getFoutprice()));
		}
		// 则扣后的金额
		endTotal = XiaoUtils.getFormatDouble(beTotal * dzk);
		// 封装
		totalEntity.setDname(djEntity.getDname());
		totalEntity.setPreTotal(beTotal);
		totalEntity.setEndTotal(endTotal);
		totalEntity.setDzk(dzk);
		return totalEntity;
	}

	@RequestMapping("insertMes")
	@ResponseBody
	public int insertMes(OutcpEntity outcpEntity) {
		System.out.println(outcpEntity);
		Integer[] arfid = outcpEntity.getArfid();
		Integer[] artcount = outcpEntity.getArtcount();
		// 进行出库操作
		MytfEntity findOneByFid = null;
		int fcount = 0;
		int tcount = 0;
		for (int i = 0; i < arfid.length; i++) {
			findOneByFid = mytfService.findOneByFid(arfid[i]);
			// 获取库存量
			fcount = findOneByFid.getFcount();
			// 获取到出库数量
			for (int j = 0; j < artcount.length; j++) {
				tcount = artcount[i];
			}
			// 修改仓库数量
			findOneByFid.setFcount(fcount - tcount);
			// 写入后台数据库
			mytfService.update(findOneByFid);
		}
		//修改用户的金额
		MemberEntity memberEntity = memberService.findOneByRid(outcpEntity.getRid());
		memberEntity.setRmoney(XiaoUtils.getFormatDouble(memberEntity.getRmoney()-outcpEntity.getEndTotal()));
		//修改-写入数据库
		memberService.update(memberEntity);

		//增加出库记录
		outcpService.updateTflag(outcpEntity);

		return 1;
	}

	@RequestMapping("updateTcount")
	@ResponseBody
	public int updateTcount(OutcpEntity outcpEntity) {
		outcpService.updateTcount(outcpEntity);
		return 1;
	}

	@RequestMapping("findAllCount")
	@ResponseBody
	public int findAllCount(int rid) {
		return outcpService.findAllCount(rid);
	}

	@RequestMapping("findAllByFid")
	@ResponseBody
	public JSONObject findAllByFid(OutcpEntity outcpEntity) {
		outcpEntity.setTflag(1);
		List<OutcpEntity> findAll = outcpService.findAllNoPagerByFid(outcpEntity);
		int count = outcpService.findAllCountByFid(outcpEntity);
		System.out.println(count);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("del")
	@ResponseBody
	public int del(int tid) {
		outcpService.delOne(tid);
		return 1;
	}
}
