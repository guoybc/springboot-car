package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.YouhuiEntity;
import com.xiao.service.YouhuiService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("youhui")
public class YouhuiController {

	@Autowired
	private YouhuiService youhuiService;

	@RequestMapping("toYouhui")
	public String toYouhui() {
		return "youhui/youhui";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(YouhuiEntity youhuiEntity) {
		List<YouhuiEntity> findAll = youhuiService.findAllByPager(youhuiEntity);
		int count = youhuiService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				youhuiService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "youhui/insert";
	}

	@RequestMapping("findOneByYtitle")
	@ResponseBody
	public int findOneByYtitle(String ytitle) {
		YouhuiEntity youhuiEntity = youhuiService.findOneByYtitle(ytitle);
		if (youhuiEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(YouhuiEntity youhuiEntity) {
		youhuiService.insert(youhuiEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int yid) {
		youhuiService.delOne(yid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int yid, Model model) {
		YouhuiEntity youhuiEntity = youhuiService.findOneByYid(yid);
		model.addAttribute("ar", youhuiEntity);
		return "youhui/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(YouhuiEntity youhuiEntity) {
		youhuiService.update(youhuiEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<YouhuiEntity> findAllNoPager(){
		return youhuiService.findAll();
	}

	@RequestMapping("findOneByYid")
	@ResponseBody
	public YouhuiEntity findOneByYid(int yid) {
		return youhuiService.findOneByYid(yid);
	}

}
