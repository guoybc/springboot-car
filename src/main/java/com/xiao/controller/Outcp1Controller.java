package com.xiao.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.Outcp1Entity;
import com.xiao.entity.UserEntity;
import com.xiao.service.Outcp1Service;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("outcp1")
public class Outcp1Controller {

	@Autowired
	private Outcp1Service outcp1Service;

	@RequestMapping("toOutcp1")
	public String toOutcp1() {
		return "outcp1/outcp1";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(Outcp1Entity outcp1Entity) {
		List<Outcp1Entity> findAll = outcp1Service.findAllByPager(outcp1Entity);
		int count = outcp1Service.findAllCount(outcp1Entity);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("toInsert")
	public String toInsert(Model model) {
		return "outcp1/insert";
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(Outcp1Entity outcp1Entity, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserEntity attribute = (UserEntity) session.getAttribute("USER");
		int uid = attribute.getUid();
		outcp1Entity.setUid(uid);
		// 获取操作时间
		outcp1Entity.setWtime(XiaoUtils.getCurrentTimeMes());
		outcp1Service.insert(outcp1Entity);
		return 1;
	}

	@RequestMapping("findAllByFid")
	@ResponseBody
	public JSONObject findAllByFid(Outcp1Entity outcp1Entity){
		List<Outcp1Entity> findAll = outcp1Service.findAllByPager(outcp1Entity);
		int count = outcp1Service.findAllCount(outcp1Entity);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

}
