package com.xiao.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiao.entity.MytfEntity;
import com.xiao.service.MytfService;
import com.xiao.util.XiaoUtils;

@Controller
@RequestMapping("mytf")
public class MytfController {

	private final String IMG_URL = "E://JavaCode//Carimg//";

	@Autowired
	private MytfService mytfService;

	@RequestMapping("toMytf")
	public String toMytf() {
		return "mytf/mytf";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(MytfEntity mytfEntity) {
		List<MytfEntity> findAll = mytfService.findAllByPager(mytfEntity);
		int count = mytfService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "mytf/insert";
	}

	@RequestMapping("findOneByFname")
	@ResponseBody
	public int findOneByFname(String aname) {
		MytfEntity mytfEntity = mytfService.findOneByFname(aname);
		if (mytfEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(MytfEntity mytfEntity) {
		String oldName = "";
		String newName = "";
		if (!mytfEntity.getFileimg().isEmpty()) {
			// 获取上传路径
			oldName = mytfEntity.getFileimg().getOriginalFilename();
			newName = XiaoUtils.getReloadName(oldName);
			try {
				byte[] bytes = mytfEntity.getFileimg().getBytes();
				Path path = Paths.get(IMG_URL + newName);
				Files.write(path, bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mytfEntity.setFimg(newName);
		// 获取创建的时间
		System.out.println(mytfEntity);
		mytfService.insert(mytfEntity);
		return 1;

	}

	@RequestMapping("toUpdate")
	public String toUpdate(int fid, Model model) {
		MytfEntity mytfEntity = mytfService.findOneByFid(fid);
		model.addAttribute("ar", mytfEntity);
		return "mytf/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(MytfEntity mytfEntity) {
		MytfEntity one = mytfService.findOneByFid(mytfEntity.getFid());
		// 获取原图片名字
		String fimg = one.getFimg();
		// 获取上传路径
		Path path = null;
		if (!mytfEntity.getFileimg().isEmpty()) {
			// 重新上传图片
			String oldName = mytfEntity.getFileimg().getOriginalFilename();
			String newName = XiaoUtils.getReloadName(oldName);
			try {
				byte[] bytes = mytfEntity.getFileimg().getBytes();
				path = Paths.get(IMG_URL + newName);
				Files.write(path, bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 删除这个图片
			path = Paths.get(IMG_URL + fimg);
			if (Files.exists(path)) {
				try {
					Files.delete(path);
				} catch (IOException e) {
					// TODO Auto-generated catch block
				}
			}
			mytfEntity.setFimg(newName);
		}
		mytfService.update(mytfEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<MytfEntity> findAllNoPager() {
		return mytfService.findAll();
	}


	@RequestMapping("findAllByCid")
	@ResponseBody
	public List<MytfEntity> findAllByCid(int cid){
		return mytfService.findAllByCid(cid);
	}

	@RequestMapping("findOneByFid")
	@ResponseBody
	public MytfEntity findOneByFid(int fid) {
		return mytfService.findOneByFid(fid);
	}

}
