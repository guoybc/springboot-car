package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.DjEntity;
import com.xiao.service.DjService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("dj")
public class DjController {

	@Autowired
	private DjService djService;

	@RequestMapping("toDj")
	public String toDj() {
		return "dj/dj";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(DjEntity djEntity) {
		List<DjEntity> findAll = djService.findAllByPager(djEntity);
		int count = djService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				djService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "dj/insert";
	}

	@RequestMapping("findOneByDname")
	@ResponseBody
	public int findOneByDname(String dname) {
		DjEntity djEntity =  djService.findOneByDname(dname);
		if(djEntity!=null) {
			return 1;
		}else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(DjEntity djEntity) {
		djService.insert(djEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int did) {
		djService.delOne(did);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int did,Model model) {
		DjEntity djEntity= djService.findOneByDid(did);
		model.addAttribute("ar",djEntity);
		return "dj/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(DjEntity djEntity) {
		djService.update(djEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<DjEntity> findAllNoPager(){
		return djService.findAll();
	}


}
