package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.CptypeEntity;
import com.xiao.service.CptypeService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("cptype")
public class CptypeController {

	@Autowired
	private CptypeService cptypeService;

	@RequestMapping("toCptype")
	public String toCptype() {
		return "cptype/cptype";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(CptypeEntity cptypeEntity) {
		List<CptypeEntity> findAll = cptypeService.findAllByPager(cptypeEntity);
		int count = cptypeService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				cptypeService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "cptype/insert";
	}

	@RequestMapping("findOneByCname")
	@ResponseBody
	public int findOneByCname(String cname) {
		CptypeEntity cptypeEntity = cptypeService.findOneByCname(cname);
		if (cptypeEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(CptypeEntity cptypeEntity) {
		cptypeService.insert(cptypeEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int cid) {
		cptypeService.delOne(cid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int cid, Model model) {
		CptypeEntity cptypeEntity = cptypeService.findOneByCid(cid);
		model.addAttribute("ar", cptypeEntity);
		return "cptype/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(CptypeEntity cptypeEntity) {
		cptypeService.update(cptypeEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<CptypeEntity> findAllNoPager(){
		return cptypeService.findAll();
	}

}
