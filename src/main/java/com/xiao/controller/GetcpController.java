package com.xiao.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiao.entity.GetcpEntity;
import com.xiao.entity.MytfEntity;
import com.xiao.entity.UserEntity;
import com.xiao.service.GetcpService;
import com.xiao.service.MytfService;
import com.xiao.util.XiaoUtils;

@Controller
@RequestMapping("getcp")
public class GetcpController {

	@Autowired
	private GetcpService getcpService;

	@Autowired
	private MytfService mytfService;

	@RequestMapping("toGetcp")
	public String toGetcp() {
		return "getcp/getcp";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(GetcpEntity getcpEntity) {
		List<GetcpEntity> findAll = getcpService.findAllByPager(getcpEntity);
		int count = getcpService.findAllCount(getcpEntity.getFid());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("toInsert")
	public String toInsert(int fid,Model model) {
		MytfEntity ar = mytfService.findOneByFid(fid);
		model.addAttribute("ar",ar );
		return "getcp/getcpShowMes/insert";
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(GetcpEntity getcpEntity,HttpServletRequest request) {
		//获取session
		HttpSession session = request.getSession();
		UserEntity user = (UserEntity)session.getAttribute("USER");
		int uid = user.getUid();
		//获取系统当前时间
		String gtime = XiaoUtils.getCurrentTimeMes();
		getcpEntity.setGtime(gtime);
		getcpEntity.setUid(uid);
		//入库
		getcpService.insert(getcpEntity);
		//修改mytf数据
		MytfEntity mytfEntity = mytfService.findOneByFid(getcpEntity.getFid());
		mytfEntity.setFcount(mytfEntity.getFcount()+getcpEntity.getGcount());
		//入库
		mytfService.update(mytfEntity);
		return 1;

	}


	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<GetcpEntity> findAllNoPager(int fid) {
		return getcpService.findAll(fid);
	}

	/**
	 * 跳转到转入仓库操作详情界面
	 * @param fid
	 * @param model
	 * @return
	 */
	@RequestMapping("toShowMes")
	public String toShowMes(int fid,Model model) {
		model.addAttribute("fid", fid);
		return "getcp/getcpShowMes/getcpMes";
	}

	/**
	 * 出库信息
	 * @param fid
	 * @param model
	 * @return
	 */
	@RequestMapping("toShowOutMes")
	public String toShowOutMes(int fid,Model model) {
		model.addAttribute("fid", fid);
		return "getcp/outcpShowMes/outcpMes";
	}

}
