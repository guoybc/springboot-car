package com.xiao.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiao.entity.LipinEntity;
import com.xiao.service.LipinService;
import com.xiao.util.XiaoUtils;

@Controller
@RequestMapping("lipin")
public class LipinController {

	private final String IMG_URL = "E://JavaCode//Carimg//";

	@Autowired
	private LipinService lipinService;

	@RequestMapping("toLipin")
	public String toLipin() {
		return "lipin/lipin";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(LipinEntity lipinEntity) {
		List<LipinEntity> findAll = lipinService.findAllByPager(lipinEntity);
		int count = lipinService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				lipinService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "lipin/insert";
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(LipinEntity lipinEntity) {
		String oldName = "";
		String newName = "";
		if (!lipinEntity.getFileimg().isEmpty()) {
			// 获取上传路径
			oldName = lipinEntity.getFileimg().getOriginalFilename();
			newName = XiaoUtils.getReloadName(oldName);
			try {
				byte[] bytes = lipinEntity.getFileimg().getBytes();
				Path path = Paths.get(IMG_URL + newName);
				Files.write(path, bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		lipinEntity.setNimg(newName);
		lipinEntity.setNncount(lipinEntity.getNcount());
		lipinService.insert(lipinEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int nid) {
		lipinService.delOne(nid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int nid, Model model) {
		LipinEntity lipinEntity = lipinService.findOneByNid(nid);
		model.addAttribute("ar", lipinEntity);
		return "lipin/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(LipinEntity lipinEntity) {
		System.out.println(lipinEntity);
		// 获取上传路径
		Path path = null;
		LipinEntity one = lipinService.findOneByNid(lipinEntity.getNid());
		if (!lipinEntity.getFileimg().isEmpty()) {
			// 重新上传图片
			String oldName = lipinEntity.getFileimg().getOriginalFilename();
			String newName = XiaoUtils.getReloadName(oldName);
			try {
				byte[] bytes = lipinEntity.getFileimg().getBytes();
				path = Paths.get(IMG_URL + newName);
				Files.write(path, bytes);
				// 删除旧图片
				path = Paths.get(IMG_URL + one.getNimg());
				if (Files.exists(path)) {
					Files.delete(path);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			lipinEntity.setNimg(newName);
		}
		lipinService.update(lipinEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<LipinEntity> findAllNoPager() {
		return lipinService.findAll();
	}

	@RequestMapping("findOneByNid")
	@ResponseBody
	public LipinEntity findOneByHid(int nid) {
		return lipinService.findOneByNid(nid);
	}

}
