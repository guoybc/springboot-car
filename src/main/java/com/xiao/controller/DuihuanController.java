package com.xiao.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiao.entity.DuihuanEntity;
import com.xiao.entity.LipinEntity;
import com.xiao.entity.MemberEntity;
import com.xiao.entity.UserEntity;
import com.xiao.service.DuihuanService;
import com.xiao.service.LipinService;
import com.xiao.service.MemberService;
import com.xiao.util.XiaoUtils;

@Controller
@RequestMapping("duihuan")
public class DuihuanController {

	@Autowired
	private DuihuanService duihuanService;

	@Autowired
	private MemberService memberService;

	@Autowired
	private LipinService lipinService;

	@RequestMapping("toDuihuan")
	public String toDuihuan() {
		return "duihuan/duihuan";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(DuihuanEntity duihuanEntity) {
		List<DuihuanEntity> findAll = duihuanService.findAllByPager(duihuanEntity);
		int count = duihuanService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				duihuanService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "duihuan/insert";
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(DuihuanEntity duihuanEntity,HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserEntity entity = (UserEntity)session.getAttribute("USER");
		MemberEntity memberEntity = memberService.findOneByRid(duihuanEntity.getRid());
		memberEntity.setRjf(Double.valueOf(duihuanEntity.getRrjf()));
		//更新积分
		memberService.update(memberEntity);
		//更新库存
		LipinEntity lipinEntity = lipinService.findOneByNid(duihuanEntity.getNid());
		lipinEntity.setNncount(lipinEntity.getNncount()-duihuanEntity.getHcount());
		lipinService.update(lipinEntity);
		//获取时间
		String currentTimeMes = XiaoUtils.getCurrentTimeMes();
		duihuanEntity.setHtime(currentTimeMes);
		duihuanEntity.setUid(entity.getUid());
		duihuanService.insert(duihuanEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int hid) {
		duihuanService.delOne(hid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int hid, Model model) {
		DuihuanEntity duihuanEntity = duihuanService.findOneByHid(hid);
		model.addAttribute("ar", duihuanEntity);
		return "duihuan/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(DuihuanEntity duihuanEntity) {
		duihuanService.update(duihuanEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<DuihuanEntity> findAllNoPager() {
		return duihuanService.findAll();
	}

}
