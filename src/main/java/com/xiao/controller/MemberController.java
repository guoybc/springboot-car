package com.xiao.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.xiao.entity.DjCountEntity;
import com.xiao.entity.MemberEntity;
import com.xiao.service.MemberService;
import com.xiao.util.XiaoUtils;

@Controller
@RequestMapping("member")
public class MemberController {

	private final String IMG_URL = "E://JavaCode//Carimg//";

	@Autowired
	private MemberService memberService;

	@RequestMapping("toMember")
	public String toMember() {
		return "member/member";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(MemberEntity memberEntity) {
		List<MemberEntity> findAll = memberService.findAllByPager(memberEntity);
		int count = memberService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				memberService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "member/insert";
	}

	@RequestMapping("findOneByRcard")
	@ResponseBody
	public Object findOneByRcard(String rcard) {
		MemberEntity memberEntity = memberService.findOneByRcard(rcard);
		if (memberEntity != null) {
			return memberEntity;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(MemberEntity memberEntity) {
		memberEntity.setRstatus(1);
		memberEntity.setRmoney(0.0);
		memberEntity.setRjf(0.0);
		String oldName = "";
		String newName = "";
		if (!memberEntity.getFileimg().isEmpty()) {
			// 获取上传路径
			oldName = memberEntity.getFileimg().getOriginalFilename();
			newName = XiaoUtils.getReloadName(oldName);
			try {
				byte[] bytes = memberEntity.getFileimg().getBytes();
				Path path = Paths.get(IMG_URL + newName);
				Files.write(path, bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		memberEntity.setRimg(newName);
		// 获取创建的时间
		String currentTime = XiaoUtils.getCurrentTime();
		memberEntity.setRtime(currentTime);
		// 放入数据库
		memberService.insert(memberEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int rid) {
		memberService.delOne(rid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int rid, Model model) {
		MemberEntity memberEntity = memberService.findOneByRid(rid);
		model.addAttribute("ar", memberEntity);
		return "member/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(MemberEntity memberEntity) {
		// 获取上传路径
		Path path = null;
		MemberEntity one = memberService.findOneByRid(memberEntity.getRid());
		if (!memberEntity.getFileimg().isEmpty()) {
			// 重新上传图片
			String oldName = memberEntity.getFileimg().getOriginalFilename();
			String newName = XiaoUtils.getReloadName(oldName);
			try {
				byte[] bytes = memberEntity.getFileimg().getBytes();
				path = Paths.get(IMG_URL + newName);
				Files.write(path, bytes);
				//删除旧图片
				path = Paths.get(IMG_URL + one.getRimg());
				if(Files.exists(path)) {
					Files.delete(path);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			memberEntity.setRimg(newName);
		}
		memberService.update(memberEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<MemberEntity> findAllNoPager() {
		return memberService.findAll();
	}

	@RequestMapping("toshowMes")
	public String toshowMes(int rid, Model model) {
		MemberEntity ar = memberService.findOneByRid(rid);
		model.addAttribute("rid", rid);
		model.addAttribute("ar", ar);
		return "member/showMes";
	}

	@RequestMapping("findOneByRid")
	@ResponseBody
	public MemberEntity findOneByRid(int rid) {
		return memberService.findOneByRid(rid);
	}

	@RequestMapping("findAllByDid")
	@ResponseBody
	public JSONObject findAllByDid(MemberEntity memberEntity) {
		List<MemberEntity> findAll = memberService.findAllByDid(memberEntity);
		int count = memberService.findCountByDid(memberEntity.getDid());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}
	@RequestMapping("findDjAndCount")
	@ResponseBody
	public List<DjCountEntity> findDjAndCount(){
		return memberService.findDjAndCount();
	}
}
