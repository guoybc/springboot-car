package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.ServicetypeEntity;
import com.xiao.service.ServicetypeService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("servicetype")
public class ServicetypeController {

	@Autowired
	private ServicetypeService servicetypeService;

	@RequestMapping("toServicetype")
	public String toServicetype() {
		return "servicetype/servicetype";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(ServicetypeEntity servicetypeEntity) {
		List<ServicetypeEntity> findAll = servicetypeService.findAllByPager(servicetypeEntity);
		int count = servicetypeService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				servicetypeService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "servicetype/insert";
	}

	@RequestMapping("findOneBySname")
	@ResponseBody
	public int findOneBySname(String sname) {
		ServicetypeEntity servicetypeEntity = servicetypeService.findOneBySname(sname);
		if (servicetypeEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(ServicetypeEntity servicetypeEntity) {
		servicetypeService.insert(servicetypeEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int sid) {
		servicetypeService.delOne(sid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int sid, Model model) {
		ServicetypeEntity servicetypeEntity = servicetypeService.findOneBySid(sid);
		model.addAttribute("ar", servicetypeEntity);
		return "servicetype/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(ServicetypeEntity servicetypeEntity) {
		servicetypeService.update(servicetypeEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<ServicetypeEntity> findAllNoPager(){
		return servicetypeService.findAll();
	}

}
