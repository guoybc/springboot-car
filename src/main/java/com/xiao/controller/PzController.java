package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.PzEntity;
import com.xiao.service.PzService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("pz")
public class PzController {

	@Autowired
	private PzService pzService;

	@RequestMapping("toPz")
	public String toPz() {
		return "pz/pz";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(PzEntity pzEntity) {
		List<PzEntity> findAll = pzService.findAllByPager(pzEntity);
		int count = pzService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				pzService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "pz/insert";
	}

	@RequestMapping("findOneByZname")
	@ResponseBody
	public int findOneByZname(String zname) {
		PzEntity pzEntity = pzService.findOneByZname(zname);
		if (pzEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(PzEntity pzEntity) {
		pzService.insert(pzEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int zid) {
		pzService.delOne(zid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int zid, Model model) {
		PzEntity pzEntity = pzService.findOneByZid(zid);
		model.addAttribute("ar", pzEntity);
		return "pz/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(PzEntity pzEntity) {
		pzService.update(pzEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<PzEntity> findAllNoPager(){
		return pzService.findAll();
	}


}
