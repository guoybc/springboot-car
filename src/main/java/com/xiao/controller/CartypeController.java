package com.xiao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiao.entity.CartypeEntity;
import com.xiao.service.CartypeService;
import com.xiao.util.XiaoUtils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("cartype")
public class CartypeController {

	@Autowired
	private CartypeService cartypeService;

	@RequestMapping("toCartype")
	public String toCartype() {
		return "cartype/cartype";
	}

	@RequestMapping("findAll")
	@ResponseBody
	public JSONObject findAll(CartypeEntity cartypeEntity) {
		List<CartypeEntity> findAll = cartypeService.findAllByPager(cartypeEntity);
		int count = cartypeService.findAllCount();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rows", findAll);
		jsonObject.put("total", count);
		return jsonObject;
	}

	@RequestMapping("delSome")
	@ResponseBody
	public int delSome(String ar) {
		int[] dels = XiaoUtils.getSplitString(ar);
		for (int i = 0; i < dels.length; i++) {
			// 循环删除
			if (dels[i] != 0) {
				cartypeService.delOne(dels[i]);
			}
		}
		return 1;
	}

	@RequestMapping("toInsert")
	public String toInsert() {
		return "cartype/insert";
	}

	@RequestMapping("findOneByAname")
	@ResponseBody
	public int findOneByAname(String aname) {
		CartypeEntity cartypeEntity = cartypeService.findOneByAname(aname);
		if (cartypeEntity != null) {
			return 1;
		} else {
			return 0;
		}
	}

	@RequestMapping("insert")
	@ResponseBody
	public int insert(CartypeEntity cartypeEntity) {
		cartypeService.insert(cartypeEntity);
		return 1;

	}

	@RequestMapping("delOne")
	@ResponseBody
	public int delOne(int aid) {
		cartypeService.delOne(aid);
		return 1;
	}

	@RequestMapping("toUpdate")
	public String toUpdate(int aid, Model model) {
		CartypeEntity cartypeEntity = cartypeService.findOneByAid(aid);
		model.addAttribute("ar", cartypeEntity);
		return "cartype/update";
	}

	@RequestMapping("update")
	@ResponseBody
	public int update(CartypeEntity cartypeEntity) {
		cartypeService.update(cartypeEntity);
		return 1;
	}

	@RequestMapping("findAllNoPager")
	@ResponseBody
	public List<CartypeEntity> findAllNoPager(){
		return cartypeService.findAll();
	}

}
