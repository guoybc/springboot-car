package com.xiao.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class LoginConfig implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 注册TestInterceptor拦截器
		InterceptorRegistration registration = registry.addInterceptor(new AdminInterceptor());
		registration.addPathPatterns("/**"); // 所有路径都被拦截
		registration.excludePathPatterns( // 添加不拦截路径
				"/img/**",//图片
				"/login/**", // 登录
				"/**/*.html", // html静态资源
				"/**/*.js", // js静态资源
				"/**/*.css", // css静态资源
				"/**/*.woff", "/**/*.ttf", "/**/*.png", "/**/*.img", "/**/*.eot", "/**/*.svg", "/**/*.ttf",
				"/**/*.woff", "/**/*.woff2");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// TODO Auto-generated method stub
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/")
				.addResourceLocations("file:E:/JavaCode/Carimg/");
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}

}
