package com.xiao.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import com.xiao.entity.UserEntity;

public class AdminInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		try {
			// 统一拦截（查询当前session是否存在user）(这里user会在每次登陆成功后，写入session)
			UserEntity user = (UserEntity) request.getSession().getAttribute("USER");
			if (user != null) {
				return true;
			}
			response.sendRedirect("/login/index");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;// 如果设置为false时，被请求时，拦截器执行到此处将不会继续操作
	}

}
