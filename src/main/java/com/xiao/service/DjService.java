package com.xiao.service;

import java.util.List;

import com.xiao.entity.DjEntity;

public interface DjService {

	public List<DjEntity> findAll();

	public int findAllCount();

	public List<DjEntity> findAllByPager(DjEntity djEntity);

	public void delOne(int did);

	public DjEntity findOneByDname(String dname);

	public void insert(DjEntity djEntity);

	public DjEntity findOneByDid(int did);

	public void update(DjEntity djEntity);


}
