package com.xiao.service;

import java.util.List;

import com.xiao.entity.DjCountEntity;
import com.xiao.entity.MemberEntity;

public interface MemberService {

	public List<MemberEntity> findAll();

	public int findAllCount();

	public List<MemberEntity> findAllByPager(MemberEntity memberEntity);

	public void delOne(int rid);

	public MemberEntity findOneByRcard(String rcard);

	public void insert(MemberEntity memberEntity);

	public MemberEntity findOneByRid(int rid);

	public void update(MemberEntity memberEntity);

	public List<MemberEntity> findAllByDid(MemberEntity memberEntity);

	public int findCountByDid(int did);

	public List<DjCountEntity> findDjAndCount();

}
