package com.xiao.service;

import java.util.List;

import com.xiao.entity.ServicetypeEntity;

public interface ServicetypeService {

	public List<ServicetypeEntity> findAll();

	public int findAllCount();

	public List<ServicetypeEntity> findAllByPager(ServicetypeEntity servicetypeEntity);

	public void delOne(int sid);

	public ServicetypeEntity findOneBySname(String sname);

	public void insert(ServicetypeEntity servicetypeEntity);

	public ServicetypeEntity findOneBySid(int sid);

	public void update(ServicetypeEntity servicetypeEntity);
}
