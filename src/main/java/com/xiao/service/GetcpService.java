package com.xiao.service;

import java.util.List;

import com.xiao.entity.GetcpEntity;

public interface GetcpService {

	public List<GetcpEntity> findAll(int fid);

	public int findAllCount(int fid);

	public List<GetcpEntity> findAllByPager(GetcpEntity getcpEntity);

	public void insert(GetcpEntity getcpEntity);

	public void update(GetcpEntity getcpEntity);

}
