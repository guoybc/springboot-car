package com.xiao.service;

import java.util.List;

import com.xiao.entity.YouhuiEntity;

public interface YouhuiService {

	public List<YouhuiEntity> findAll();

	public int findAllCount();

	public List<YouhuiEntity> findAllByPager(YouhuiEntity youhuiEntity);

	public void delOne(int yid);

	public YouhuiEntity findOneByYtitle(String ytitle);

	public void insert(YouhuiEntity youhuiEntity);

	public YouhuiEntity findOneByYid(int yid);

	public void update(YouhuiEntity youhuiEntity);

}
