package com.xiao.service;

import java.util.List;

import com.xiao.entity.PzEntity;

public interface PzService {

	public List<PzEntity> findAll();

	public int findAllCount();

	public List<PzEntity> findAllByPager(PzEntity pzEntity);

	public void delOne(int sid);

	public PzEntity findOneByZname(String zname);

	public void insert(PzEntity pzEntity);

	public PzEntity findOneByZid(int sid);

	public void update(PzEntity pzEntity);

}
