package com.xiao.service;

import java.util.List;

import com.xiao.entity.MytfEntity;

public interface MytfService {

	public List<MytfEntity> findAll();

	public int findAllCount();

	public List<MytfEntity> findAllByPager(MytfEntity mytfEntity);

	public MytfEntity findOneByFname(String fname);

	public void insert(MytfEntity mytfEntity);

	public MytfEntity findOneByFid(int fid);

	public void update(MytfEntity mytfEntity);

	public List<MytfEntity> findAllByCid(int cid);

}
