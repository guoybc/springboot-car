package com.xiao.service;

import java.util.List;

import com.xiao.entity.LipinEntity;

public interface LipinService {

	public List<LipinEntity> findAll();

	public int findAllCount();

	public List<LipinEntity> findAllByPager(LipinEntity lipinEntity);

	public void delOne(int nid);

	public void insert(LipinEntity lipinEntity);

	public LipinEntity findOneByNid(int nid);

	public void update(LipinEntity lipinEntity);

}
