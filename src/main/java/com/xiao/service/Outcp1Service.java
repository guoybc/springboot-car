package com.xiao.service;

import java.util.List;

import com.xiao.entity.Outcp1Entity;

public interface Outcp1Service {

	public List<Outcp1Entity> findAll();

	public int findAllCount(Outcp1Entity outcp1Entity);

	public List<Outcp1Entity> findAllByPager(Outcp1Entity outcpEntity);

	public void insert(Outcp1Entity outcpEntity);


}
