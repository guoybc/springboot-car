package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.Outcp1Dao;
import com.xiao.entity.Outcp1Entity;
import com.xiao.service.Outcp1Service;

@Service
public class Outcp1ServiceImpl implements Outcp1Service {

	@Autowired
	private Outcp1Dao outcp1Dao;

	@Override
	public List<Outcp1Entity> findAll() {
		// TODO Auto-generated method stub
		return outcp1Dao.findAll();
	}

	@Override
	public int findAllCount(Outcp1Entity outcpEntity) {
		// TODO Auto-generated method stub
		return outcp1Dao.findAllCount(outcpEntity);
	}

	@Override
	public List<Outcp1Entity> findAllByPager(Outcp1Entity outcpEntity) {
		// TODO Auto-generated method stub
		return outcp1Dao.findAllByPager(outcpEntity);
	}

	@Override
	public void insert(Outcp1Entity outcpEntity) {
		// TODO Auto-generated method stub
		outcp1Dao.insert(outcpEntity);
	}


}
