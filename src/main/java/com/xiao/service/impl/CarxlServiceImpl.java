package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.CarxlDao;
import com.xiao.entity.CarxlEntity;
import com.xiao.service.CarxlService;

@Service
public class CarxlServiceImpl implements CarxlService {

	@Autowired
	private CarxlDao carxlDao;

	@Override
	public List<CarxlEntity> findAll() {
		// TODO Auto-generated method stub
		return carxlDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return carxlDao.findAllCount();
	}

	@Override
	public List<CarxlEntity> findAllByPager(CarxlEntity carxlEntity) {
		// TODO Auto-generated method stub
		return carxlDao.findAllByPager(carxlEntity);
	}

	@Override
	public void delOne(int xid) {
		// TODO Auto-generated method stub
		carxlDao.delOne(xid);
	}

	@Override
	public CarxlEntity findOneByXname(String xname) {
		// TODO Auto-generated method stub
		return carxlDao.findOneByXname(xname);
	}

	@Override
	public void insert(CarxlEntity carxlEntity) {
		// TODO Auto-generated method stub
		carxlDao.insert(carxlEntity);
	}

	@Override
	public CarxlEntity findOneByXid(int xid) {
		// TODO Auto-generated method stub
		return carxlDao.findOneByXid(xid);
	}

	@Override
	public void update(CarxlEntity carxlEntity) {
		// TODO Auto-generated method stub
		carxlDao.update(carxlEntity);
	}

	@Override
	public List<CarxlEntity> findAllByAid(int aid) {
		// TODO Auto-generated method stub
		return carxlDao.findAllByAid(aid);
	}

}
