package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.DjDao;
import com.xiao.entity.DjEntity;
import com.xiao.service.DjService;

@Service
public class DjServiceImpl implements DjService {

	@Autowired
	private DjDao djDao;

	@Override
	public List<DjEntity> findAll() {
		// TODO Auto-generated method stub
		return djDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return djDao.findAllCount();
	}

	@Override
	public List<DjEntity> findAllByPager(DjEntity djEntity) {
		// TODO Auto-generated method stub
		return djDao.findAllByPager(djEntity);
	}

	@Override
	public void delOne(int did) {
		// TODO Auto-generated method stub
		djDao.delOne(did);
	}

	@Override
	public DjEntity findOneByDname(String dname) {
		// TODO Auto-generated method stub
		return djDao.findOneByDname(dname);
	}

	@Override
	public void insert(DjEntity djEntity) {
		// TODO Auto-generated method stub
		djDao.insert(djEntity);
	}

	@Override
	public DjEntity findOneByDid(int did) {
		// TODO Auto-generated method stub
		return djDao.findOneByDid(did);
	}

	@Override
	public void update(DjEntity djEntity) {
		// TODO Auto-generated method stub
		djDao.update(djEntity);
	}

}
