package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.CptypeDao;
import com.xiao.entity.CptypeEntity;
import com.xiao.service.CptypeService;

@Service
public class CptypeServiceImpl implements CptypeService {

	@Autowired
	private CptypeDao cptypeDao;

	@Override
	public List<CptypeEntity> findAll() {
		// TODO Auto-generated method stub
		return cptypeDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return cptypeDao.findAllCount();
	}

	@Override
	public List<CptypeEntity> findAllByPager(CptypeEntity cptypeEntity) {
		// TODO Auto-generated method stub
		return cptypeDao.findAllByPager(cptypeEntity);
	}

	@Override
	public void delOne(int cid) {
		// TODO Auto-generated method stub
		cptypeDao.delOne(cid);
	}

	@Override
	public CptypeEntity findOneByCname(String dname) {
		// TODO Auto-generated method stub
		return cptypeDao.findOneByCname(dname);
	}

	@Override
	public void insert(CptypeEntity cptypeEntity) {
		// TODO Auto-generated method stub
		cptypeDao.insert(cptypeEntity);
	}

	@Override
	public CptypeEntity findOneByCid(int cid) {
		// TODO Auto-generated method stub
		return cptypeDao.findOneByCid(cid);
	}

	@Override
	public void update(CptypeEntity cptypeEntity) {
		// TODO Auto-generated method stub
		cptypeDao.update(cptypeEntity);
	}

}
