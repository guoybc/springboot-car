package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.MytfDao;
import com.xiao.entity.MytfEntity;
import com.xiao.service.MytfService;

@Service
public class MytfServiceImpl implements MytfService {

	@Autowired
	private MytfDao mytfDao;

	@Override
	public List<MytfEntity> findAll() {
		// TODO Auto-generated method stub
		return mytfDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return mytfDao.findAllCount();
	}

	@Override
	public List<MytfEntity> findAllByPager(MytfEntity mytfEntity) {
		// TODO Auto-generated method stub
		return mytfDao.findAllByPager(mytfEntity);
	}

	@Override
	public MytfEntity findOneByFname(String fname) {
		// TODO Auto-generated method stub
		return mytfDao.findOneByFname(fname);
	}

	@Override
	public void insert(MytfEntity mytfEntity) {
		// TODO Auto-generated method stub
		mytfDao.insert(mytfEntity);
	}

	@Override
	public MytfEntity findOneByFid(int fid) {
		// TODO Auto-generated method stub
		return mytfDao.findOneByFid(fid);
	}

	@Override
	public void update(MytfEntity mytfEntity) {
		// TODO Auto-generated method stub
		mytfDao.update(mytfEntity);
	}

	@Override
	public List<MytfEntity> findAllByCid(int cid) {
		// TODO Auto-generated method stub
		return mytfDao.findAllByCid(cid);
	}

}
