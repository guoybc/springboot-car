package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.YouhuiDao;
import com.xiao.entity.YouhuiEntity;
import com.xiao.service.YouhuiService;

@Service
public class YouhuiServiceImpl implements YouhuiService {

	@Autowired
	private YouhuiDao youhuiDao;

	@Override
	public List<YouhuiEntity> findAll() {
		// TODO Auto-generated method stub
		return youhuiDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return youhuiDao.findAllCount();
	}

	@Override
	public List<YouhuiEntity> findAllByPager(YouhuiEntity youhuiEntity) {
		// TODO Auto-generated method stub
		return youhuiDao.findAllByPager(youhuiEntity);
	}

	@Override
	public void delOne(int yid) {
		// TODO Auto-generated method stub
		youhuiDao.delOne(yid);
	}

	@Override
	public YouhuiEntity findOneByYtitle(String ytitle) {
		// TODO Auto-generated method stub
		return youhuiDao.findOneByYtitle(ytitle);
	}

	@Override
	public void insert(YouhuiEntity youhuiEntity) {
		// TODO Auto-generated method stub
		youhuiDao.insert(youhuiEntity);
	}

	@Override
	public YouhuiEntity findOneByYid(int yid) {
		// TODO Auto-generated method stub
		return youhuiDao.findOneByYid(yid);
	}

	@Override
	public void update(YouhuiEntity youhuiEntity) {
		// TODO Auto-generated method stub
		youhuiDao.update(youhuiEntity);
	}




}
