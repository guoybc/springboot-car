package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.UserDao;
import com.xiao.entity.UserEntity;
import com.xiao.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public List<UserEntity> findAll() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return userDao.findAllCount();
	}

	@Override
	public List<UserEntity> findAllByPager(UserEntity userEntity) {
		// TODO Auto-generated method stub
		return userDao.findAllByPager(userEntity);
	}

	@Override
	public void delOne(int uid) {
		// TODO Auto-generated method stub
		userDao.delOne(uid);
	}

	@Override
	public UserEntity findOneByUname(String uname) {
		// TODO Auto-generated method stub
		return userDao.findOneByUname(uname);
	}

	@Override
	public void insert(UserEntity userEntity) {
		// TODO Auto-generated method stub
		userDao.insert(userEntity);
	}

	@Override
	public UserEntity findOneByUid(int uid) {
		// TODO Auto-generated method stub
		return userDao.findOneByUid(uid);
	}

	@Override
	public void update(UserEntity userEntity) {
		// TODO Auto-generated method stub
		userDao.update(userEntity);
	}

	@Override
	public UserEntity finOneByUser(UserEntity userEntity) {
		// TODO Auto-generated method stub
		return userDao.finOneByUser(userEntity);
	}

}
