package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.ServicetypeDao;
import com.xiao.entity.ServicetypeEntity;
import com.xiao.service.ServicetypeService;

@Service
public class ServicetypeServiceImpl implements ServicetypeService {

	@Autowired
	private ServicetypeDao servicetypeDao;

	@Override
	public List<ServicetypeEntity> findAll() {
		// TODO Auto-generated method stub
		return servicetypeDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return servicetypeDao.findAllCount();
	}

	@Override
	public List<ServicetypeEntity> findAllByPager(ServicetypeEntity servicetypeEntity) {
		// TODO Auto-generated method stub
		return servicetypeDao.findAllByPager(servicetypeEntity);
	}

	@Override
	public void delOne(int sid) {
		// TODO Auto-generated method stub
		servicetypeDao.delOne(sid);
	}

	@Override
	public ServicetypeEntity findOneBySname(String sname) {
		// TODO Auto-generated method stub
		return servicetypeDao.findOneBySname(sname);
	}

	@Override
	public void insert(ServicetypeEntity servicetypeEntity) {
		// TODO Auto-generated method stub
		servicetypeDao.insert(servicetypeEntity);
	}

	@Override
	public ServicetypeEntity findOneBySid(int sid) {
		// TODO Auto-generated method stub
		return servicetypeDao.findOneBySid(sid);
	}

	@Override
	public void update(ServicetypeEntity servicetypeEntity) {
		// TODO Auto-generated method stub
		servicetypeDao.update(servicetypeEntity);
	}

}
