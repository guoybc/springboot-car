package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.LipinDao;
import com.xiao.entity.LipinEntity;
import com.xiao.service.LipinService;

@Service
public class LipinServiceImpl implements LipinService {

	@Autowired
	private LipinDao lipinDao;

	@Override
	public List<LipinEntity> findAll() {
		// TODO Auto-generated method stub
		return lipinDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return lipinDao.findAllCount();
	}

	@Override
	public List<LipinEntity> findAllByPager(LipinEntity lipinEntity) {
		// TODO Auto-generated method stub
		return lipinDao.findAllByPager(lipinEntity);
	}

	@Override
	public void delOne(int nid) {
		// TODO Auto-generated method stub
		lipinDao.delOne(nid);
	}

	@Override
	public void insert(LipinEntity lipinEntity) {
		// TODO Auto-generated method stub
		lipinDao.insert(lipinEntity);
	}

	@Override
	public LipinEntity findOneByNid(int nid) {
		// TODO Auto-generated method stub
		return lipinDao.findOneByNid(nid);
	}

	@Override
	public void update(LipinEntity lipinEntity) {
		// TODO Auto-generated method stub
		lipinDao.update(lipinEntity);
	}

}
