package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.JiciDao;
import com.xiao.entity.JiciEntity;
import com.xiao.service.JiciService;

@Service
public class JiciServiceImpl implements JiciService {

	@Autowired
	private JiciDao jiciDao;

	@Override
	public List<JiciEntity> findAll() {
		// TODO Auto-generated method stub
		return jiciDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return jiciDao.findAllCount();
	}

	@Override
	public List<JiciEntity> findAllByPager(JiciEntity JiciEntity) {
		// TODO Auto-generated method stub
		return jiciDao.findAllByPager(JiciEntity);
	}

	@Override
	public void insert(JiciEntity JiciEntity) {
		// TODO Auto-generated method stub
		jiciDao.insert(JiciEntity);
	}

	@Override
	public void update(JiciEntity JiciEntity) {
		// TODO Auto-generated method stub
		jiciDao.update(JiciEntity);
	}

	@Override
	public JiciEntity findOneByJid(int jid) {
		// TODO Auto-generated method stub
		return jiciDao.findOneByJid(jid);
	}



}
