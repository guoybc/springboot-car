package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.OutcpDao;
import com.xiao.entity.OutcpEntity;
import com.xiao.service.OutcpService;

@Service
public class OutcpServiceImpl implements OutcpService {

	@Autowired
	private OutcpDao outcpDao;

	@Override
	public List<OutcpEntity> findAll(int rid) {
		// TODO Auto-generated method stub
		return outcpDao.findAll(rid);
	}

	@Override
	public int findAllCount(int rid) {
		// TODO Auto-generated method stub
		return outcpDao.findAllCount(rid);
	}

	@Override
	public List<OutcpEntity> findAllByPager(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		return outcpDao.findAllByPager(outcpEntity);
	}

	@Override
	public void delOne(int tid) {
		// TODO Auto-generated method stub
		outcpDao.delOne(tid);
	}

	@Override
	public void insert(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		outcpDao.insert(outcpEntity);
	}

	@Override
	public OutcpEntity findOneByTid(int tid) {
		// TODO Auto-generated method stub
		return outcpDao.findOneByTid(tid);
	}

	@Override
	public void update(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		outcpDao.update(outcpEntity);
	}

	@Override
	public OutcpEntity findCountByFid(int fid) {
		// TODO Auto-generated method stub
		return outcpDao.findCountByFid(fid);
	}

	@Override
	public List<OutcpEntity> findAllByRid(int rid) {
		// TODO Auto-generated method stub
		return outcpDao.findAllByRid(rid);
	}

	@Override
	public void updateTflag(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		outcpDao.updateTflag(outcpEntity);
	}

	@Override
	public OutcpEntity findCountByOutcp(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		return outcpDao.findCountByOutcp(outcpEntity);
	}

	@Override
	public void updateTcount(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		outcpDao.updateTcount(outcpEntity);
	}

	@Override
	public int findAllCountByFid(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		return outcpDao.findAllCountByFid(outcpEntity);
	}

	@Override
	public List<OutcpEntity> findAllNoPagerByFid(OutcpEntity outcpEntity) {
		// TODO Auto-generated method stub
		return outcpDao.findAllNoPagerByFid(outcpEntity);
	}

}
