package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.ChongDao;
import com.xiao.entity.ChongEntity;
import com.xiao.service.ChongService;

@Service
public class ChongServiceImpl implements ChongService {

	@Autowired
	private ChongDao chongDao;

	@Override
	public List<ChongEntity> findAll() {
		// TODO Auto-generated method stub
		return chongDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return chongDao.findAllCount();
	}

	@Override
	public List<ChongEntity> findAllByPager(ChongEntity chongEntity) {
		// TODO Auto-generated method stub
		return chongDao.findAllByPager(chongEntity);
	}

	@Override
	public void delOne(int oid) {
		// TODO Auto-generated method stub
		chongDao.delOne(oid);
	}

	@Override
	public void insert(ChongEntity chongEntity) {
		// TODO Auto-generated method stub
		chongDao.insert(chongEntity);
	}

	@Override
	public ChongEntity findOneByOid(int oid) {
		// TODO Auto-generated method stub
		return chongDao.findOneByOid(oid);
	}

	@Override
	public void update(ChongEntity chongEntity) {
		// TODO Auto-generated method stub
		chongDao.update(chongEntity);
	}

}
