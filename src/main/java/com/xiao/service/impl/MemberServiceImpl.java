package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.MemberDao;
import com.xiao.entity.DjCountEntity;
import com.xiao.entity.MemberEntity;
import com.xiao.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {

	@Autowired
	private MemberDao memberDao;

	@Override
	public List<MemberEntity> findAll() {
		// TODO Auto-generated method stub
		return memberDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return memberDao.findAllCount();
	}

	@Override
	public List<MemberEntity> findAllByPager(MemberEntity memberEntity) {
		// TODO Auto-generated method stub
		return memberDao.findAllByPager(memberEntity);
	}

	@Override
	public void delOne(int rid) {
		// TODO Auto-generated method stub
		memberDao.delOne(rid);
	}

	@Override
	public MemberEntity findOneByRcard(String rcard) {
		// TODO Auto-generated method stub
		return memberDao.findOneByRcard(rcard);
	}

	@Override
	public void insert(MemberEntity memberEntity) {
		// TODO Auto-generated method stub
		memberDao.insert(memberEntity);
	}

	@Override
	public MemberEntity findOneByRid(int rid) {
		// TODO Auto-generated method stub
		return memberDao.findOneByRid(rid);
	}

	@Override
	public void update(MemberEntity memberEntity) {
		// TODO Auto-generated method stub
		memberDao.update(memberEntity);
	}

	@Override
	public List<MemberEntity> findAllByDid(MemberEntity memberEntity) {
		// TODO Auto-generated method stub
		return memberDao.findAllByDid(memberEntity);
	}

	@Override
	public int findCountByDid(int did) {
		// TODO Auto-generated method stub
		return memberDao.findCountByDid(did);
	}

	@Override
	public List<DjCountEntity> findDjAndCount() {
		// TODO Auto-generated method stub
		return memberDao.findDjAndCount();
	}

}
