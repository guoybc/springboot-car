package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.PzDao;
import com.xiao.entity.PzEntity;
import com.xiao.service.PzService;

@Service
public class PzServiceImpl implements PzService {

	@Autowired
	private PzDao pzDao;

	@Override
	public List<PzEntity> findAll() {
		// TODO Auto-generated method stub
		return pzDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return pzDao.findAllCount();
	}

	@Override
	public List<PzEntity> findAllByPager(PzEntity pzEntity) {
		// TODO Auto-generated method stub
		return pzDao.findAllByPager(pzEntity);
	}

	@Override
	public void delOne(int sid) {
		// TODO Auto-generated method stub
		pzDao.delOne(sid);
	}

	@Override
	public PzEntity findOneByZname(String zname) {
		// TODO Auto-generated method stub
		return pzDao.findOneByZname(zname);
	}

	@Override
	public void insert(PzEntity pzEntity) {
		// TODO Auto-generated method stub
		pzDao.insert(pzEntity);
	}

	@Override
	public PzEntity findOneByZid(int sid) {
		// TODO Auto-generated method stub
		return pzDao.findOneByZid(sid);
	}

	@Override
	public void update(PzEntity pzEntity) {
		// TODO Auto-generated method stub
		pzDao.update(pzEntity);
	}

}
