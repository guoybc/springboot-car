package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.xiao.dao.CartypeDao;
import com.xiao.entity.CartypeEntity;
import com.xiao.service.CartypeService;

@Service
@Cacheable(cacheNames = "carType")
public class CartypeServiceImpl implements CartypeService {

	@Autowired
	private CartypeDao cartypeDao;

	@Override
	@Cacheable(cacheNames = "carTypeList")
	public List<CartypeEntity> findAll() {
		// TODO Auto-generated method stub
		return cartypeDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return cartypeDao.findAllCount();
	}

	@Override
	public List<CartypeEntity> findAllByPager(CartypeEntity cartypeEntity) {
		// TODO Auto-generated method stub
		return cartypeDao.findAllByPager(cartypeEntity);
	}

	@Override
	public void delOne(int aid) {
		// TODO Auto-generated method stub
		cartypeDao.delOne(aid);
	}

	@Override
	public CartypeEntity findOneByAname(String aname) {
		// TODO Auto-generated method stub
		return cartypeDao.findOneByAname(aname);
	}

	@Override
	@CacheEvict(cacheNames = "carTypeList",allEntries = true)
	public void insert(CartypeEntity cartypeEntity) {
		// TODO Auto-generated method stub
		cartypeDao.insert(cartypeEntity);
	}

	@Override
	public CartypeEntity findOneByAid(int aid) {
		// TODO Auto-generated method stub
		return cartypeDao.findOneByAid(aid);
	}

	@Override
	public void update(CartypeEntity cartypeEntity) {
		// TODO Auto-generated method stub
		cartypeDao.update(cartypeEntity);
	}

}
