package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.GetcpDao;
import com.xiao.entity.GetcpEntity;
import com.xiao.service.GetcpService;

@Service
public class GetcpServiceImpl implements GetcpService {

	@Autowired
	private GetcpDao getcpDao;

	@Override
	public List<GetcpEntity> findAll(int fid) {
		// TODO Auto-generated method stub
		return getcpDao.findAll(fid);
	}

	@Override
	public int findAllCount(int fid) {
		// TODO Auto-generated method stub
		return getcpDao.findAllCount(fid);
	}

	@Override
	public List<GetcpEntity> findAllByPager(GetcpEntity getcpEntity) {
		// TODO Auto-generated method stub
		return getcpDao.findAllByPager(getcpEntity);
	}

	@Override
	public void insert(GetcpEntity getcpEntity) {
		// TODO Auto-generated method stub
		getcpDao.insert(getcpEntity);
	}

	@Override
	public void update(GetcpEntity getcpEntity) {
		// TODO Auto-generated method stub
		getcpDao.update(getcpEntity);
	}

}
