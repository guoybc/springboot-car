package com.xiao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiao.dao.DuihuanDao;
import com.xiao.entity.DuihuanEntity;
import com.xiao.service.DuihuanService;

@Service
public class DuihuanServiceImpl implements DuihuanService {

	@Autowired
	private DuihuanDao duihuanDao;

	@Override
	public List<DuihuanEntity> findAll() {
		// TODO Auto-generated method stub
		return duihuanDao.findAll();
	}

	@Override
	public int findAllCount() {
		// TODO Auto-generated method stub
		return duihuanDao.findAllCount();
	}

	@Override
	public List<DuihuanEntity> findAllByPager(DuihuanEntity duihuanEntity) {
		// TODO Auto-generated method stub
		return duihuanDao.findAllByPager(duihuanEntity);
	}

	@Override
	public void delOne(int hid) {
		// TODO Auto-generated method stub
		duihuanDao.delOne(hid);
	}

	@Override
	public void insert(DuihuanEntity duihuanEntity) {
		// TODO Auto-generated method stub
		duihuanDao.insert(duihuanEntity);
	}

	@Override
	public DuihuanEntity findOneByHid(int hid) {
		// TODO Auto-generated method stub
		return duihuanDao.findOneByHid(hid);
	}

	@Override
	public void update(DuihuanEntity duihuanEntity) {
		// TODO Auto-generated method stub
		duihuanDao.update(duihuanEntity);
	}

}
