package com.xiao.service;

import java.util.List;

import com.xiao.entity.ChongEntity;

public interface ChongService {

	public List<ChongEntity> findAll();

	public int findAllCount();

	public List<ChongEntity> findAllByPager(ChongEntity chongEntity);

	public void delOne(int oid);

	public void insert(ChongEntity chongEntity);

	public ChongEntity findOneByOid(int oid);

	public void update(ChongEntity chongEntity);

}
