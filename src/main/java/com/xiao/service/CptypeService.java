package com.xiao.service;

import java.util.List;

import com.xiao.entity.CptypeEntity;

public interface CptypeService {

	public List<CptypeEntity> findAll();

	public int findAllCount();

	public List<CptypeEntity> findAllByPager(CptypeEntity cptypeEntity);

	public void delOne(int cid);

	public CptypeEntity findOneByCname(String dname);

	public void insert(CptypeEntity cptypeEntity);

	public CptypeEntity findOneByCid(int cid);

	public void update(CptypeEntity cptypeEntity);

}
