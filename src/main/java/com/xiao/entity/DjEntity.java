package com.xiao.entity;

import java.io.Serializable;

public class DjEntity extends PagerEntity implements Serializable {

	private int did;
	private String dname;
	private Double djf;
	private Double dmoneyBl;
	private Double dzk;

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public Double getDjf() {
		return djf;
	}

	public void setDjf(Double djf) {
		this.djf = djf;
	}

	public Double getDmoneyBl() {
		return dmoneyBl;
	}

	public void setDmoneyBl(Double dmoneyBl) {
		this.dmoneyBl = dmoneyBl;
	}

	public Double getDzk() {
		return dzk;
	}

	public void setDzk(Double dzk) {
		this.dzk = dzk;
	}

	@Override
	public String toString() {
		return "DjEntity [did=" + did + ", dname=" + dname + ", djf=" + djf + ", dmoneyBl=" + dmoneyBl + ", dzk=" + dzk
				+ "]";
	}

}
