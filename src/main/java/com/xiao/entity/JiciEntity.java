package com.xiao.entity;

import java.io.Serializable;

public class JiciEntity extends PagerEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7798677788639208836L;

	private int jid;

	private int rid;
	private String rcard;
	private String rname;

	private String jtime;
	private int sid;
	private String sname;

	private Float jmoney;

	private int uid;
	private String urealname;

	public int getJid() {
		return jid;
	}

	public void setJid(int jid) {
		this.jid = jid;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getRcard() {
		return rcard;
	}

	public void setRcard(String rcard) {
		this.rcard = rcard;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public String getJtime() {
		return jtime;
	}

	public void setJtime(String jtime) {
		this.jtime = jtime;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Float getJmoney() {
		return jmoney;
	}

	public void setJmoney(Float jmoney) {
		this.jmoney = jmoney;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	@Override
	public String toString() {
		return "JiciEntity [jid=" + jid + ", rid=" + rid + ", rcard=" + rcard + ", rname=" + rname + ", jtime=" + jtime
				+ ", sid=" + sid + ", sname=" + sname + ", jmoney=" + jmoney + ", uid=" + uid + ", urealname="
				+ urealname + "]";
	}

}
