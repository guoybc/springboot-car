package com.xiao.entity;

import java.io.Serializable;

public class PzEntity extends PagerEntity implements Serializable {

	private int zid;
	private String zname;

	public int getZid() {
		return zid;
	}

	public void setZid(int zid) {
		this.zid = zid;
	}

	public String getZname() {
		return zname;
	}

	public void setZname(String zname) {
		this.zname = zname;
	}

	@Override
	public String toString() {
		return "PzEntity [zid=" + zid + ", zname=" + zname + "]";
	}

}
