package com.xiao.entity;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class LipinEntity extends PagerEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5293272744035692698L;

	private int nid;
	private String nname;
	private String nimg;
	private MultipartFile fileimg;
	private int njf;
	private int ncount;
	private int nncount;

	public int getNid() {
		return nid;
	}

	public void setNid(int nid) {
		this.nid = nid;
	}

	public String getNname() {
		return nname;
	}

	public void setNname(String nname) {
		this.nname = nname;
	}

	public String getNimg() {
		return nimg;
	}

	public void setNimg(String nimg) {
		this.nimg = nimg;
	}

	public MultipartFile getFileimg() {
		return fileimg;
	}

	public void setFileimg(MultipartFile fileimg) {
		this.fileimg = fileimg;
	}

	public int getNjf() {
		return njf;
	}

	public void setNjf(int njf) {
		this.njf = njf;
	}

	public int getNcount() {
		return ncount;
	}

	public void setNcount(int ncount) {
		this.ncount = ncount;
	}

	public int getNncount() {
		return nncount;
	}

	public void setNncount(int nncount) {
		this.nncount = nncount;
	}

	@Override
	public String toString() {
		return "LipinEntity [nid=" + nid + ", nname=" + nname + ", nimg=" + nimg + ", fileimg=" + fileimg + ", njf=" + njf
				+ ", ncount=" + ncount + ", nncount=" + nncount + "]";
	}

}
