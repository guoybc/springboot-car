package com.xiao.entity;

import java.io.Serializable;

public class UserEntity extends PagerEntity implements Serializable {

	private int uid;
	private String uname;
	private String upsw;
	private String urealname;
	private String utel;
	private int usex;

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpsw() {
		return upsw;
	}

	public void setUpsw(String upsw) {
		this.upsw = upsw;
	}

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	public String getUtel() {
		return utel;
	}

	public void setUtel(String utel) {
		this.utel = utel;
	}

	public int getUsex() {
		return usex;
	}

	public void setUsex(int usex) {
		this.usex = usex;
	}

	@Override
	public String toString() {
		return "UserEntity [uid=" + uid + ", uname=" + uname + ", upsw=" + upsw + ", urealname=" + urealname + ", utel="
				+ utel + ", usex=" + usex + "]";
	}

}
