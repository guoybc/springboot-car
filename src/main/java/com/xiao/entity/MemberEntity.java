package com.xiao.entity;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class MemberEntity extends PagerEntity implements Serializable {

	private Integer rid;
	private String rcard;
	private String rname;
	private String rpsw;
	private String rimg;
	private String rtel;
	private String rsex;
	private String rbirthday;
	private Integer rstatus;
	private Double rjf;
	private String rcarnum;
	private String rcolor;
	private Float rway;
	private String rnum;
	private String raddress;
	private String rremark;
	private String rtime;
	private Double rmoney;
	private int rflag;

	private MultipartFile fileimg;

	// 会员等级
	private int did;
	private String dname;
	private Double djf;
	private Double dmoneyBl;
	private Double dzk;

	// 汽车系列
	private int xid;
	private int aid;
	private String xname;

	// 汽车系列
	private String aname;

	// 凭证
	private int zid;
	private String zname;

	public int getRflag() {
		return rflag;
	}

	public void setRflag(int rflag) {
		this.rflag = rflag;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	public Integer getRid() {
		return rid;
	}

	public void setRid(Integer rid) {
		this.rid = rid;
	}

	public String getRcard() {
		return rcard;
	}

	public void setRcard(String rcard) {
		this.rcard = rcard;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public String getRpsw() {
		return rpsw;
	}

	public void setRpsw(String rpsw) {
		this.rpsw = rpsw;
	}

	public String getRimg() {
		return rimg;
	}

	public void setRimg(String rimg) {
		this.rimg = rimg;
	}

	public String getRtel() {
		return rtel;
	}

	public void setRtel(String rtel) {
		this.rtel = rtel;
	}

	public String getRsex() {
		return rsex;
	}

	public void setRsex(String rsex) {
		this.rsex = rsex;
	}

	public String getRbirthday() {
		return rbirthday;
	}

	public void setRbirthday(String rbirthday) {
		this.rbirthday = rbirthday;
	}

	public Integer getRstatus() {
		return rstatus;
	}

	public void setRstatus(Integer rstatus) {
		this.rstatus = rstatus;
	}

	public Double getRjf() {
		return rjf;
	}

	public void setRjf(Double rjf) {
		this.rjf = rjf;
	}

	public String getRcarnum() {
		return rcarnum;
	}

	public void setRcarnum(String rcarnum) {
		this.rcarnum = rcarnum;
	}

	public String getRcolor() {
		return rcolor;
	}

	public void setRcolor(String rcolor) {
		this.rcolor = rcolor;
	}

	public Float getRway() {
		return rway;
	}

	public void setRway(Float rway) {
		this.rway = rway;
	}

	public String getRnum() {
		return rnum;
	}

	public void setRnum(String rnum) {
		this.rnum = rnum;
	}

	public String getRaddress() {
		return raddress;
	}

	public void setRaddress(String raddress) {
		this.raddress = raddress;
	}

	public String getRremark() {
		return rremark;
	}

	public void setRremark(String rremark) {
		this.rremark = rremark;
	}

	public String getRtime() {
		return rtime;
	}

	public void setRtime(String rtime) {
		this.rtime = rtime;
	}

	public Double getRmoney() {
		return rmoney;
	}

	public void setRmoney(Double rmoney) {
		this.rmoney = rmoney;
	}

	public MultipartFile getFileimg() {
		return fileimg;
	}

	public void setFileimg(MultipartFile fileimg) {
		this.fileimg = fileimg;
	}

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public Double getDjf() {
		return djf;
	}

	public void setDjf(Double djf) {
		this.djf = djf;
	}

	public Double getDmoneyBl() {
		return dmoneyBl;
	}

	public void setDmoneyBl(Double dmoneyBl) {
		this.dmoneyBl = dmoneyBl;
	}

	public Double getDzk() {
		return dzk;
	}

	public void setDzk(Double dzk) {
		this.dzk = dzk;
	}

	public int getXid() {
		return xid;
	}

	public void setXid(int xid) {
		this.xid = xid;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getXname() {
		return xname;
	}

	public void setXname(String xname) {
		this.xname = xname;
	}

	public int getZid() {
		return zid;
	}

	public void setZid(int zid) {
		this.zid = zid;
	}

	public String getZname() {
		return zname;
	}

	public void setZname(String zname) {
		this.zname = zname;
	}

	@Override
	public String toString() {
		return "MemberEntity [rid=" + rid + ", rcard=" + rcard + ", rname=" + rname + ", rpsw=" + rpsw + ", rimg="
				+ rimg + ", rtel=" + rtel + ", rsex=" + rsex + ", rbirthday=" + rbirthday + ", rstatus=" + rstatus
				+ ", rjf=" + rjf + ", rcarnum=" + rcarnum + ", rcolor=" + rcolor + ", rway=" + rway + ", rnum=" + rnum
				+ ", raddress=" + raddress + ", rremark=" + rremark + ", rtime=" + rtime + ", rmoney=" + rmoney
				+ ", rflag=" + rflag + ", fileimg=" + fileimg + ", did=" + did + ", dname=" + dname + ", djf=" + djf
				+ ", dmoneyBl=" + dmoneyBl + ", dzk=" + dzk + ", xid=" + xid + ", aid=" + aid + ", xname=" + xname
				+ ", aname=" + aname + ", zid=" + zid + ", zname=" + zname + "]";
	}

}
