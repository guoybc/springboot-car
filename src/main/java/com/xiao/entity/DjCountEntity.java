package com.xiao.entity;

import java.io.Serializable;

public class DjCountEntity implements Serializable {

	private int count;
	private String dname;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	@Override
	public String toString() {
		return "DjCountEntity [count=" + count + ", dname=" + dname + "]";
	}

}
