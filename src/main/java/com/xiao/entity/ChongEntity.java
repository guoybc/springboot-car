package com.xiao.entity;

import java.io.Serializable;

public class ChongEntity extends PagerEntity implements Serializable {

	private int oid;
	private Float omoney;
	private Float osmoney;
	private Float olastmoney;
	private String otime;
	private String oremark;

	// 优惠活动
	private int yid;
	private String ytitle;
	private String ybegintime;
	private String yendtime;
	private Double ymoney;
	private Double ylessmoney;

	// 操作人信息
	private int uid;
	private String uname;
	private String urealname;

	// 会员信息
	private int rid;
	private String rcard;
	private String rname;
	private String rpsw;
	private String rimg;
	private String rtel;
	private String rsex;
	private String rbirthday;
	private Integer rstatus;
	private Double rjf;
	private String rcarnum;
	private String rcolor;
	private Float rway;
	private String rnum;
	private String raddress;
	private String rremark;
	private String rtime;
	private Double rmoney;
	private int rflag;

	public String getOremark() {
		return oremark;
	}

	public void setOremark(String oremark) {
		this.oremark = oremark;
	}

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public Float getOmoney() {
		return omoney;
	}

	public void setOmoney(Float omoney) {
		this.omoney = omoney;
	}

	public Float getOsmoney() {
		return osmoney;
	}

	public void setOsmoney(Float osmoney) {
		this.osmoney = osmoney;
	}

	public Float getOlastmoney() {
		return olastmoney;
	}

	public void setOlastmoney(Float olastmoney) {
		this.olastmoney = olastmoney;
	}

	public String getOtime() {
		return otime;
	}

	public void setOtime(String otime) {
		this.otime = otime;
	}

	public int getYid() {
		return yid;
	}

	public void setYid(int yid) {
		this.yid = yid;
	}

	public String getYtitle() {
		return ytitle;
	}

	public void setYtitle(String ytitle) {
		this.ytitle = ytitle;
	}

	public String getYbegintime() {
		return ybegintime;
	}

	public void setYbegintime(String ybegintime) {
		this.ybegintime = ybegintime;
	}

	public String getYendtime() {
		return yendtime;
	}

	public void setYendtime(String yendtime) {
		this.yendtime = yendtime;
	}

	public Double getYmoney() {
		return ymoney;
	}

	public void setYmoney(Double ymoney) {
		this.ymoney = ymoney;
	}

	public Double getYlessmoney() {
		return ylessmoney;
	}

	public void setYlessmoney(Double ylessmoney) {
		this.ylessmoney = ylessmoney;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getRcard() {
		return rcard;
	}

	public void setRcard(String rcard) {
		this.rcard = rcard;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public String getRpsw() {
		return rpsw;
	}

	public void setRpsw(String rpsw) {
		this.rpsw = rpsw;
	}

	public String getRimg() {
		return rimg;
	}

	public void setRimg(String rimg) {
		this.rimg = rimg;
	}

	public String getRtel() {
		return rtel;
	}

	public void setRtel(String rtel) {
		this.rtel = rtel;
	}

	public String getRsex() {
		return rsex;
	}

	public void setRsex(String rsex) {
		this.rsex = rsex;
	}

	public String getRbirthday() {
		return rbirthday;
	}

	public void setRbirthday(String rbirthday) {
		this.rbirthday = rbirthday;
	}

	public Integer getRstatus() {
		return rstatus;
	}

	public void setRstatus(Integer rstatus) {
		this.rstatus = rstatus;
	}

	public Double getRjf() {
		return rjf;
	}

	public void setRjf(Double rjf) {
		this.rjf = rjf;
	}

	public String getRcarnum() {
		return rcarnum;
	}

	public void setRcarnum(String rcarnum) {
		this.rcarnum = rcarnum;
	}

	public String getRcolor() {
		return rcolor;
	}

	public void setRcolor(String rcolor) {
		this.rcolor = rcolor;
	}

	public Float getRway() {
		return rway;
	}

	public void setRway(Float rway) {
		this.rway = rway;
	}

	public String getRnum() {
		return rnum;
	}

	public void setRnum(String rnum) {
		this.rnum = rnum;
	}

	public String getRaddress() {
		return raddress;
	}

	public void setRaddress(String raddress) {
		this.raddress = raddress;
	}

	public String getRremark() {
		return rremark;
	}

	public void setRremark(String rremark) {
		this.rremark = rremark;
	}

	public String getRtime() {
		return rtime;
	}

	public void setRtime(String rtime) {
		this.rtime = rtime;
	}

	public Double getRmoney() {
		return rmoney;
	}

	public void setRmoney(Double rmoney) {
		this.rmoney = rmoney;
	}

	public int getRflag() {
		return rflag;
	}

	public void setRflag(int rflag) {
		this.rflag = rflag;
	}

	@Override
	public String toString() {
		return "ChongEntity [oid=" + oid + ", omoney=" + omoney + ", osmoney=" + osmoney + ", olastmoney=" + olastmoney
				+ ", otime=" + otime + ", oremark=" + oremark + ", yid=" + yid + ", ytitle=" + ytitle + ", ybegintime="
				+ ybegintime + ", yendtime=" + yendtime + ", ymoney=" + ymoney + ", ylessmoney=" + ylessmoney + ", uid="
				+ uid + ", uname=" + uname + ", urealname=" + urealname + ", rid=" + rid + ", rcard=" + rcard
				+ ", rname=" + rname + ", rpsw=" + rpsw + ", rimg=" + rimg + ", rtel=" + rtel + ", rsex=" + rsex
				+ ", rbirthday=" + rbirthday + ", rstatus=" + rstatus + ", rjf=" + rjf + ", rcarnum=" + rcarnum
				+ ", rcolor=" + rcolor + ", rway=" + rway + ", rnum=" + rnum + ", raddress=" + raddress + ", rremark="
				+ rremark + ", rtime=" + rtime + ", rmoney=" + rmoney + ", rflag=" + rflag + "]";
	}

}
