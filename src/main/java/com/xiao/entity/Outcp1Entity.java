package com.xiao.entity;

import java.io.Serializable;

public class Outcp1Entity extends PagerEntity implements Serializable {

	private Integer wid;

	private Integer fid;
	private String fname;
	private Integer cid;
	private String cname;

	private Integer wcount;
	private String wname;
	private String wtel;
	private Integer uid;
	private String wtime;

	private String urealname;

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public Integer getWid() {
		return wid;
	}

	public void setWid(Integer wid) {
		this.wid = wid;
	}

	public Integer getFid() {
		return fid;
	}

	public void setFid(Integer fid) {
		this.fid = fid;
	}

	public Integer getWcount() {
		return wcount;
	}

	public void setWcount(Integer wcount) {
		this.wcount = wcount;
	}

	public String getWname() {
		return wname;
	}

	public void setWname(String wname) {
		this.wname = wname;
	}

	public String getWtel() {
		return wtel;
	}

	public void setWtel(String wtel) {
		this.wtel = wtel;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getWtime() {
		return wtime;
	}

	public void setWtime(String wtime) {
		this.wtime = wtime;
	}

	@Override
	public String toString() {
		return "Outcp1Entity [wid=" + wid + ", fid=" + fid + ", fname=" + fname + ", cid=" + cid + ", cname=" + cname
				+ ", wcount=" + wcount + ", wname=" + wname + ", wtel=" + wtel + ", uid=" + uid + ", wtime=" + wtime
				+ ", urealname=" + urealname + "]";
	}

}
