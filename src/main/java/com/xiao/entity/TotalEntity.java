package com.xiao.entity;

import java.io.Serializable;

public class TotalEntity implements Serializable {

	private Double preTotal;
	private String dname;
	private Double dzk;
	private Double endTotal;

	public Double getDzk() {
		return dzk;
	}

	public void setDzk(Double dzk) {
		this.dzk = dzk;
	}

	public Double getPreTotal() {
		return preTotal;
	}

	public void setPreTotal(Double preTotal) {
		this.preTotal = preTotal;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public Double getEndTotal() {
		return endTotal;
	}

	public void setEndTotal(Double endTotal) {
		this.endTotal = endTotal;
	}

	@Override
	public String toString() {
		return "TotalEntity [preTotal=" + preTotal + ", dname=" + dname + ", dzk=" + dzk + ", endTotal=" + endTotal
				+ "]";
	}

}
