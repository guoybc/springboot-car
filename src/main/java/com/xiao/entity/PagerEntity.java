package com.xiao.entity;

import java.io.Serializable;

public class PagerEntity implements Serializable {

	private int begin;
	private int pages;
	private String sort;
	private String order;

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	@Override
	public String toString() {
		return "PagerEntity [begin=" + begin + ", pages=" + pages + ", sort=" + sort + ", order=" + order + "]";
	}

}
