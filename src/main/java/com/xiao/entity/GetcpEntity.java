package com.xiao.entity;

import java.io.Serializable;

public class GetcpEntity extends PagerEntity implements Serializable {

	private int gid;
	private int gcount;
	private String gtime;

	private int fid;
	private String fname;
	private String fdw;
	private String faddress;
	private Float foutprice;
	private Float Finprice;
	private String fimg;
	private int fcount;

	private int cid;
	private String cname;

	private int uid;
	private String urealname;

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public int getGcount() {
		return gcount;
	}

	public void setGcount(int gcount) {
		this.gcount = gcount;
	}

	public String getGtime() {
		return gtime;
	}

	public void setGtime(String gtime) {
		this.gtime = gtime;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getFdw() {
		return fdw;
	}

	public void setFdw(String fdw) {
		this.fdw = fdw;
	}

	public String getFaddress() {
		return faddress;
	}

	public void setFaddress(String faddress) {
		this.faddress = faddress;
	}

	public Float getFoutprice() {
		return foutprice;
	}

	public void setFoutprice(Float foutprice) {
		this.foutprice = foutprice;
	}

	public Float getFinprice() {
		return Finprice;
	}

	public void setFinprice(Float finprice) {
		Finprice = finprice;
	}

	public String getFimg() {
		return fimg;
	}

	public void setFimg(String fimg) {
		this.fimg = fimg;
	}

	public int getFcount() {
		return fcount;
	}

	public void setFcount(int fcount) {
		this.fcount = fcount;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	@Override
	public String toString() {
		return "GetcpEntity [gid=" + gid + ", gcount=" + gcount + ", gtime=" + gtime + ", fid=" + fid + ", fname="
				+ fname + ", fdw=" + fdw + ", faddress=" + faddress + ", foutprice=" + foutprice + ", Finprice="
				+ Finprice + ", fimg=" + fimg + ", fcount=" + fcount + ", cid=" + cid + ", cname=" + cname + ", uid="
				+ uid + ", urealname=" + urealname + "]";
	}

}
