package com.xiao.entity;

import java.io.Serializable;

public class YouhuiEntity extends PagerEntity implements Serializable {

	private int yid;
	private String ytitle;
	private String ybegintime;
	private String yendtime;
	private Double ymoney;
	private Double ylessmoney;

	public int getYid() {
		return yid;
	}

	public void setYid(int yid) {
		this.yid = yid;
	}

	public String getYbegintime() {
		return ybegintime;
	}

	public void setYbegintime(String ybegintime) {
		this.ybegintime = ybegintime;
	}

	public String getYendtime() {
		return yendtime;
	}

	public void setYendtime(String yendtime) {
		this.yendtime = yendtime;
	}

	public String getYtitle() {
		return ytitle;
	}

	public void setYtitle(String ytitle) {
		this.ytitle = ytitle;
	}

	public Double getYmoney() {
		return ymoney;
	}

	public void setYmoney(Double ymoney) {
		this.ymoney = ymoney;
	}

	public Double getYlessmoney() {
		return ylessmoney;
	}

	public void setYlessmoney(Double ylessmoney) {
		this.ylessmoney = ylessmoney;
	}

	@Override
	public String toString() {
		return "YouhuiEntity [yid=" + yid + ", ytitle=" + ytitle + ", ybegintime=" + ybegintime + ", yendtime="
				+ yendtime + ", ymoney=" + ymoney + ", ylessmoney=" + ylessmoney + "]";
	}

}
