package com.xiao.entity;

import java.io.Serializable;

public class DuihuanEntity extends PagerEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7048949896605960208L;

	private int hid;

	private int nid;
	private String nname;
	private int nncount;

	private int rid;
	private String rcard;
	private String rname;
	private String rjf;
	private String rrjf;// 剩余积分;

	private int hcount;
	private String htime;

	private int uid;
	private String urealname;

	public int getHid() {
		return hid;
	}

	public void setHid(int hid) {
		this.hid = hid;
	}

	public int getNid() {
		return nid;
	}

	public void setNid(int nid) {
		this.nid = nid;
	}

	public String getNname() {
		return nname;
	}

	public void setNname(String nname) {
		this.nname = nname;
	}

	public int getNncount() {
		return nncount;
	}

	public void setNncount(int nncount) {
		this.nncount = nncount;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getRcard() {
		return rcard;
	}

	public void setRcard(String rcard) {
		this.rcard = rcard;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public int getHcount() {
		return hcount;
	}

	public void setHcount(int hcount) {
		this.hcount = hcount;
	}

	public String getHtime() {
		return htime;
	}

	public void setHtime(String htime) {
		this.htime = htime;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRjf() {
		return rjf;
	}

	public void setRjf(String rjf) {
		this.rjf = rjf;
	}

	public String getRrjf() {
		return rrjf;
	}

	public void setRrjf(String rrjf) {
		this.rrjf = rrjf;
	}

	@Override
	public String toString() {
		return "DuihuanEntity [hid=" + hid + ", nid=" + nid + ", nname=" + nname + ", nncount=" + nncount + ", rid="
				+ rid + ", rcard=" + rcard + ", rname=" + rname + ", rjf=" + rjf + ", rrjf=" + rrjf + ", hcount="
				+ hcount + ", htime=" + htime + ", uid=" + uid + ", urealname=" + urealname + "]";
	}

}
