package com.xiao.entity;

import java.io.Serializable;
import java.util.Arrays;

public class OutcpEntity extends PagerEntity implements Serializable {

	private Integer tid;
	private Integer tcount;
	private String ttime;
	private Integer tflag;

	private Integer rid;

	private Integer fid;
	private String fname;
	private String fdw;
	private String faddress;
	private Float foutprice;
	private Float finprice;
	private String fimg;
	private Integer fcount;

	private Integer cid;
	private String cname;

	private Integer uid;
	private String urealname;

	private Double endTotal;
	private Double dzk;

	private Integer[] arfid;
	private Integer[] artcount;

	private String rname;
	private String rcard;

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public String getRcard() {
		return rcard;
	}

	public void setRcard(String rcard) {
		this.rcard = rcard;
	}

	public Integer[] getArfid() {
		return arfid;
	}

	public Double getDzk() {
		return dzk;
	}

	public void setDzk(Double dzk) {
		this.dzk = dzk;
	}

	public void setArfid(Integer[] arfid) {
		this.arfid = arfid;
	}

	public Integer[] getArtcount() {
		return artcount;
	}

	public void setArtcount(Integer[] artcount) {
		this.artcount = artcount;
	}

	public Double getEndTotal() {
		return endTotal;
	}

	public void setEndTotal(Double endTotal) {
		this.endTotal = endTotal;
	}

	public Integer getRid() {
		return rid;
	}

	public void setRid(Integer rid) {
		this.rid = rid;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public Integer getTcount() {
		return tcount;
	}

	public void setTcount(Integer tcount) {
		this.tcount = tcount;
	}

	public String getTtime() {
		return ttime;
	}

	public void setTtime(String ttime) {
		this.ttime = ttime;
	}

	public Integer getTflag() {
		return tflag;
	}

	public void setTflag(Integer tflag) {
		this.tflag = tflag;
	}

	public Integer getFid() {
		return fid;
	}

	public void setFid(Integer fid) {
		this.fid = fid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getFdw() {
		return fdw;
	}

	public void setFdw(String fdw) {
		this.fdw = fdw;
	}

	public String getFaddress() {
		return faddress;
	}

	public void setFaddress(String faddress) {
		this.faddress = faddress;
	}

	public Float getFoutprice() {
		return foutprice;
	}

	public void setFoutprice(Float foutprice) {
		this.foutprice = foutprice;
	}

	public Float getFinprice() {
		return finprice;
	}

	public void setFinprice(Float finprice) {
		this.finprice = finprice;
	}

	public String getFimg() {
		return fimg;
	}

	public void setFimg(String fimg) {
		this.fimg = fimg;
	}

	public Integer getFcount() {
		return fcount;
	}

	public void setFcount(Integer fcount) {
		this.fcount = fcount;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUrealname() {
		return urealname;
	}

	public void setUrealname(String urealname) {
		this.urealname = urealname;
	}

	@Override
	public String toString() {
		return "OutcpEntity [tid=" + tid + ", tcount=" + tcount + ", ttime=" + ttime + ", tflag=" + tflag + ", rid="
				+ rid + ", fid=" + fid + ", fname=" + fname + ", fdw=" + fdw + ", faddress=" + faddress + ", foutprice="
				+ foutprice + ", finprice=" + finprice + ", fimg=" + fimg + ", fcount=" + fcount + ", cid=" + cid
				+ ", cname=" + cname + ", uid=" + uid + ", urealname=" + urealname + ", endTotal=" + endTotal + ", dzk="
				+ dzk + ", arfid=" + Arrays.toString(arfid) + ", artcount=" + Arrays.toString(artcount) + ", rname="
				+ rname + ", rcard=" + rcard + "]";
	}

}
