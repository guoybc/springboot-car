package com.xiao.entity;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class MytfEntity extends PagerEntity implements Serializable {

	private int fid;
	private String fname;
	private String fdw;
	private String faddress;
	private Float foutprice;
	private Float Finprice;
	private String fimg;
	private int fcount;
	private MultipartFile fileimg;

	private int cid;
	private String cname;

	public MultipartFile getFileimg() {
		return fileimg;
	}

	public void setFileimg(MultipartFile fileimg) {
		this.fileimg = fileimg;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getFdw() {
		return fdw;
	}

	public void setFdw(String fdw) {
		this.fdw = fdw;
	}

	public String getFaddress() {
		return faddress;
	}

	public void setFaddress(String faddress) {
		this.faddress = faddress;
	}

	public Float getFoutprice() {
		return foutprice;
	}

	public void setFoutprice(Float foutprice) {
		this.foutprice = foutprice;
	}

	public Float getFinprice() {
		return Finprice;
	}

	public void setFinprice(Float finprice) {
		Finprice = finprice;
	}

	public String getFimg() {
		return fimg;
	}

	public void setFimg(String fimg) {
		this.fimg = fimg;
	}

	public int getFcount() {
		return fcount;
	}

	public void setFcount(int fcount) {
		this.fcount = fcount;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	@Override
	public String toString() {
		return "MytfEntity [fid=" + fid + ", fname=" + fname + ", fdw=" + fdw + ", faddress=" + faddress
				+ ", foutprice=" + foutprice + ", Finprice=" + Finprice + ", fimg=" + fimg + ", fcount=" + fcount
				+ ", fileimg=" + fileimg + ", cid=" + cid + ", cname=" + cname + "]";
	}

}
