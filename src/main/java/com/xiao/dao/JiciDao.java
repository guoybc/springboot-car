package com.xiao.dao;

import java.util.List;

import com.xiao.entity.JiciEntity;

public interface JiciDao {

	public List<JiciEntity> findAll();

	public int findAllCount();

	public List<JiciEntity> findAllByPager(JiciEntity JiciEntity);

	public void insert(JiciEntity JiciEntity);

	public void update(JiciEntity JiciEntity);

	public JiciEntity findOneByJid(int jid);

}
