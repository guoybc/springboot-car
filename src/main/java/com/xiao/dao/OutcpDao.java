package com.xiao.dao;

import java.util.List;

import com.xiao.entity.OutcpEntity;

public interface OutcpDao {

	public List<OutcpEntity> findAll(int rid);

	public int findAllCount(int rid);

	public List<OutcpEntity> findAllByPager(OutcpEntity outcpEntity);

	public void delOne(int tid);

	public void insert(OutcpEntity outcpEntity);

	public OutcpEntity findOneByTid(int tid);

	public void update(OutcpEntity outcpEntity);

	public OutcpEntity findCountByFid(int fid);

	public List<OutcpEntity> findAllByRid(int rid);

	public void updateTflag(OutcpEntity outcpEntity);

	public OutcpEntity findCountByOutcp(OutcpEntity outcpEntity);

	public void updateTcount(OutcpEntity outcpEntity);

	public int findAllCount(OutcpEntity outcpEntity);

	public List<OutcpEntity> findAllNoPagerByFid(OutcpEntity outcpEntity);

	public int findAllCountByFid(OutcpEntity outcpEntity);

}
