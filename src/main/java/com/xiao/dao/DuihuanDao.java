package com.xiao.dao;

import java.util.List;

import com.xiao.entity.DuihuanEntity;

public interface DuihuanDao {

	public List<DuihuanEntity> findAll();

	public int findAllCount();

	public List<DuihuanEntity> findAllByPager(DuihuanEntity duihuanEntity);

	public void delOne(int hid);

	public void insert(DuihuanEntity duihuanEntity);

	public DuihuanEntity findOneByHid(int hid);

	public void update(DuihuanEntity duihuanEntity);

}
