package com.xiao.dao;

import java.util.List;

import com.xiao.entity.CartypeEntity;

public interface CartypeDao {

	public List<CartypeEntity> findAll();

	public int findAllCount();

	public List<CartypeEntity> findAllByPager(CartypeEntity cartypeEntity);

	public void delOne(int aid);

	public CartypeEntity findOneByAname(String aname);

	public void insert(CartypeEntity cartypeEntity);

	public CartypeEntity findOneByAid(int aid);

	public void update(CartypeEntity cartypeEntity);

}
