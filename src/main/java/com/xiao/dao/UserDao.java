package com.xiao.dao;

import java.util.List;

import com.xiao.entity.UserEntity;

public interface UserDao {

	public List<UserEntity> findAll();

	public int findAllCount();

	public List<UserEntity> findAllByPager(UserEntity userEntity);

	public void delOne(int uid);

	public UserEntity findOneByUname(String uname);

	public void insert(UserEntity userEntity);

	public UserEntity findOneByUid(int uid);

	public void update(UserEntity userEntity);

	public UserEntity finOneByUser(UserEntity userEntity);

}
