package com.xiao.dao;

import java.util.List;

import com.xiao.entity.Outcp1Entity;

public interface Outcp1Dao {

	public List<Outcp1Entity> findAll();

	public int findAllCount(Outcp1Entity outcpEntity);

	public List<Outcp1Entity> findAllByPager(Outcp1Entity outcpEntity);

	public void insert(Outcp1Entity outcpEntity);


}
