package com.xiao.dao;

import java.util.List;

import com.xiao.entity.LipinEntity;

public interface LipinDao {

	public List<LipinEntity> findAll();

	public int findAllCount();

	public List<LipinEntity> findAllByPager(LipinEntity lipinEntity);

	public void delOne(int nid);

	public void insert(LipinEntity lipinEntity);

	public LipinEntity findOneByNid(int nid);

	public void update(LipinEntity lipinEntity);

}
