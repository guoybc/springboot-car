package com.xiao.dao;

import java.util.List;

import com.xiao.entity.ServicetypeEntity;

public interface ServicetypeDao {

	public List<ServicetypeEntity> findAll();

	public int findAllCount();

	public List<ServicetypeEntity> findAllByPager(ServicetypeEntity servicetypeEntity);

	public void delOne(int sid);

	public ServicetypeEntity findOneBySname(String sname);

	public void insert(ServicetypeEntity servicetypeEntity);

	public ServicetypeEntity findOneBySid(int sid);

	public void update(ServicetypeEntity servicetypeEntity);

}
