package com.xiao.dao;

import java.util.List;

import com.xiao.entity.CarxlEntity;

public interface CarxlDao {

	public List<CarxlEntity> findAll();

	public int findAllCount();

	public List<CarxlEntity> findAllByPager(CarxlEntity carxlEntity);

	public void delOne(int xid);

	public CarxlEntity findOneByXname(String xname);

	public void insert(CarxlEntity carxlEntity);

	public CarxlEntity findOneByXid(int xid);

	public void update(CarxlEntity carxlEntity);

	public List<CarxlEntity> findAllByAid(int aid);

}
