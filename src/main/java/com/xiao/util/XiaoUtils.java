package com.xiao.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class XiaoUtils {

	//分割前端的删除数据
	public static int [] getSplitString(String str) {
		String[] split = str.split("&");
		int ar[] = new int[10];
		int j =0 ;
		for (int i = 0; i < split.length; i++) {
			j = Integer.valueOf(split[i]);
			ar[i] = j;
		}
		return ar;
	}

	/**
	 * 图片重命名
	 * @param str
	 * @return
	 */
	public static String getReloadName(String filename) {
		String result="";

		String lastname=filename.substring(filename.lastIndexOf("."));
		Date d=new Date();
		SimpleDateFormat ff=new SimpleDateFormat("yyyyMMddhhmmss");
		String time=ff.format(d);
		Random rad=new Random();
		int num=rad.nextInt(99999999);
		result=time+num+lastname;

		return result;
	}

	/**
	 * 获取当前系统时间
	 * @return
	 */
	public static String getCurrentTime() {
		SimpleDateFormat rj = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		String format = rj.format(new Date());
		return format;
	}
	/**
	 * 获取当前系统时间
	 * @return
	 */
	public static String getCurrentTimeMes() {
		SimpleDateFormat rj = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		String format = rj.format(new Date());
		return format;
	}

	public static Double getFormatDouble(Double count) {
		String result = String .format("%.2f",count);
		return Double.valueOf(result);
	}



}
