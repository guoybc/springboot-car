package com.xiao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
@MapperScan("com.xiao.dao")
public class SpringBootCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCarApplication.class, args);
	}

}
