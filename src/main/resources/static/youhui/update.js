$(function() {
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var yid = $("#yid").val();
		var ytitle = $("#ytitle").val();
		var ybegintime = $("#ybegintime").val();
		var yendtime = $("#yendtime").val();
		var ymoney = $("#ymoney").val();
		var ylessmoney = $("#ylessmoney").val();
		if (ytitle == null || ytitle == '') {
			layer.tips("请输入优惠活动名称", "#ytitle", {
				tips : [ 2, 'red' ]
			});

		} else if (ybegintime == null || ybegintime == '') {
			layer.tips("请选择活动开始时间", "#ybegintime", {
				tips : [ 2, 'red' ]
			});

		} else if (yendtime == null || yendtime == '') {
			layer.tips("请选择活动结束时间", "#yendtime", {
				tips : [ 2, 'red' ]
			});

		} else if (ymoney == null || ymoney == '') {
			layer.tips("请输入优惠金额", "#ymoney", {
				tips : [ 2, 'red' ]
			});

		} else if (ylessmoney == null || ylessmoney == '') {
			layer.tips("请输入起始金额", "#ylessmoney", {
				tips : [ 2, 'red' ]
			});

		} else if (ymoney > ylessmoney) {
			layer.tips("优惠金额不能大于起始金额", '#ymoney', {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../youhui/update", {
					'yid' : yid,
					'ytitle' : ytitle,
					'ybegintime' : ybegintime,
					'yendtime' : yendtime,
					'ymoney' : ymoney,
					'ylessmoney' : ylessmoney
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}