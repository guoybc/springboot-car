$(function() {
	getFocusYtitle();

	getBlurYtitle();

	getAdd();

})
// 聚焦
function getFocusYtitle() {
	$("#ytitle").focus();
}

// 失去焦点
function getBlurYtitle() {
	$("#ytitle").blur(function() {
		var ytitle = $(this).val();
		if (ytitle.length !== 0) {
			$.ajax({
				url : '../youhui/findOneByYtitle',
				data : {
					'ytitle' : ytitle
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#ytitle", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getYtitleExit() {
	var ytitle = $(this).val();
	if (ytitle.length !== 0) {
		$.ajax({
			url : '../youhui/findOneByYtitle',
			data : {
				'ytitle' : ytitle
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#ytitle", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var ytitle = $("#ytitle").val();
		var ybegintime = $("#ybegintime").val();
		var yendtime = $("#yendtime").val();
		var ymoney = $("#ymoney").val();
		var ylessmoney = $("#ylessmoney").val();
		if (ytitle == null || ytitle == '') {
			layer.tips("请输入优惠活动名称", "#ytitle", {
				tips : [ 2, 'red' ]
			});

		} else if (ybegintime == null || ybegintime == '') {
			layer.tips("请选择活动开始时间", "#ybegintime", {
				tips : [ 2, 'red' ]
			});

		} else if (yendtime == null || yendtime == '') {
			layer.tips("请选择活动结束时间", "#yendtime", {
				tips : [ 2, 'red' ]
			});

		} else if (ymoney == null || ymoney == '') {
			layer.tips("请输入优惠金额", "#ymoney", {
				tips : [ 2, 'red' ]
			});

		} else if (ylessmoney == null || ylessmoney == '') {
			layer.tips("请输入起始金额", "#ylessmoney", {
				tips : [ 2, 'red' ]
			});

		} else if (ymoney > ylessmoney) {
			layer.tips("优惠金额不能大于起始金额", '#ymoney', {
				tips : [ 2, 'red' ]
			});
		} else {
			// 判断是否有相同的名称
			$.post("../youhui/findOneByYtitle", {
				'ytitle' : ytitle
			}, function(data) {
				if (data === 1) {
					layer.tips("产品名称已经存在", "#ytitle", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../youhui/findOneByYtitle',
						data : {
							'ytitle' : ytitle
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前活动名称已存在请重新输入", "#ytitle", {
									tips : [ 2, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../youhui/insert", {
										'ytitle' : ytitle,
										'ybegintime':ybegintime,
										'yendtime':yendtime,
										'ymoney':ymoney,
										'ylessmoney':ylessmoney
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}