var title = [], json = [], cont = [];
$(function() {
	// 创建表格
	// getAll();
	// 外置delete
	// mydel();
	// 外置增加
	// myAdd();
	// 外置修改
	// myupp();

	// 获取统计信息
	getStatus();
	getMes();
	getMain1();
	getMain2();
	getAllStatus();
})
function getStatus() {
	$("#dj_kh").addClass("active");
	$("#bb").addClass("open");
}

function getMes() {
	$.ajax({
		url : "../member/findDjAndCount",
		data : '',
		dataType : 'json',
		async : false,
		type : 'post',
		success : function(mydata) {
			$.each(mydata, function(index, xx) {
				var row = {
					value : xx.count,
					name : xx.dname
				};
				title[index] = xx.dname;
				cont[index] = xx.count;
				json.push(row);
			});

		}
	});
}

function getMain1() {
	// 基于准备好的dom，初始化echarts实例
	var myChart = echarts.init(document.getElementById('main1'));
	// 指定图表的配置项和数据
	var option = {
		title : {
			text : '会员等级人数表',
			left : 'center'
		},
		tooltip : {
			trigger : 'item',
			formatter : '{a} <br/>{b} : {c} ({d}%)'
		},
		legend : {
			orient : 'vertical',
			left : 'left',
			data : title
		},
		series : [ {
			name : '会员数量',
			type : 'pie',
			radius : '55%',
			center : [ '50%', '60%' ],
			data : json,
			emphasis : {
				itemStyle : {
					shadowBlur : 10,
					shadowOffsetX : 0,
					shadowColor : 'rgba(0, 0, 0, 0.5)'
				}
			}
		} ]
	};
	// 使用刚指定的配置项和数据显示图表。
	myChart.setOption(option);
}
function getMain2() {
	// 基于准备好的dom，初始化echarts实例
	var myChart = echarts.init(document.getElementById('main2'));
	// 指定图表的配置项和数据
	var option = {
		title : {
			text : '会员等级人数表',
			left : 'center'
		},
		tooltip : {
			trigger : 'axis',
			axisPointer : { // 坐标轴指示器，坐标轴触发有效
				type : 'shadow' // 默认为直线，可选为：'line' | 'shadow'
			}
		},
		xAxis : {
			type : 'category',
			data : title
		},
		yAxis : {
			type : 'value'
		},
		series : [ {
			data : cont,
			type : 'bar'
		} ]
	};
	// 使用刚指定的配置项和数据显示图表。
	myChart.setOption(option);
}

function getStatus() {
	$("#dj_kh").addClass("active");
	$("#bb").addClass("open");
}

// 获得所有的页眉
function getAllStatus() {
	$("#myTab4").empty();
	$(".tab-content").empty();
	// 查询所有的会员等级
	$.ajax({
		url : '../dj/findAllNoPager',
		type : 'post',
		datatype : 'json',
		data : '',
		async : false,
		success : function(data) {
			// 查询全部
			$("#myTab4").append('<li class="active"><a data-toggle="tab" href="#id_0">所有会员</a></li>');
			$(".tab-content").append('<div id="id_0" class="tab-pane in active"><table id="tab0" class="table table-striped table-bordered table-hover"></table></div>')
			getAllTable(0);
			$.each(data, function(index, x) {
				$("#myTab4").append('<li><a data-toggle="tab" href="#id_' + x.did + '">' + x.dname + '</a></li>');
				$(".tab-content").append(
						'<div id="id_' + x.did + '" class="tab-pane"><table id="tab' + x.did + '" class="table table-striped table-bordered table-hover"></table></div>')
				getAllTable(x.did);
			})
		}
	})
	// 加载数据表格

}

function getAllTable(obj) {
	$("#tab" + obj).empty();
	$("#tab" + obj).bootstrapTable({
		url : '../member/findAllByDid?did=' + obj, // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始
			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		height : 530,
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "rid", // 每一行的唯一标识，一般为主键列
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		onClickCell : function(field, value, row, $element) {
			if (field == 'rimg') {

			}
		},
		columns : [ {
			field : 'rid',
			align : 'center',
			title : '编号',
			halign : 'center'
		}, {
			field : 'rcard',
			align : 'center',
			title : '会员编号',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'rname',
			align : 'center',
			title : '会员名称',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'rimg',
			align : 'center',
			title : '会员相片',
			formatter : function(value, row, index) {
				var e = '<span class="xname"><a href="#" class="green"  onclick="showimg(\'../' + row.rimg + '\')"><i class="ace-icon glyphicon glyphicon-picture"></i>照片详情</a>';
				return e;
			}
		}, {
			field : 'rtel',
			align : 'center',
			title : '会员号码',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'raddress',
			align : 'center',
			title : '联系地址',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'rsex',
			align : 'center',
			title : '会员性别',
			formatter : function(value, row, index) {
				var e;
				if (row.rsex == 1) {
					e = '<span class="xname" >' + '男' + '</span>';
				} else {
					e = '<span class="xname" >' + '女' + '</span>';
				}
				return e;
			}
		}, {
			field : 'dname',
			align : 'center',
			title : '会员等级',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'rstatus',
			align : 'center',
			title : '考号状态',
			formatter : function(value, row, index) {
				var e;
				if (row.rstatus == 1) {
					e = '<span class="xname" style="color:green;">' + '正常' + '</span>';
				} else {
					e = '<span class="xname" style="color:red;">' + '失效' + '</span>';
				}
				return e;
			}
		}, {
			field : 'rmoney',
			align : 'center',
			title : '卡上余额',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'aname',
			align : 'center',
			title : '汽车品牌',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'xname',
			align : 'center',
			title : '汽车系列',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'rjf',
			align : 'center',
			title : '卡上积分',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'rid',
			align : 'center',
			title : '查看详情',
			formatter : function(value, row, index) {
				var e = '<span class="xname" style="color:blue;"><a href="#" onclick=showMes(' + row.rid + ')><i class="menu-icon fa fa-eye pink"></i>详情</a> </span>';
				return e;
			}
		} ]
	});
}

function showimg(src) {
	if (src || src == "") {
		layer.msg("没有发现图片！", {
			icon : 6,
			time : 400
		});
	}
	var img = new Image();
	img.onload = function() {// 避免图片还未加载完成无法获取到图片的大小。
		// 避免图片太大，导致弹出展示超出了网页显示访问，所以图片大于浏览器时下窗口可视区域时，进行等比例缩小。
		var max_height = $(window).height() - 100;
		var max_width = $(window).width();

		// rate1，rate2，rate3 三个比例中取最小的。
		var rate1 = max_height / img.height;
		var rate2 = max_width / img.width;
		var rate3 = 1;
		var rate = Math.min(rate1, rate2, rate3);
		// 等比例缩放
		var imgHeight = img.height * rate; // 获取图片高度
		var imgWidth = img.width * rate; // 获取图片宽度

		var imgHtml = "<img src='" + src + "' width='" + imgWidth + "px' height='" + imgHeight + "px'/>";
		// 弹出层
		layer.open({
			type : 1,
			title : '会员照片',// 不显示标题
			closeBtn : 0,
			area : [ 'auto', 'auto' ],
			skin : 'layui-layer-nobg', // 没有背景色
			shadeClose : true,
			content : imgHtml
		});
	}
	img.src = src;
}

function showMes(obj) {
	layer.open({
		type : 2,
		title : '查看会员信息',
		shadeClose : true,
		shade : 0.8,
		area : [ '70%', '100%' ],
		content : "../member/toshowMes?rid=" + obj,
		skin : 'blue_base',
		anim : 0,
		end : function(index) {
		}
	});
}
