var rid;
// 需要的实际费用
var endTotal;
var rmoney;
// 库存数量
var fid_ar = new Array();
var fcount_ar = new Array();
var tid_ar = new Array();
// 购买数据
var new_fid_ar = new Array();
var new_tcount_ar = new Array();

$(function() {
	rid = $("#rid").val();
	getAll(rid);
	// 获取当前所有商品的类型

	// 查看当前用户的总资金
	getOneByRid()
	// 合计
	getTotal(rid);

	// 提交
	getDown();

})

function getAll(obj) {
	$("#tab").empty();
	$("#tab").bootstrapTable(
			{
				url : '../outcp/findAll?rid=' + obj, // 请求后台的URL（*）
				method : 'get', // 请求方式（*）
				dataType : 'json',
				toolbar : '#toolbar', // 工具按钮用哪个容器
				striped : true, // 是否显示行间隔色
				pagination : true, // 是否显示分页（*）
				height : 300,
				queryParams : function(params) {
					var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
						pages : params.limit, // 页面大小(每一页要显示多少条)
						begin : params.offset
					// 从数据库第几条记录开始

					};
					return temp;
				},// 传递参数（*）
				sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
				pageNumber : 1, // 初始化加载第一页，默认第一页
				pageSize : 10, // 每页的记录行数（*）
				pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
				showColumns : true, // 是否显示所有的列
				showRefresh : true, // 是否显示刷新按钮
				minimumCountColumns : 1, // 最少允许的列数
				clickToSelect : true, // 是否启用点击选中行
				uniqueId : "tid", // 每一行的唯一标识，一般为主键列
				showToggle : true, // 是否显示详细视图和列表视图的切换按钮
				cardView : false, // 是否显示详细视图
				detailView : false, // 是否显示父子表
				columns : [
						{
							field : 'tid',
							align : 'center',
							title : '编号',
							halign : 'center'
						},
						{
							field : 'cname',
							align : 'center',
							title : '产品类型',
							halign : 'center'
						},
						{
							field : 'fname',
							align : 'center',
							title : '产品名称',
							halign : 'center'
						},
						{
							field : 'foutprice',
							align : 'center',
							title : '出售价格(元)',
							halign : 'center'
						},
						{
							field : 'finprice',
							align : 'center',
							title : '则扣价格(元)',
							halign : 'center',
							formatter : function(value, row, index) {
								var price = row.foutprice * row.dzk;
								var e = '<span>' + price.toFixed(2) + '</span>';

								return e;
							}
						},
						{
							field : 'tcount',
							align : 'center',
							title : '购买数量',
							halign : 'center',
							formatter : function(value, row, index) {
								new_fid_ar.push(row.fid);
								new_tcount_ar.push(row.tcount);
								fcount_ar.push(row.fcount);
								var e;
								if (row.tcount > row.fcount) {
									e = '<span class="xname">购买数量:<span style="color:blue">' + value + '</span>  库存量:<span style="color:blue">' + row.fcount
											+ '</span><span style="color:red">(库存量不足<i class="fa fa-close"></i>)</span></span>';
								} else {
									e = '<span class="xname">购买数量:<span style="color:blue">' + value + '</span>  库存量:<span style="color:blue">' + row.fcount
											+ '</span><span style="color:green">(库存量充足<i class="fa fa-check" aria-hidden="true"></i>)</span></span>'
								}
								return e;
							}
						} ]
			});
}

function getTotal(obj) {
	$.ajax({
		url : '../outcp/getTotal?rid=' + obj,
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			endTotal = data.endTotal
			$("#preTotal").text(data.preTotal);
			$("#endTotal").text(endTotal);
			$("#did").text(data.dname);
			$("#dzk").text(data.dzk);
		}
	})
}
// 查询当前会员的总资金
function getOneByRid() {
	$.ajax({
		url : '../member/findOneByRid?rid=' + rid,
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			rmoney = data.rmoney;
		}
	})
}
function getDown() {
	$("#down").click(function() {
		fid_ar.length = 0;
		$.ajax({
			url : '../outcp/findAllNoPager?rid=' + rid,
			data : "",
			datatype : 'json',
			type : 'post',
			async:false,
			success : function(data) {
				$.each(data, function(index, x) {
					fid_ar.push(x.fid);
					tid_ar.push(x.tid);
				})
			}
		})
		var flag = true;//放入库存不足的货物编号
		var v_flag = new Array();
		for (var i = 0; i < fcount_ar.length; i++) {
			if (fcount_ar[i] < new_tcount_ar[i]) {
				v_flag.push(tid_ar[i])
				flag = false;
			}
		}
		if(!flag){
			layer.msg('编号为' + v_flag + '货物库存不足!', {
				icon : 5
			});
		}
		if (flag) {
			if (rmoney < endTotal) {
				layer.msg('余额不足请充值!', {
					icon : 5
				});
			} else {
				layer.confirm('确定增加吗?', {
					btn : [ '确定', '取消' ]
				// 按钮
				}, function(index) {
					$.ajax({
						url : '../outcp/insertMes',
						traditional : true,
						data : {
							'rid' : rid,
							'arfid' : fid_ar,
							'artcount' : new_tcount_ar,
							'tflag' : 1,
							'endTotal' : endTotal
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							// 关闭当前弹窗
							var index = parent.layer.getFrameIndex(window.name);
							parent.layer.close(index);
							parent.layer.msg('交易成功！', {
								icon : 1
							});
						}
					})
				}, function(index) {
					layer.close(index);
				})
			}
		}
	})
}
