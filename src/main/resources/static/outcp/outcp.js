var rid;
var rimg;
var tidCount;
$(function() {
	// 修改下拉列表的状态
	getStatus();

	getSearch();

	// 购买商品
	getShop();

	// 结账
	getEnd();

	// 单元格编辑数量
	getmyupp();
})

function getSearch() {
	$("#search").click(function() {
		search();
	})
}

function search() {
	var rcard = $("#rcard_search").val();
	if (rcard.length == 0) {
		layer.tips("请输入会员卡卡号", "#rcard_search", {
			tips : [ 1, 'red' ],
			time : 2000
		});
	} else {
		$.ajax({
			url : '../member/findOneByRcard',
			data : {
				'rcard' : rcard
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data == 0) {
					layer.tips("卡号不存在请重新输入", "#rcard_search", {
						tips : [ 1, 'red' ]
					});
					$("#form1").css("display", "none");
					$("#tab").css("display", "none");
				} else {
					// 显示下面的信息框
					showMes(data);
				}
			}
		})
	}
}

function showMes(data) {
	$("#form1").css("display", "block");
	$("#form2").css("display", "block");
	getMes(data);
	getAll(data);
}

function getStatus() {
	$("#outcp").addClass("active");
	$("#xf").addClass("open");
}

function getMes(obj) {
	rid = obj.rid;
	rimg = obj.rimg;
	rimg = "../"+rimg;
	var rcard = obj.rcard;
	var rname = obj.rname;
	var rpsw = obj.rpsw;
	var rtel = obj.rtel;
	var rsex = obj.rsex;
	var rbirthday = obj.rbirthday;
	var rcarnum = obj.rcarnum;
	var rcolor = obj.rcolor;
	var rway = obj.rway;
	var rnum = obj.rnum;
	var raddress = obj.raddress;
	var rremark = obj.rremark;
	var did = obj.dname;
	var xid = obj.xname;
	var zid = obj.zname;
	var aid = obj.aname;
	var rmoney = obj.rmoney;
	var rjf = obj.rjf;
	var rtime = obj.rtime;
	var rstatus = obj.rstatus;
	$("#aid").text(aid);
	$("#xid").text(xid);
	$("#did").text(did);
	$("#zid").text(zid);
	$("#rcard").text(rcard);
	$("#rname").text(obj.rname);
	$("#rpsw").text(obj.rpsw);
	$("#rtel").text(obj.rtel);
	if (rsex == 0) {
		rsex = '男'
	} else {
		rsex = '女'
	}
	$("#rsex").text(rsex);
	$("#rbirthday").text(obj.rbirthday);
	$("#rcarnum").text(obj.rcarnum);
	$("#rcolor").text(obj.rcolor);
	$("#rway").text(obj.rway);
	$("#rnum").text(obj.rnum);
	$("#raddress").text(obj.raddress);
	$("#rremark").text(obj.rremark);
	$("#rmoney").text(rmoney);
	$("#rjf").text(rjf);
	$("#rimg").html('<a href="javascript:void(0);" class="span_mes"  onclick="showimg(\'' + rimg + '\')"><i class="fa fa-image" aria-hidden="true"></i>查看详情</a>');
	$("#rtime").text(rtime);
	if (rstatus == 1) {
		rstatus = '正常'
	} else {
		tstatus = '销毁'
	}
	$("#rstatus").text(rstatus);
}

function findAllCount(obj) {
	$.ajax({
		url : '../outcp/findAllCount',
		data : {
			'rid' : rid
		},
		datatype : 'json',
		type : 'post',
		success : function(data) {
			tidCount = data;
			if (data < 1) {
				$("#btn_add_money").attr("disabled", 'disabled');
			} else {
				$("#btn_add_money").attr("disabled", false);
			}
		}
	})
}

function getAll(obj) {
	// 查询所有待购物的商品数目
	rid = obj.rid;
	findAllCount(rid);
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../outcp/findAll?rid=' + rid, // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		height : 530,
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "tid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			checkbox : true
		}, {
			field : 'tid',
			align : 'center',
			title : '编号',
			halign : 'center'
		}, {
			field : 'cname',
			align : 'center',
			title : '产品类型',
			halign : 'center'
		}, {
			field : 'fname',
			align : 'center',
			title : '产品名称',
			halign : 'center'
		}, {
			field : 'foutprice',
			align : 'center',
			title : '出售价格(元)',
			halign : 'center'
		}, {
			field : 'finprice',
			align : 'center',
			title : '则扣价格(元)',
			halign : 'center'
		}, {
			field : 'tcount',
			align : 'center',
			title : '购买数量',
			halign : 'center',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			title : '操作',
			field : 'tid',
			align : 'center',
			formatter : function(value, row, index) {

				var e = '<a href="javascript:void(0)" class="green"  onclick="edit(\'' + row.tid + '\')"><i class="ace-icon fa fa-pencil bigger-130"></i></a>&nbsp;&nbsp; ';
				var d = '<a href="javascript:void(0)" class="red" onclick="del(\'' + row.tid + '\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a> ';
				return e + d;
			}
		} ]
	});
}
// 购买商品
function getShop() {
	$("#btn_add_shop").click(function() {
		layer.open({
			type : 2,
			title : '购买商品',
			shadeClose : true,
			shade : 0.8,
			area : [ '70%', '90%' ],
			content : "../outcp/toInsert?rid=" + rid,
			skin : 'blue_base',
			end : function(index) {
				$("#tab").bootstrapTable('refresh');
				$("#btn_add_money").attr("disabled", false);
				tidCount += 1;
			}
		});
	})
}
// 结账
function getEnd() {
	$("#btn_add_money").click(function() {
		if (tidCount > 0) {
			layer.open({
				type : 2,
				title : '商品订单',
				shadeClose : true,
				shade : 0.8,
				area : [ '70%', '90%' ],
				content : "../outcp/toEnd?rid=" + rid,
				skin : 'blue_base',
				end : function(index) {
					$("#tab").bootstrapTable('refresh');
					// 再次判断是否可以结账;
					findAllCount(rid);
					// 修改上面用户数据
					$.ajax({
						url : '../member/findOneByRcard',
						data : {
							'rcard' : rcard
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							// 显示下面的信息框
							getMes(obj);
						}
					})
				}
			});
		} else {
			return;
		}
	})
}
// 修改
function getmyupp() {
	$("body").on("click", ".xname", function() {
		// 得到编号
		var tid = $(this).parent().parent().find('td:eq(1)').html();
		// 修改
		$(this).editable({
			type : "text", // 编辑框的类型。支持text|textarea|select|date|checklist等
			title : "用户名", // 编辑框的标题
			disabled : false, // 是否禁用编辑
			emptytext : "空文本", // 空值的默认文本
			mode : "popup", // 编辑框的模式：支持popup和inline两种模式，默认是popup
			validate : function(value) { // 字段验证
				if (!$.trim(value)) {
					return '不能为空';
				} else {
					$.ajax({
						url : '../outcp/updateTcount',
						dataType : 'json',
						data : {
							'tcount' : value,
							'tid' : tid
						},
						type : 'post',
						success : function(mydata) {
							if (mydata == 1) {
								layer.msg('修改成功!!', {
									icon : 1
								});

							}
							$("#tab").bootstrapTable('refresh');
						}
					});
				}
			}
		});

	})
}

//删除
function del(obj){
	$.ajax({
		url:"../outcp/del",
		data:{'tid':obj},
		datatype:'json',
		type:'post',
		success:function(data){
			if (data == 1) {
				layer.msg('删除成功!!', {
					icon : 1
				});

			}
			$("#tab").bootstrapTable('refresh');
		}
	})
}
function showimg(src) {
	if (src || src == "") {
		layer.msg("没有发现图片！", {
			icon : 6,
			time : 400
		});
	}
	var img = new Image();
	img.onload = function() {// 避免图片还未加载完成无法获取到图片的大小。
		// 避免图片太大，导致弹出展示超出了网页显示访问，所以图片大于浏览器时下窗口可视区域时，进行等比例缩小。
		var max_height = $(window).height() - 100;
		var max_width = $(window).width();

		// rate1，rate2，rate3 三个比例中取最小的。
		var rate1 = max_height / img.height;
		var rate2 = max_width / img.width;
		var rate3 = 1;
		var rate = Math.min(rate1, rate2, rate3);
		// 等比例缩放
		var imgHeight = img.height * rate; // 获取图片高度
		var imgWidth = img.width * rate; // 获取图片宽度

		var imgHtml = "<img src='" + src + "' width='" + imgWidth + "px' height='" + imgHeight + "px'/>";
		// 弹出层
		layer.open({
			type : 1,
			title : '会员照片',// 不显示标题
			closeBtn : 0,
			area : [ 'auto', 'auto' ],
			skin : 'layui-layer-nobg', // 没有背景色
			shadeClose : true,
			content : imgHtml
		});
	}
	img.src = src;
}
