/*
 * 正则表达式 
 */
var reg_tel = /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/

$(function() {
	getFocusSname();

	getBlurSname();

	// 查询所有汽车品牌
	getAllCartype();
	// 查询所有会员等级
	getAllDj();
	// 找到所有证件类型
	getAllZj();
	// 获取车辆系列二级下拉框
	getCarXl();

	getAdd();

})
// 聚焦
function getFocusSname() {
	$("#sname").focus();
}

// 失去焦点
function getBlurSname() {
	$("#sname").blur(function() {
		var sname = $(this).val();
		if (sname.length !== 0) {
			$.ajax({
				url : '../servicetype/findOneBySname',
				data : {
					'sname' : sname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#sname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getSnameExit() {
	var sname = $(this).val();
	if (sname.length !== 0) {
		$.ajax({
			url : '../servicetype/findOneBySname',
			data : {
				'sname' : sname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#sname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 查询所有的汽车品牌
function getAllCartype() {
	$("#aid").empty();
	$("#aid").append("<option value='0'>---请选择汽车品牌---</option>");
	$.ajax({
		url : '../cartype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#aid").append("<option value=" + x.aid + " >" + x.aname + "</option>");
			})
		}
	})
}
// 获取车辆系列二级下拉框
function getCarXl() {
	$("body").on("change", '#aid', function() {
		var aid = $(this).val();
		$("#xid").empty();
		$("#xid").append("<option value='0'>---请选择汽车品牌---</option>");
		$.ajax({
			url : '../carxl/findAllByAid?aid=' + aid,
			data : "",
			datatype : 'json',
			type : 'post',
			success : function(data) {
				$.each(data, function(index, x) {
					$("#xid").append("<option value=" + x.xid + " >" + x.xname + "</option>");
				})
			}
		})
	})
}

// 查询所有的会员等级
function getAllDj() {
	$("#did").empty();
	$("#did").append("<option value='0'>---请选择会员等级---</option>");
	$.ajax({
		url : '../dj/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#did").append("<option value=" + x.did + " >" + x.dname + "</option>");
			})
		}
	})
}

// 查询所有的证件类型
function getAllZj() {
	$("#zid").empty();
	$("#zid").append("<option value='0'>---请选择证件类型---</option>");
	$.ajax({
		url : '../pz/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#zid").append("<option value=" + x.zid + " >" + x.zname + "</option>");
			})
		}
	})
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var rcard = $("#rcard").val();
		var rname = $("#rname").val();
		var rpsw = $("#rpsw").val();
		var fileimg = $("#fileimg").val();
		var rtel = $("#rtel").val();
		var rsex = $("#rsex").val();
		var rbirthday = $("#rbirthday").val();
		var rcarnum = $("#rcarnum").val();
		var rcolor = $("#rcolor").val();
		var rway = $("#rway").val();
		var rnum = $("#rnum").val();
		var raddress = $("#raddress").val();
		var rremark = $("#rremark").val();
		var did = $("#did").val();
		var xid = $("#xid").val();
		var zid = $("#zid").val();
		var aid = $("#aid").val();
		if (rcard == null || rcard == '') {
			layer.tips("请输入会员卡号", "#rcard", {
				tips : [ 2, 'red' ]
			});

		} else if (rname == null || rname == '') {
			layer.tips("请输入会员姓名", "#rname", {
				tips : [ 2, 'red' ]
			});

		} else if (rpsw == null || rpsw == '') {
			layer.tips("请输入卡密", "#rpsw", {
				tips : [ 2, 'red' ]
			});

		} else if (fileimg == null || fileimg == '') {
			layer.tips("请选择相片", "#fileimg", {
				tips : [ 2, 'red' ]
			});

		} else if (rtel == null || rtel == '') {
			layer.tips("请输入会员联系电话", "#rtel", {
				tips : [ 2, 'red' ]
			});

		} else if (rsex == 3) {
			layer.tips("请选择会员性别", "#rsex", {
				tips : [ 2, 'red' ]
			});

		} else if (rbirthday == null || rbirthday == '') {
			layer.tips("请选择会员生日", "#rbirthday", {
				tips : [ 2, 'red' ]
			});

		} else if (rcarnum == null || rcarnum == '') {
			layer.tips("请输入会员车牌号", "#rcarnum", {
				tips : [ 2, 'red' ]
			});

		} else if (rcolor == null || rcolor == '') {
			layer.tips("请输入车辆颜色", "#rcolor", {
				tips : [ 2, 'red' ]
			});

		} else if (rway == null || rway == '') {
			layer.tips("请输入行驶里程", "#rway", {
				tips : [ 2, 'red' ]
			});

		} else if (rnum == null || rnum == '') {
			layer.tips("请输入证件编号", "#rnum", {
				tips : [ 2, 'red' ]
			});

		} else if (raddress == null || raddress == '') {
			layer.tips("请输入会员住址", "#raddress", {
				tips : [ 2, 'red' ]
			});

		} else if (rremark == null || rremark == '') {
			layer.tips("请输入会员备注", "#rremark", {
				tips : [ 2, 'red' ]
			});

		} else if (did == 0) {
			layer.tips("请选择会员等级", "#did", {
				tips : [ 2, 'red' ]
			});

		} else if (aid == 0) {
			layer.tips("请选择车牌", "#aid", {
				tips : [ 2, 'red' ]
			});

		} else if (xid == 0) {
			layer.tips("请选择车系列", "#xid", {
				tips : [ 2, 'red' ]
			});

		} else if (zid == 0) {
			layer.tips("请选择凭证", "#zid", {
				tips : [ 2, 'red' ]
			});

		}else if (reg_tel.test(rtel)==false) {
			layer.tips("电话格式非法", "#rtel", {
				tips : [ 2, 'red' ]
			});

		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajaxFileUpload({
					url : '../member/insert', // 用于文件上传的服务器端请求地址
					secureuri : false, // 是否需要安全协议，一般设置为false
					fileElementId : [ 'fileimg' ], // 文件上传域的ID
					data : {
						'rcard' : rcard,
						'rname' : rname,
						'fileimg' : fileimg,
						'rpsw' : rpsw,
						'rtel' : rtel,
						'rsex' : rsex,
						'rbirthday' : rbirthday,
						'rcarnum' : rcarnum,
						'rcolor' : rcolor,
						'rway' : rway,
						'rnum' : rnum,
						'raddress' : raddress,
						'rremark' : rremark,
						'did' : did,
						'xid' : xid,
						'aid' : aid,
						'zid' : zid
					}, // 此参数非常严谨，写错一个引号都不行
					dataType : 'json', // 返回值类型 一般设置为json
					type : 'post',
					success : function(data, status) {
						//
					},
					error : function(data, status, e) {
						var index = parent.layer.getFrameIndex(window.name);
						// 刷新父页面
						window.parent.location.reload();
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('增加成功！', {
							icon : 1
						});
					}
				});
			}, function(index) {
				layer.close(index);
			})
		}
	})

}
