$(function() {
	//设置信息只可读
	$("input").attr("disabled", true);
	$("select").attr("disabled", true);
	$("textarea").attr("disabled", true);
	// 查询所有汽车品牌
	getAllCartype();
	// 查询所有会员等级
	getAllDj();
	// 找到所有证件类型
	getAllZj();
	// 获取车辆系列二级下拉框
	getCarXl();
})
// 查询所有的汽车品牌
function getAllCartype() {
	$("#aid").empty();
	$("#aid").append("<option value='0'>---请选择汽车品牌---</option>");
	$.ajax({
		url : '../cartype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#aid").append("<option value=" + x.aid + " >" + x.aname + "</option>");
			})
		}
	})
}
// 获取车辆系列二级下拉框
function getCarXl() {
	$("body").on("change", '#aid', function() {
		var aid = $(this).val();
		$("#xid").empty();
		$("#xid").append("<option value='0'>---请选择汽车品牌---</option>");
		$.ajax({
			url : '../carxl/findAllByAid?aid=' + aid,
			data : "",
			datatype : 'json',
			type : 'post',
			success : function(data) {
				$.each(data, function(index, x) {
					$("#xid").append("<option value=" + x.xid + " >" + x.xname + "</option>");
				})
			}
		})
	})
}

// 查询所有的会员等级
function getAllDj() {
	$("#did").empty();
	$("#did").append("<option value='0'>---请选择会员等级---</option>");
	$.ajax({
		url : '../dj/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#did").append("<option value=" + x.did + " >" + x.dname + "</option>");
			})
		}
	})
}

// 查询所有的证件类型
function getAllZj() {
	$("#zid").empty();
	$("#zid").append("<option value='0'>---请选择证件类型---</option>");
	$.ajax({
		url : '../pz/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#zid").append("<option value=" + x.zid + " >" + x.zname + "</option>");
			})
		}
	})
}