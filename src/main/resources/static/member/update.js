var reg_tel = /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/

$(function() {
	// 查询所有汽车品牌
	getAllCartype();
	// 找到所有证件类型
	getAllZj();
	// 查询所有的会员等级
	getAllDj()

	// 修改
	getUpp();
});
// 查询所有的汽车品牌
function getAllCartype() {
	$("#aid").empty();
	$("#aid").append("<option value='0'>---请选择会员等级---</option>");
	$.ajax({
		url : '../cartype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#aid").append("<option value=" + x.aid + " >" + x.aname + "</option>");
			})
			// 选中
			var r_aid = $("#r_aid").val();
			$("#aid").val(r_aid);
			// 获取车辆系列二级下拉框
			getCarXl(r_aid);
		}
	})
}
// 获取车辆系列二级下拉框
function getCarXl(obj) {
	$("#xid").empty();
	$.ajax({
		url : '../carxl/findAllByAid?aid=' + obj,
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#xid").append("<option value=" + x.xid + " >" + x.xname + "</option>");
			})
			var r_xid = $("#r_xid").val();
			$("#xid").val(r_xid);
		}
	})
}

// 查询所有的会员等级
function getAllDj() {
	$("#did").empty();
	$.ajax({
		url : '../dj/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#did").append("<option value=" + x.did + " >" + x.dname + "</option>");
			})
			var r_did = $("#r_did").val();
			$("#did").val(r_did);
		}
	})
}

// 查询所有的证件类型
function getAllZj() {
	$("#zid").empty();
	$.ajax({
		url : '../pz/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#zid").append("<option value=" + x.zid + " >" + x.zname + "</option>");
			})
			var r_zid = $("#r_zid").val();
			$("#zid").val(r_zid);
		}
	})
}

// 增加
function getUpp() {
	$("#add").click(function() {
		var rid = $("#rid").val();
		var rcard = $("#rcard").val();
		var rname = $("#rname").val();
		var rpsw = $("#rpsw").val();
		var fileimg = $("#fileimg").val();
		var rtel = $("#rtel").val();
		var rsex = $("#rsex").val();
		var rbirthday = $("#rbirthday").val();
		var rcarnum = $("#rcarnum").val();
		var rcolor = $("#rcolor").val();
		var rway = $("#rway").val();
		var rnum = $("#rnum").val();
		var raddress = $("#raddress").val();
		var rremark = $("#rremark").val();
		var did = $("#did").val();
		var xid = $("#xid").val();
		var zid = $("#zid").val();
		var aid = $("#aid").val();
		if (rcard == null || rcard == '') {
			layer.tips("请输入会员卡号", "#rcard", {
				tips : [ 2, 'red' ]
			});

		} else if (rname == null || rname == '') {
			layer.tips("请输入会员姓名", "#rname", {
				tips : [ 2, 'red' ]
			});

		} else if (rpsw == null || rpsw == '') {
			layer.tips("请输入卡密", "#rpsw", {
				tips : [ 2, 'red' ]
			});

		} else if (rtel == null || rtel == '') {
			layer.tips("请输入会员联系电话", "#rtel", {
				tips : [ 2, 'red' ]
			});

		} else if (rsex == 3) {
			layer.tips("请选择会员性别", "#rsex", {
				tips : [ 2, 'red' ]
			});

		} else if (rbirthday == null || rbirthday == '') {
			layer.tips("请选择会员生日", "#rbirthday", {
				tips : [ 2, 'red' ]
			});

		} else if (rcarnum == null || rcarnum == '') {
			layer.tips("请输入会员车牌号", "#rcarnum", {
				tips : [ 2, 'red' ]
			});

		} else if (rcolor == null || rcolor == '') {
			layer.tips("请输入车辆颜色", "#rcolor", {
				tips : [ 2, 'red' ]
			});

		} else if (rway == null || rway == '') {
			layer.tips("请输入行驶里程", "#rway", {
				tips : [ 2, 'red' ]
			});

		} else if (rnum == null || rnum == '') {
			layer.tips("请输入证件编号", "#rnum", {
				tips : [ 2, 'red' ]
			});

		} else if (raddress == null || raddress == '') {
			layer.tips("请输入会员住址", "#raddress", {
				tips : [ 2, 'red' ]
			});

		} else if (rremark == null || rremark == '') {
			layer.tips("请输入会员备注", "#rremark", {
				tips : [ 2, 'red' ]
			});

		} else if (did == 0) {
			layer.tips("请选择会员等级", "#did", {
				tips : [ 2, 'red' ]
			});

		} else if (aid == 0) {
			layer.tips("请选择车牌", "#aid", {
				tips : [ 2, 'red' ]
			});

		} else if (xid == 0) {
			layer.tips("请选择车系列", "#xid", {
				tips : [ 2, 'red' ]
			});

		} else if (zid == 0) {
			layer.tips("请选择凭证", "#zid", {
				tips : [ 2, 'red' ]
			});

		} else if (reg_tel.test(rtel) == false) {
			layer.tips("电话格式非法", "#rtel", {
				tips : [ 2, 'red' ]
			});

		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajaxFileUpload({
					url : '../member/update', // 用于文件上传的服务器端请求地址
					secureuri : false, // 是否需要安全协议，一般设置为false
					fileElementId : [ 'fileimg' ], // 文件上传域的ID
					data : {
						'rid':rid,
						'rcard' : rcard,
						'rname' : rname,
						'fileimg' : fileimg,
						'rpsw' : rpsw,
						'rtel' : rtel,
						'rsex' : rsex,
						'rbirthday' : rbirthday,
						'rcarnum' : rcarnum,
						'rcolor' : rcolor,
						'rway' : rway,
						'rnum' : rnum,
						'raddress' : raddress,
						'rremark' : rremark,
						'did' : did,
						'xid' : xid,
						'aid' : aid,
						'zid' : zid
					}, // 此参数非常严谨，写错一个引号都不行
					dataType : 'json', // 返回值类型 一般设置为json
					type : 'post',
					success : function(data, status) {
						//
					},
					error : function(data, status, e) {
						var index = parent.layer.getFrameIndex(window.name);
						// 刷新父页面
						window.parent.location.reload();
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 1
						});
					}
				});
			}, function(index) {
				layer.close(index);
			})
		}
	})

}
