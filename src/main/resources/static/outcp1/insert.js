var cid;
var fid;
var fcount;
var tcount;
$(function() {
	// 获取产品数量
	getAllCptype();

	getCptypeChange();

	getMytfChange();

	getAdd()
})

function getAllCptype() {
	$("#cid").empty();
	$("#cid").append("<option value='0'>---请选择产品类型---</option>");
	$.ajax({
		url : '../cptype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#cid").append("<option value=" + x.cid + " >" + x.cname + "</option>");
			})
		}
	})
}

function getCptypeChange() {
	$("body").on("change", '#cid', function() {
		cid = $(this).val();
		// 获取下拉的产品名称
		getMytfSelect(cid);
		// 清空价格
		$("#fcount").val("");
		$("#foutprice").val("");
	})
}
function getMytfSelect(obj) {
	$("#fid").empty();
	$("#fid").append("<option value='0'>---请选择产品名称---</option>");
	$.ajax({
		url : '../mytf/findAllByCid?cid=' + obj,
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#fid").append("<option value=" + x.fid + " >" + x.fname + "-(产地:" + x.faddress + ")" + "</option>");
			})
		}
	})
}

function getMytfChange() {
	$("body").on("change", '#fid', function() {
		fid = $(this).val();
		$.ajax({
			url : '../mytf/findOneByFid?fid=' + fid,
			data : "",
			datatype : 'json',
			type : 'post',
			success : function(data) {
				fcount = data.fcount;
				$("#fcount").val(data.fcount + data.fdw);
				$("#foutprice").val(data.foutprice);
				if (fcount == 0) {
					$("#add").attr('disabled', true);
					$("#wcount").attr('disabled', true);
					$("#wcount").attr("placeholder", '库存不足请换一个商品');
					layer.msg('库存不足请换一个商品!!', {
						icon : 5
					});
				}else{
					$("#add").attr('disabled', false);
					$("#wcount").attr('disabled', false);
					$("#wcount").attr("placeholder", '    请输入商品数量');
				}
			}
		})

	})
}
function getAdd() {
	$("#add").click(function() {
		fid = $("#fid").val();
		var wname = $("#wname").val();
		var wcount = $("#wcount").val();
		var wtel = $("#wtel").val();
		if (cid == 0) {
			layer.tips("请选择产品类型", "#cid", {
				tips : [ 2, 'red' ]
			});
		} else if (fid == 0) {
			layer.tips("请选择产品名称", "#fid", {
				tips : [ 2, 'red' ]
			});
		} else if (wcount.length == 0 || wcount == '') {
			layer.tips("请输入购货数量", "#wcount", {
				tips : [ 2, 'red' ]
			});
		} else if (wname.length==0 || wname == '' ) {
			layer.tips("请输入顾客名称", "#wname", {
				tips : [ 2, 'red' ]
			});
		}else if (wtel.length==0 || wtel == '' ) {
			layer.tips("请输入顾客电话", "#wtel", {
				tips : [ 2, 'red' ]
			});
		}else if (tcount > fcount) {
			layer.tips("库存不足", "#fcount", {
				tips : [ 2, 'red' ]
			});
		}else if (wcount > fcount) {
			layer.tips("库存不足,请重新输入", "#fcount", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajax({
					url : '../outcp1/insert',
					data : {
						'fid' : fid,
						'wcount' : wcount,
						'wtel':wtel,
						'wname':wname
					},
					datatype : 'json',
					type : 'post',
					success : function(data) {
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('增加成功！', {
							icon : 1
						});
					}
				})
			}, function(index) {
				layer.close(index);
			})
		}
	})
}