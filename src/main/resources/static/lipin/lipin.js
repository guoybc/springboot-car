$(function() {
	// 修改下拉列表的状态
	getStatus();

	// 查询所有信息;
	getAllSk();

	// 增加
	myAdd();

	mydel();

	myupp();
})
function getStatus() {
	$("#lipin").addClass("active");
	$("#lp").addClass("open");
}

function getAllSk() {
	// 查询所有待购物的商品数目
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../lipin/findAll', // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		height : 530,
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "nid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			checkbox : true
		}, {
			field : 'nid',
			align : 'center',
			title : '编号',
			halign : 'center'
		}, {
			field : 'nname',
			align : 'center',
			title : '礼品详细',
			halign : 'center'
		}, {
			field : 'nimg',
			align : 'center',
			title : '礼品图片',
			halign : 'center',
			formatter : function(value, row, index) {
				var e = '<span class="xname"><a href="#" class="green"  onclick="showimg(\'../' + row.nimg + '\')"><i class="ace-icon glyphicon glyphicon-picture"></i>照片详情</a>';
				return e;
			}
		}, {
			field : 'njf',
			align : 'center',
			title : '消耗积分',
			halign : 'center'
		}, {
			field : 'ncount',
			align : 'center',
			title : '进货总量',
			halign : 'center'
		}, {
			field : 'nncount',
			align : 'center',
			title : '库存总量',
			halign : 'center'
		}, {
			title : '操作',
			field : 'nid',
			align : 'center',
			formatter : function(value, row, index) {

				var e = '<a href="javascript:void(0)" class="green"  onclick="edit(\'' + row.nid + '\')"><i class="ace-icon fa fa-pencil bigger-130"></i></a>&nbsp;&nbsp; ';
				var d = '<a href="javascript:void(0)" class="red" onclick="del(\'' + row.nid + '\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a> ';
				return e + d;
			}
		} ]
	});
}

function mydel() {
	$("#btn_delete").click(function() {
		// 得到你要修改的记录的编号(数组)！
		var tt = $.map($('#tab').bootstrapTable('getSelections'), function(row) {
			return row.nid;
		});
		if (tt.length == 0) {
			layer.tips("请最少选择一个要删除的对象！", "#btn_delete", {
				tips : [ 1, 'red' ]
			});
		} else {
			layer.confirm('确定删除吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				var ar = '';
				for (var i = 0; i < tt.length; i++) {
					ar += tt[i] + "&";
				}
				$.ajax({
					url : '../lipin/delSome',
					type : 'post',
					datatype : 'json',
					data : {
						'ar' : ar
					},
					success : function(data) {
						// 刷新当前页面
						$("#tab").bootstrapTable('refresh');
					}
				})
				layer.close(index);
				layer.msg('删除成功', {
					icon : 1,
					time : 1000
				});
			}, function(index) {
				layer.close(index);
				layer.msg('取消删除', {
					icon : 1,
					time : 1000
				});
			})
		}

	});
}

function myAdd() {
	$("#btn_add").click(function() {
		layer.open({
			type : 2,
			title : '增加礼品信息',
			shadeClose : true,
			shade : 0.8,
			area : [ '70%', '100%' ],
			content : "../lipin/toInsert",
			skin : 'blue_base',
			end : function(index) {
				$("#tab").bootstrapTable('refresh');
			}
		});
	});
}
// 内置的修改
function edit(obj) {
	layer.open({
		type : 2,
		title : '修改礼品信息',
		shadeClose : true,
		shade : 0.8,
		area : [ '70%', '100%' ],
		content : "../lipin/toUpdate?nid=" + obj,
		skin : 'blue_base',
		anim : 0,
		end : function(index) {
			$("#tab").bootstrapTable('refresh');
		}
	});
}
// 内置删除
function del(obj) {
	layer.confirm('确定删除吗?', {
		btn : [ '确定', '取消' ]
	// 按钮
	}, function(index) {
		$.post('../lipin/delOne', {
			'nid' : obj
		}, function(data) {
			if (data == 1) {
				layer.close(index);
				layer.msg('删除成功', {
					icon : 1,
					time : 1000
				});
				$("#tab").bootstrapTable('refresh');
			}
		})
	}, function(index) {
		layer.close(index);
	})
}

// 外置修改
function myupp() {
	$("#btn_update").click(function() {
		// 得到你要修改的记录的编号(数组)！
		var tt = $.map($('#tab').bootstrapTable('getSelections'), function(row) {
			return row.nid;
		});
		if (tt.length == 0) {
			layer.tips("请选择一个要修改的对象！", "#btn_update", {
				tips : [ 1, 'red' ]
			});
		} else if (tt.length > 1) {

			layer.tips("一次只能修改一个对象！", "#btn_update", {
				tips : [ 1, 'red' ]
			});
		} else {
			edit(tt[0]);
		}

	});
}

function showMes(obj) {
	layer.open({
		type : 2,
		title : '查看会员信息',
		shadeClose : true,
		shade : 0.8,
		area : [ '70%', '100%' ],
		content : "../member/toshowMes?nid=" + obj,
		skin : 'blue_base',
		anim : 0,
		end : function(index) {
		}
	});
}

/*******************************************************************************
 * html:<img src="img/1.jpg" onclick="showimg('img/1.jpg');">
 * 图片弹出展示,默认原大小展示。图片大于浏览器时下窗口可视区域时，进行等比例缩小。 src 图片路径。必须项 imgHeight
 * 图片显示高度，默认原大小展示。图片大于浏览器时下窗口可视区域时，进行等比例缩小。 imgWidth
 * 图片显示宽度，默认原大小展示。图片大于浏览器时下窗口可视区域时，进行等比例缩小。
 */

function showimg(src) {
	if (src || src == "") {
		layer.msg("没有发现图片！", {
			icon : 6,
			time : 400
		});
	}
	var img = new Image();
	img.onload = function() {// 避免图片还未加载完成无法获取到图片的大小。
		// 避免图片太大，导致弹出展示超出了网页显示访问，所以图片大于浏览器时下窗口可视区域时，进行等比例缩小。
		var max_height = $(window).height() - 100;
		var max_width = $(window).width();

		// rate1，rate2，rate3 三个比例中取最小的。
		var rate1 = max_height / img.height;
		var rate2 = max_width / img.width;
		var rate3 = 1;
		var rate = Math.min(rate1, rate2, rate3);
		// 等比例缩放
		var imgHeight = img.height * rate; // 获取图片高度
		var imgWidth = img.width * rate; // 获取图片宽度

		var imgHtml = "<img src='" + src + "' width='" + imgWidth + "px' height='" + imgHeight + "px'/>";
		// 弹出层
		layer.open({
			type : 1,
			title : '会员照片',// 不显示标题
			closeBtn : 0,
			area : [ 'auto', 'auto' ],
			skin : 'layui-layer-nobg', // 没有背景色
			shadeClose : true,
			content : imgHtml
		});
	}
	img.src = src;
}