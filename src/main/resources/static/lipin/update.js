$(function() {
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var nid = $("#nid").val();
		var nname = $("#nname").val();
		var fileimg = $("#fileimg").val();
		var njf = $("#njf").val();
		if (nname.length == 0 || nname == null) {
			layer.tips("请输入礼品名称", "#nname", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajaxFileUpload({
					url : '../lipin/update', // 用于文件上传的服务器端请求地址
					secureuri : false, // 是否需要安全协议，一般设置为false
					fileElementId : [ 'fileimg' ], // 文件上传域的ID
					data : {
						'nid':nid,
						'nname' : nname,
						'njf':njf,
						'fileimg' : fileimg
					}, // 此参数非常严谨，写错一个引号都不行
					dataType : 'json', // 返回值类型 一般设置为json
					type : 'post',
					success : function(data, status) {
						//
					},
					error : function(data, status, e) {
						var index = parent.layer.getFrameIndex(window.name);
						// 关闭当前弹窗
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 1
						});
					}
				});
			}, function(index) {
				layer.close(index);
			})
		}
	})
}