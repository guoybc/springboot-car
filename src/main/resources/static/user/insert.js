$(function() {
	getFocusUname();

	getBlurUname();

	getAdd();
})
// 聚焦
function getFocusUname() {
	$("#uname").focus();
}

// 失去焦点
function getBlurUname() {
	$("#uname").blur(function() {
		var uname = $(this).val();
		if (uname.length !== 0) {
			$.ajax({
				url : '../user/findOneByUname',
				data : {
					'uname' : uname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("该账户已存在", "#uname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getUnameExit() {
	var uname = $(this).val();
	if (uname.length !== 0) {
		$.ajax({
			url : '../user/findOneByUname',
			data : {
				'uname' : uname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("该账户已存在", "#uname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var uname = $("#uname").val();
		var upsw = $("#upsw").val();
		var urealname = $("#urealname").val();
		var utel = $("#utel").val();
		var usex = $("input[name='usex']:checked").val();
		if (uname == null || uname == '') {
			layer.tips("请输入账号", "#uname", {
				tips : [ 2, 'red' ]
			});

		} else if (upsw == null || upsw == '') {
			layer.tips("请输入密码", "#upsw", {
				tips : [ 2, 'red' ]
			});

		} else if (urealname == null || urealname == '') {
			layer.tips("请输入真实姓名", "#urealname", {
				tips : [ 2, 'red' ]
			});

		} else if (utel == null || utel == '') {
			layer.tips("请输入电话", "#utel", {
				tips : [ 2, 'red' ]
			});

		} else {
			// 判断是否有相同的名称
			$.post("../user/findOneByUname", {
				'uname' : uname
			}, function(data) {
				if (data == 1) {
					layer.tips("该账户已存在", "#uname", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../user/findOneByUname',
						data : {
							'uname' : uname
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前名称已存在", "#uname", {
									tips : [ 1, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../user/insert", {
										'uname' : uname,
										'upsw' : upsw,
										'urealname' : urealname,
										'usex' : usex,
										'utel' : utel
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}