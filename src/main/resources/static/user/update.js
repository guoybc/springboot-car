var old_usex ;
$(function() {
	//获取后端的usex
	old_usex = $("#usex").val();
	//让性别选中
	$("input[name='usex'][value="+old_usex+"]").attr("checked",true);
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var uid = $("#uid").val();
		var uname = $("#uname").val();
		var upsw = $("#upsw").val();
		var urealname = $("#urealname").val();
		var usex = $("input[name='usex']:checked").val();
		var utel = $("#utel").val();
		
		if (uname == null || uname == '') {
			layer.tips("请输入账号", "#uname", {
				tips : [ 2, 'red' ]
			});

		} else if (upsw == null || upsw == '') {
			layer.tips("请输入密码", "#upsw", {
				tips : [ 2, 'red' ]
			});

		} else if (urealname == null || urealname == '') {
			layer.tips("请输入真实姓名", "#urealname", {
				tips : [ 2, 'red' ]
			});

		} else if (utel == null || utel == '') {
			layer.tips("请输入电话", "#utel", {
				tips : [ 2, 'red' ]
			});

		} else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../user/update", {
					'uid':uid,
					'uname' : uname,
					'upsw' : upsw,
					'urealname' : urealname,
					'usex' : usex,
					'utel' : utel
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}