$(function() {
	getFocusSname();

	getBlurSname();

	getAdd();
})
// 聚焦
function getFocusSname() {
	$("#sname").focus();
}

// 失去焦点
function getBlurSname() {
	$("#sname").blur(function() {
		var sname = $(this).val();
		if (sname.length !== 0) {
			$.ajax({
				url : '../servicetype/findOneBySname',
				data : {
					'sname' : sname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#sname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getSnameExit() {
	var sname = $(this).val();
	if (sname.length !== 0) {
		$.ajax({
			url : '../servicetype/findOneBySname',
			data : {
				'sname' : sname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#sname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var sname = $("#sname").val();
		if (sname == null || sname == '') {
			layer.tips("当前名称为空", "#sname", {
				tips : [ 2, 'red' ]
			});

		} else {
			// 判断是否有相同的名称
			$.post("../servicetype/findOneBySname", {
				'sname' : sname
			}, function(data) {
				if (data === 1) {
					layer.tips("部门名称已经存在", "#sname", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../servicetype/findOneBySname',
						data : {
							'sname' : sname
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前名称已存在", "#sname", {
									tips : [ 1, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../servicetype/insert", {
										'sname' : sname
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}