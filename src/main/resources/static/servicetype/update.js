$(function() {
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var sid = $("#sid").val();
		var sname = $("#sname").val();
		if (sname == null || sname == '') {
			layer.tips("当前名称为空", "#sname", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../servicetype/update", {
					'sid' : sid,
					'sname' : sname
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}