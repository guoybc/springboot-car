//正则表达式
//小数
var reg_count = /^((([^0][0-9]+|0)\.([0-9]{1,2}))$)|^(([1-9]+)\.([0-9]{1,2})$)/;
// 检验钱
var reg_allCount = /([1-9]([0-9]+)?(.[0-9]{1,2})?$)|(^(0){1}$)|([0-9].0-9?$)/;
// 检验汉字姓名
var reg_nameCN = /^[\u2E80-\u9FFF]+$/;
$(function() {
	getFocusDname();

	getBlurDname();

	getAdd();
})
// 聚焦
function getFocusDname() {
	$("#dname").focus();
}

// 失去焦点
function getBlurDname() {
	$("#dname").blur(function() {
		var dname = $(this).val();
		if (dname.length !== 0) {
			$.ajax({
				url : '../dj/findOneByDname',
				data : {
					'dname' : dname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#dname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getDnameExit() {
	var dname = $(this).val();
	if (dname.length !== 0) {
		$.ajax({
			url : '../dj/findOneByDname',
			data : {
				'dname' : dname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#dname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var dname = $("#dname").val();
		var djf = $("#djf").val();
		var dmoneyBl = $("#dmoneyBl").val();
		var dzk = $("#dzk").val();
		if (dname == null || dname == '') {
			layer.tips("当前名称为空", "#dname", {
				tips : [ 2, 'red' ]
			});
		} else if (djf == null || djf == '') {
			layer.tips("积分信息为空", "#djf", {
				tips : [ 2, 'red' ]
			});
		} else if (dmoneyBl == null || dmoneyBl == '') {
			layer.tips("兑换比率为空", "#dmoneyBl", {
				tips : [ 2, 'red' ]
			});
		} else if (dzk == null || dzk == '') {
			layer.tips("兑换则扣为空", "#dzk", {
				tips : [ 2, 'red' ]
			});
		} else if (!reg_nameCN.test(dname)) {
			layer.tips("名称信息不合法", "#dname", {
				tips : [ 2, 'red' ]
			});
		} else if (!reg_allCount.test(djf)) {
			layer.tips("积分信息不合法", "#djf", {
				tips : [ 2, 'red' ]
			});
		} else if (!reg_count.test(dmoneyBl)) {
			layer.tips("兑换比率不合法", "#dmoneyBl", {
				tips : [ 2, 'red' ]
			});
		} else if (!reg_count.test(dzk)) {
			layer.tips("兑换折扣不合法", "#dzk", {
				tips : [ 2, 'red' ]
			});
		} else {
			// 判断是否有相同的名称
			$.post("../dj/findOneByDname", {
				'dname' : dname
			}, function(data) {
				if (data === 1) {
					layer.tips("部门名称已经存在", "#dname", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../dj/findOneByDname',
						data : {
							'dname' : dname
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前名称已存在", "#dname", {
									tips : [ 1, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../dj/insert", {
										'dname' : dname,
										'djf' : djf,
										'dmoneyBl' : dmoneyBl,
										'dzk' : dzk
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}