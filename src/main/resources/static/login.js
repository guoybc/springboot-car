$(function() {
	getBtn();
})

function getBtn() {
	$("#btn").click(function() {
		var uname = $("#uname").val();
		var upsw = $("#upsw").val();
		if (uname == null || uname == '') {
			layer.tips("请输入账号", "#uname", {
				tips : [ 2, 'red' ]
			});

		} else if (upsw == null || upsw == '') {
			layer.tips("请输入密码", "#upsw", {
				tips : [ 2, 'red' ]
			});

		} else {
			$.ajax({
				url : '../login/tologin',
				data : {
					'uname' : uname,
					'upsw' : upsw
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if(data==1){
						//跳转到main界面
						window.location.href = '../user/toMain';
					}else{
						//弹出提示框显示账号密码错误
						layer.msg('账号密码错误');
						return;
					}
				}
			})
		}
	})
}