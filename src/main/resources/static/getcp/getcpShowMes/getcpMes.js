var fid;
$(function() {
	// 获取fid
	fid = $("#fid").val();
	// 修改下拉列表的状态
	getStatus();
	// 创建表格
	getAll(fid);
	// 外置增加
	myAdd();
	// 返回上一级
	down();

})

function getStatus() {
	$("#getcp").addClass("active");
	$("#ck").addClass("open");
}

function getAll(obj) {
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../getcp/findAll?fid=' + obj, // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		height : $(window).height() - 300,
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		height : 530,
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "gid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			field : 'gid',
			align : 'center',
			title : '产品编号',
			halign : 'center'
		}, {
			field : 'fname',
			align : 'center',
			title : '产品名称',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'cname',
			align : 'center',
			title : '产品类别',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'gcount',
			align : 'center',
			title : '入库数量',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'gtime',
			align : 'center',
			title : '入库时间',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'urealname',
			align : 'center',
			title : '经办人',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		} ]
	});
}

function myAdd() {
	$("#btn_add").click(function() {
		layer.open({
			type : 2,
			title : '商品入库',
			shadeClose : true,
			shade : 0.8,
			area : [ '70%', '100%' ],
			content : "../getcp/toInsert?fid=" + fid,
			skin : 'blue_base',
			end : function(index) {
				$("#tab").bootstrapTable('refresh');
			}
		});
	})
}

function down() {
	$("#down").click(function() {
		window.location.href = '../getcp/toGetcp';
	})
}
