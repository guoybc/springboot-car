$(function() {

	getFocusGcount();
	getAdd();

})
// 聚焦
function getFocusGcount() {
	$("#gcount").focus();
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var fid = $("#fid").val();
		var gcount = $("#gcount").val();
		if (gcount.length == 0) {
			layer.tips("请输入入库数量", "#gcount", {
				tips : [ 2, 'red' ]
			});
		} else if (gcount == 0) {
			layer.tips("入库数量不能为0", "#gcount", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajax({
					url : '../getcp/insert',
					data : {
						'fid':fid,
						'gcount':gcount
					},
					datatype : 'json',
					type : 'post',
					success : function(data) {
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('增加成功！', {
							icon : 1
						});
					}
				})
			}, function(index) {
				layer.close(index);
			})
		}
	})
}
