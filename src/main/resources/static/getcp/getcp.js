$(function() {
	// 修改下拉列表的状态
	getStatus();
	// 创建表格
	getAll();
	
})

function getStatus() {
	$("#getcp").addClass("active");
	$("#ck").addClass("open");
}

function getAll() {
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../mytf/findAll', // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		height : $(window).height() - 300,
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		height : 530,
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "fid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			field : 'fid',
			align : 'center',
			title : '产品编号',
			halign : 'center'
		}, {
			field : 'fname',
			align : 'center',
			title : '产品名称',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'cname',
			align : 'center',
			title : '产品类别',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'fimg',
			align : 'center',
			title : '产品图片',
			formatter : function(value, row, index) {
				var e = '<span class="xname"><a href="javascript:void(0)" class="green"  onclick="showimg(\'../' + row.fimg + '\')"><i class="ace-icon glyphicon glyphicon-picture"></i>照片详情</a>';
				return e;
			}
		}, {
			field : 'faddress',
			align : 'center',
			title : '产地',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'foutprice',
			align : 'center',
			title : '出售价',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'finprice',
			align : 'center',
			title : '进货价',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'fcount',
			align : 'center',
			title : '产品数量',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + row.fcount+row.fdw+ '</span>';
				return e;
			}
		}, {
			title : '入库操作',
			field : 'fid',
			align : 'center',
			formatter : function(value, row, index) {

				var e = '<a href="javascript:void(0)" class="blue"  onclick="getcp(\'' + row.fid + '\')"><i class="ace-icon fa fa-globe bigger-130"></i>入库记录</a>&nbsp;&nbsp; ';
				return e;
			}
		}, {
			title : '出库操作',
			field : 'fid',
			align : 'center',
			formatter : function(value, row, index) {

				var e = '<a href="javascript:void(0)" class="blue"  onclick="outcp(\'' + row.fid + '\')"><i class="ace-icon fa fa-globe bigger-130"></i>出库记录</a>&nbsp;&nbsp; ';
				return e;
			}
		} ]
	});
}

function getcp(obj) {
	window.location.href = "../getcp/toShowMes?fid="+obj;
}

function outcp(obj){
	window.location.href = "../getcp/toShowOutMes?fid="+obj;
}

function showimg(src) {
	if (src || src == "") {
		layer.msg("没有发现图片！", {
			icon : 6,
			time : 400
		});
	}
	var img = new Image();
	img.onload = function() {// 避免图片还未加载完成无法获取到图片的大小。
		// 避免图片太大，导致弹出展示超出了网页显示访问，所以图片大于浏览器时下窗口可视区域时，进行等比例缩小。
		var max_height = $(window).height() - 100;
		var max_width = $(window).width();

		// rate1，rate2，rate3 三个比例中取最小的。
		var rate1 = max_height / img.height;
		var rate2 = max_width / img.width;
		var rate3 = 1;
		var rate = Math.min(rate1, rate2, rate3);
		// 等比例缩放
		var imgHeight = img.height * rate; // 获取图片高度
		var imgWidth = img.width * rate; // 获取图片宽度

		var imgHtml = "<img src='" + src + "' width='" + imgWidth + "px' height='" + imgHeight + "px'/>";
		// 弹出层
		layer.open({
			type : 1,
			title : '产品照片',// 不显示标题
			closeBtn : 0,
			area : [ 'auto', 'auto' ],
			skin : 'layui-layer-nobg', // 没有背景色
			shadeClose : true,
			content : imgHtml
		});
	}
	img.src = src;
}
