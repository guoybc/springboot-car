$(function() {
	getFocusAname();

	getBlurAname();

	getAdd();
})
// 聚焦
function getFocusAname() {
	$("#aname").focus();
}

// 失去焦点
function getBlurAname() {
	$("#aname").blur(function() {
		var aname = $(this).val();
		if (aname.length !== 0) {
			$.ajax({
				url : '../cartype/findOneByAname',
				data : {
					'aname' : aname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#aname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getAnameExit() {
	var aname = $(this).val();
	if (aname.length !== 0) {
		$.ajax({
			url : '../cartype/findOneByAname',
			data : {
				'aname' : aname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#aname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var aname = $("#aname").val();
		if (aname == null || aname == '') {
			layer.tips("当前名称为空", "#aname", {
				tips : [ 2, 'red' ]
			});

		} else {
			// 判断是否有相同的名称
			$.post("../cartype/findOneByAname", {
				'aname' : aname
			}, function(data) {
				if (data === 1) {
					layer.tips("部门名称已经存在", "#aname", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../cartype/findOneByAname',
						data : {
							'aname' : aname
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前名称已存在", "#aname", {
									tips : [ 1, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../cartype/insert", {
										'aname' : aname
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}