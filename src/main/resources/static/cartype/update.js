$(function() {
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var aid = $("#aid").val();
		var aname = $("#aname").val();
		if (aname == null || aname == '') {	
			layer.tips("当前名称为空", "#aname", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../cartype/update", {
					'aid' : aid,
					'aname' : aname
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}