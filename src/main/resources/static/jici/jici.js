$(function() {
	// 修改下拉列表的状态
	getStatus();

	// 查询所有信息;
	getAllSk();

	getShop();
})
function getStatus() {
	$("#jici").addClass("active");
	$("#xf").addClass("open");
}

function getAllSk() {
	// 查询所有待购物的商品数目
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../jici/findAll', // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		height : 530,
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "jid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			field : 'jid',
			align : 'center',
			title : '编号',
			halign : 'center'
		}, {
			field : 'rcard',
			align : 'center',
			title : '会员卡号',
			halign : 'center'
		}, {
			field : 'rname',
			align : 'center',
			title : '会员名称',
			halign : 'center'
		}, {
			field : 'jtime',
			align : 'center',
			title : '消费日期',
			halign : 'center'
		}, {
			field : 'sname',
			align : 'center',
			title : '消费类型',
			halign : 'center'
		}, {
			field : 'jmoney',
			align : 'center',
			title : '消费金额(元)',
			halign : 'center'
		}, {
			field : 'urealname',
			align : 'center',
			title : '经办人',
			halign : 'center'
		} ]
	});
}

// 购买商品
function getShop() {
	$("#btn_add_shop").click(function() {
		layer.open({
			type : 2,
			title : '购买商品',
			shadeClose : true,
			shade : 0.8,
			area : [ '70%', '90%' ],
			content : "../jici/toInsert",
			skin : 'blue_base',
			end : function(index) {
				$("#tab").bootstrapTable('refresh');
			}
		});
	})
}
