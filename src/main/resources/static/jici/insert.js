var reg_money = /^([1-9]\d{0,9}|0)([.]?|(\.\d{1,2})?)$/
var rcard;
var rmoney;
$(function() {
	getFocusRcard();

	getBlurRcard();

	getAllServicetype();

	getAdd()
})

function getFocusRcard() {
	$("#rcard").focus();
}

function getBlurRcard() {
	$("#rcard").blur(function() {
		rcard = $(this).val();
		findOneByRcard(rcard);
	})
}

function findOneByRcard(obj) {
	if (obj.length == 0 || obj == null) {
		layer.tips("请输入会员卡号", "#rcard", {
			tips : [ 2, 'red' ]
		});
	} else {
		$.ajax({
			url : '../member/findOneByRcard',
			data : {
				'rcard' : obj
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data == 0) {
					layer.tips("该会员卡有误请重新输入!", "#rcard", {
						tips : [ 2, 'red' ]
					});
				} else {
					// 修改状态
					$(".s_name").show();
					$("#rname").val(data.rname);
					$("#rmoney").val(data.rmoney);
					$("#rid").val(data.rid);
					rmoney = data.rmoney;
				}
			}
		})
	}
}

function getAllServicetype() {
	$("#sid").empty();
	$("#sid").append("<option value='0'>---请选择产品类型---</option>");
	$.ajax({
		url : '../servicetype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#sid").append("<option value=" + x.sid + " >" + x.sname + "</option>");
			})
		}
	})
}

function getAdd() {
	$("#add").click(function() {
		var rid = $("#rid").val();
		var sid = $("#sid").val();
		var jmoney = $("#jmoney").val();
		if (rid.length == 0 || rid == null) {
			layer.tips("当前会员卡号输入有误", "#rcard", {
				tips : [ 2, 'red' ]
			});
		} else if (sid == 0) {
			layer.tips("请选择服务类型", "#sid", {
				tips : [ 2, 'red' ]
			});
		} else if (jmoney.length == 0 || jmoney == '') {
			layer.tips("请输入消费金额", "#jmoney", {
				tips : [ 2, 'red' ]
			});
		} else if (!reg_money.test(jmoney)) {
			layer.tips("请输入消费金额格式错误", "#jmoney", {
				tips : [ 2, 'red' ]
			});
		} else if (jmoney > rmoney) {
			layer.msg('余额不足请充值!', {
				icon : 5
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajax({
					url : '../jici/insert',
					data : {
						'rid' : rid,
						'sid' : sid,
						'jmoney' : jmoney
					},
					datatype : 'json',
					type : 'post',
					success : function(data) {
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('增加成功！', {
							icon : 1
						});
					}
				})
			}, function(index) {
				layer.close(index);
			})
		}
	})
}