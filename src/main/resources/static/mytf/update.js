var old_cid;
var fid;
$(function() {
	fid = $("#fid").val();
	old_cid = $("#f_cid").val();
	
	// 点击修改按钮
	getUpdate();

	// 获取所有的产品类型
	getAllCptype();
})

function getAllCptype() {
	$("#cid").empty();
	$.ajax({
		url : '../cptype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#cid").append("<option value=" + x.cid + " >" + x.cname + "</option>");
			})
			$("#cid").val(old_cid);
		}
	})
}

function getUpdate() {
	$("#update").click(function() {
		var fname = $("#fname").val();
		var cid = $("#f_cid").val();
		var fdw = $("#fdw").val();
		var faddress = $("#faddress").val();
		var foutprice = $("#foutprice").val();
		var finprice = $("#finprice").val();
		var fileimg = $("#fileimg").val();
		if (fname.length == 0) {
			layer.tips("请输入会产品名称", "#fname", {
				tips : [ 2, 'red' ]
			});
		} else if (cid == 0) {
			layer.tips("请选择商品类型", "#cid", {
				tips : [ 2, 'red' ]
			});
		} else if (fdw == 0) {
			layer.tips("请选择商品单位", "#fdw", {
				tips : [ 2, 'red' ]
			});
		} else if (cid.length == 0) {
			layer.tips("请输入商品产地", "#faddress", {
				tips : [ 2, 'red' ]
			});
		} else if (foutprice.length == 0) {
			layer.tips("请输入出售价格", "#foutprice", {
				tips : [ 2, 'red' ]
			});
		} else if (finprice.length == 0) {
			layer.tips("请输入进货价格", "#finprice", {
				tips : [ 2, 'red' ]
			});
		} else if (foutprice < finprice) {
			alert("foutprice:" + foutprice + "  " + "finprice:" + finprice);
			layer.tips("进货价要小于售价", "#foutprice", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajaxFileUpload({
					url : '../mytf/update', // 用于文件上传的服务器端请求地址
					secureuri : false, // 是否需要安全协议，一般设置为false
					fileElementId : [ 'fileimg' ], // 文件上传域的ID
					data : {
						'fid':fid,
						'fname' : fname,
						'cid' : cid,
						'fdw' : fdw,
						'faddress' : faddress,
						'foutprice' : foutprice,
						'finprice' : finprice,
						'fileimg' : fileimg
					}, // 此参数非常严谨，写错一个引号都不行
					dataType : 'json', // 返回值类型 一般设置为json
					type : 'post',
					success : function(data, status) {
						//
					},
					error : function(data, status, e) {
						var index = parent.layer.getFrameIndex(window.name);
						// 刷新父页面
						window.parent.location.reload();
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 1
						});
					}
				});
			}, function(index) {
				layer.close(index);
			})
		}
	})
}