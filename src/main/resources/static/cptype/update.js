$(function() {
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var cid = $("#cid").val();
		var cname = $("#cname").val();
		if (cname == null || cname == '') {
			layer.tips("当前名称为空", "#cname", {
				tips : [ 2, 'red' ]
			});
		}else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../cptype/update", {
					'cid' : cid,
					'cname' : cname
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}