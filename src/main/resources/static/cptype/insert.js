$(function() {
	getFocusCname();

	getBlurCname();

	getAdd();
})
// 聚焦
function getFocusCname() {
	$("#cname").focus();
}

// 失去焦点
function getBlurCname() {
	$("#cname").blur(function() {
		var cname = $(this).val();
		if (cname.length !== 0) {
			$.ajax({
				url : '../cptype/findOneByCname',
				data : {
					'cname' : cname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#cname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getCnameExit() {
	var cname = $(this).val();
	if (cname.length !== 0) {
		$.ajax({
			url : '../cptype/findOneByCname',
			data : {
				'cname' : cname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#cname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var cname = $("#cname").val();
		if (cname == null || cname == '') {
			layer.tips("当前名称为空", "#cname", {
				tips : [ 2, 'red' ]
			});

		} else {
			// 判断是否有相同的名称
			$.post("../cptype/findOneByCname", {
				'cname' : cname
			}, function(data) {
				if (data === 1) {
					layer.tips("产品名称已经存在", "#cname", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../cptype/findOneByCname',
						data : {
							'cname' : cname
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前名称已存在", "#cname", {
									tips : [ 1, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../cptype/insert", {
										'cname' : cname
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}