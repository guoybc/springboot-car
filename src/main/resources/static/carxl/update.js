var old_aid;
$(function() {
	old_aid = $("#old_aid").val();
	
	getAllCartype();
	
	
	getUpdate();
})

// 查询所有的汽车品牌并选中
function getAllCartype() {
	$("#aid").empty();
	$.ajax({
		url : '../cartype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#aid").append("<option value=" + x.aid + " >" + x.aname + "</option>");
			})
			$("#aid").val(old_aid);
		}
	})
}

function getUpdate() {
	$("#update").click(function() {
		var aid = $("#aid").val();
		var xid = $("#xid").val();
		var xname = $("#xname").val();
		if (xname == null || xname == '') {
			layer.tips("当前名称为空", "#xname", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../carxl/update", {
					'xid' : xid,
					'aid' : aid,
					'xname' : xname
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}