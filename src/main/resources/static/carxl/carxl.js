$(function() {
	// 修改下拉列表的状态
	getStatus();
	// 创建表格
	getAll();
	// 外置delete
	mydel();
	// 外置增加
	myAdd();
	// 外置修改
	myupp();
})

function getStatus() {
	$("#carxl").addClass("active");
	$("#system").addClass("open");
}

function getAll() {
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../carxl/findAll', // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		height : $(window).height() - 300,
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		height : 530,
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "xid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			checkbox : true
		}, {
			field : 'xid',
			align : 'center',
			title : '产品编号',
			halign : 'center'
		}, {
			field : 'aname',
			align : 'center',
			title : '汽车品牌',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		},{
			field : 'xname',
			align : 'center',
			title : '汽车系列',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			title : '操作',
			field : 'xid',
			align : 'center',
			formatter : function(value, row, index) {

				var e = '<a href="#" class="green"  onclick="edit(\'' + row.xid + '\')"><i class="ace-icon fa fa-pencil bigger-130"></i></a>&nbsp;&nbsp; ';
				var d = '<a href="#" class="red" onclick="del(\'' + row.xid + '\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a> ';
				return e + d;
			}
		} ]
	});
}

function mydel() {
	$("#btn_delete").click(function() {
		// 得到你要修改的记录的编号(数组)！
		var tt = $.map($('#tab').bootstrapTable('getSelections'), function(row) {
			return row.xid;
		});
		if (tt.length == 0) {
			layer.tips("请最少选择一个要删除的对象！", "#btn_delete", {
				tips : [ 1, 'red' ]
			});
		} else {
			layer.confirm('确定删除吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				var ar = '';
				for (var i = 0; i < tt.length; i++) {
					ar += tt[i] + "&";
				}
				$.ajax({
					url : '../carxl/delSome',
					type : 'post',
					datatype : 'json',
					data : {
						'ar' : ar
					},
					success : function(data) {
						// 刷新当前页面
						$("#tab").bootstrapTable('refresh');
					}
				})
				layer.close(index);
				layer.msg('删除成功', {
					icon : 1,
					time : 1000
				});
			}, function(index) {
				layer.close(index);
				layer.msg('取消删除', {
					icon : 1,
					time : 1000
				});
			})
		}

	});
}

function myAdd() {
	$("#btn_add").click(function() {
		layer.open({
			type : 2,
			title : '增加产品',
			shadeClose : true,
			shade : 0.8,
			area : [ '70%', '65%' ],
			content : "../carxl/toInsert",
			skin : 'blue_base',
			end : function(index) {
				$("#tab").bootstrapTable('refresh');
			}
		});
	});
}
// 内置的修改
function edit(obj) {
	layer.open({
		type : 2,
		title : '修改会员类型',
		shadeClose : true,
		shade : 0.8,
		area : [ '70%', '65%' ],
		content : "../carxl/toUpdate?xid=" + obj,
		skin : 'blue_base',
		anim : 0,
		end : function(index) {
			$("#tab").bootstrapTable('refresh');
		}
	});
}
// 内置删除
function del(obj) {
	layer.confirm('确定删除吗?', {
		btn : [ '确定', '取消' ]
	// 按钮
	}, function(index) {
		$.post('../carxl/delOne', {
			'xid' : obj
		}, function(data) {
			if (data === 1) {
				layer.close(index);
				layer.msg('删除成功', {
					icon : 1,
					time : 1000
				});
				$("#tab").bootstrapTable('refresh');
			}
		})
	}, function(index) {
		layer.close(index);
	})
}

// 外置修改
function myupp() {
	$("#btn_update").click(function() {
		// 得到你要修改的记录的编号(数组)！
		var tt = $.map($('#tab').bootstrapTable('getSelections'), function(row) {
			return row.xid;
		});
		if (tt.length == 0) {
			layer.tips("请选择一个要修改的对象！", "#btn_update", {
				tips : [ 1, 'red' ]
			});
		} else if (tt.length > 1) {

			layer.tips("一次只能修改一个对象！", "#btn_update", {
				tips : [ 1, 'red' ]
			});
		} else {
			edit(tt[0]);
		}

	});
}
