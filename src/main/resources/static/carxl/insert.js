$(function() {
	getAllCartype()

	getAdd();
})
// 查询所有的汽车品牌
function getAllCartype() {
	$("#aid").empty();
	$("#aid").append("<option value='0'>---请选择汽车品牌---</option>");
	$.ajax({
		url : '../cartype/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#aid").append("<option value=" + x.aid + " >" + x.aname + "</option>");
			})
		}
	})
}

// 判断名字是否存在
function getXnameExit() {
	var xname = $(this).val();
	if (xname.length !== 0) {
		$.ajax({
			url : '../carxl/findOneByXname',
			data : {
				'xname' : xname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#xname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var xname = $("#xname").val();
		var aid = $("#aid").val();
		if (xname == null || xname == '') {
			layer.tips("当前名称为空", "#xname", {
				tips : [ 2, 'red' ]
			});
		} else if (aid == 0) {
			layer.tips("请选择汽车品牌", "#aid", {
				tips : [ 2, 'red' ]
			});
		} else {
			// 判断是否有相同的名称
			$.post("../carxl/findOneByXname", {
				'xname' : xname
			}, function(data) {
				if (data == 1) {
					layer.tips("该系列名称已经存在", "#xname", {
						tips : [ 2, 'red' ]
					});
				} else {
					layer.confirm('确定增加吗?', {
						btn : [ '确定', '取消' ]
					// 按钮
					}, function(index) {
						$.post("../carxl/insert", {
							'aid' : aid,
							'xname' : xname
						}, function(data) {
							// 关闭当前弹窗
							var index = parent.layer.getFrameIndex(window.name);
							parent.layer.close(index);
							parent.layer.msg('增加成功！', {
								icon : 1
							});
						})
					}, function(index) {
						layer.close(index);
					})
				}
			})
		}
	})
}