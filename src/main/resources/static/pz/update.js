$(function() {
	getUpdate();
})

function getUpdate() {
	$("#update").click(function() {
		var zid = $("#zid").val();
		var zname = $("#zname").val();
		if (zname == null || zname == '') {
			layer.tips("当前名称为空", "#zname", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定修改吗?', {
				btn : [ '确定', '取消' ]
			}, function(index) {
				$.post("../pz/update", {
					'zid' : zid,
					'zname' : zname
				}, function(data) {
					// 关闭当前弹窗
					if (data === 1) {
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('修改成功！', {
							icon : 6,
							time : 1000
						});
					}
				})
			})
		}
	})
}