$(function() {
	getFocusZname();

	getBlurZname();

	getAdd();
})
// 聚焦
function getFocusZname() {
	$("#zname").focus();
}

// 失去焦点
function getBlurZname() {
	$("#zname").blur(function() {
		var zname = $(this).val();
		if (zname.length !== 0) {
			$.ajax({
				url : '../pz/findOneByZname',
				data : {
					'zname' : zname
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data === 1) {
						layer.tips("当前名称已存在", "#zname", {
							tips : [ 1, 'red' ]
						});
					}
				}
			})
		}
	})
}
// 判断名字是否存在
function getZnameExit() {
	var zname = $(this).val();
	if (zname.length !== 0) {
		$.ajax({
			url : '../pz/findOneByZname',
			data : {
				'zname' : zname
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				if (data === 1) {
					layer.tips("当前名称已存在", "#zname", {
						tips : [ 1, 'red' ]
					});
				}
			}
		})
	}
}

// 增加
function getAdd() {
	$("#add").click(function() {
		var zname = $("#zname").val();
		if (zname == null || zname == '') {
			layer.tips("当前名称为空", "#zname", {
				tips : [ 2, 'red' ]
			});

		} else {
			// 判断是否有相同的名称
			$.post("../pz/findOneByZname", {
				'zname' : zname
			}, function(data) {
				if (data === 1) {
					layer.tips("部门名称已经存在", "#zname", {
						tips : [ 2, 'red' ]
					});
				} else {
					$.ajax({
						url : '../pz/findOneByZname',
						data : {
							'zname' : zname
						},
						datatype : 'json',
						type : 'post',
						success : function(data) {
							if (data === 1) {
								layer.tips("当前名称已存在", "#zname", {
									tips : [ 1, 'red' ]
								});
							} else {
								layer.confirm('确定增加吗?', {
									btn : [ '确定', '取消' ]
								// 按钮
								}, function(index) {
									$.post("../pz/insert", {
										'zname' : zname
									}, function(data) {
										// 关闭当前弹窗
										var index = parent.layer.getFrameIndex(window.name);
										parent.layer.close(index);
										parent.layer.msg('增加成功！', {
											icon : 1
										});
									})
								}, function(index) {
									layer.close(index);
								})
							}
						}
					})
				}
			})
		}
	})
}