var rid;
var osmoney;
// 充值金额
var omoney;
// 优惠活动最低金额
var ylessmoney;
// 最终金额;
var olastmoney;
// 优惠活动yid
var y_yid;
// 最终金额
var olastmoney;
$(function() {

	getFocusRcard();

	getBlurSname();

	// 优惠活动修改了
	getChangeY();

	// 增加
	getAdd();

})
// 聚焦
function getFocusRcard() {
	$("#rcard_search").focus();
}

function getBlurSname() {
	$("#search").click(function() {
		var rcard = $("#rcard_search").val();
		if (rcard.length == 0) {
			layer.tips("请输入会员卡卡号", "#rcard_search", {
				tips : [ 1, 'red' ],
				time : 2000
			});
		} else {
			$.ajax({
				url : '../member/findOneByRcard',
				data : {
					'rcard' : rcard
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data == 0) {
						layer.tips("卡号不存在请重新输入", "#rcard_search", {
							tips : [ 1, 'red' ]
						});
						$("#form2").css("display", "none");
						$("#form3").css("display", "none");
					} else {
						// 显示下面的信息框
						showMes(data);
					}
				}
			})
		}
	})
}

function showMes(obj) {
	// 信息查看框
	addMes(obj);
	// 充值增加框
	addMoney();
	// 写入会员名
	$("#rcard1").val(obj.rname);
	// 充值金额失去焦点
	getBlueOmoney();
}

function addMes(obj) {
	$("#form2").css("display", "block");
	$('#form2:input').val('');
	$("#rremark").val('');
	$('#form2').find('input,textarea,select').attr('disabled', true);
	rid = obj.rid;
	var rcard = obj.rcard;
	var rname = obj.rname;
	var rpsw = obj.rpsw;
	var rtel = obj.rtel;
	var rsex = obj.rsex;
	var rbirthday = obj.rbirthday;
	var rcarnum = obj.rcarnum;
	var rcolor = obj.rcolor;
	var rway = obj.rway;
	var rnum = obj.rnum;
	var raddress = obj.raddress;
	var rremark = obj.rremark;
	var did = obj.dname;
	var xid = obj.xname;
	var zid = obj.zname;
	var aid = obj.aname;
	var rmoney = obj.rmoney;
	var rjf = obj.rjf;
	$("#aid").val(aid);
	$("#xid").val(xid);
	$("#did").val(did);
	$("#zid").val(zid);
	$("#rcard").val(rcard);
	$("#rname").val(obj.rname);
	$("#rpsw").val(obj.rpsw);
	$("#rtel").val(obj.rtel);
	$("#rsex").val(obj.rsex);
	$("#rbirthday").val(obj.rbirthday);
	$("#rcarnum").val(obj.rcarnum);
	$("#rcolor").val(obj.rcolor);
	$("#rway").val(obj.rway);
	$("#rnum").val(obj.rnum);
	$("#raddress").val(obj.raddress);
	$("#rremark").val(obj.rremark);
	$("#rmoney").val(rmoney);
	$("#rjf").val(rjf);
}

function addMoney() {
	$("#form3").css("display", "block");
	// 查询所有的优惠活动
	getSelectY();

}

/*
 * 
 * 获取所有的优惠活动信息
 * 
 */
function getSelectY() {
	$("#c_yid").empty();
	$("#c_yid").append("<option value='0'>---请选择优惠活动---</option>");
	$.ajax({
		url : '../youhui/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#c_yid").append("<option value=" + x.yid + " >" + x.ytitle + "</option>");
			})
		}
	})
}
function getChangeY() {
	$("body").on("change", "#c_yid", function() {
		var yid = $(this).val();
		y_yid = yid;
		// 发送数据
		$.ajax({
			url : '../youhui/findOneByYid',
			data : {
				"yid" : yid
			},
			datatype : 'json',
			type : 'post',
			success : function(data) {
				osmoney = data.ymoney;
				ylessmoney = data.ylessmoney;
				$("#osmoney").val(osmoney);
				$("#omoney").attr("placeholder", "优惠最低金额:" + ylessmoney);
			}
		})
	})
}

function getBlueOmoney() {
	$("#omoney").blur(function() {
		var o_omoney = $(this).val();
		if (o_omoney.length != 0) {
			if (o_omoney >= ylessmoney) {
				// 进行合计
				alert("o_omoney:"+o_omoney + " " + osmoney);
				olastmoney = Number(o_omoney) + Number(osmoney);
				$("#olastmoney").val(olastmoney);
			} else {
				layer.tips("充值金额不能小于活动最低金额:" + ylessmoney, "#omoney", {
					tips : [ 1, 'red' ]
				});
			}
		} else {
			layer.tips("请输入充值金额", "#omoney", {
				tips : [ 1, 'red' ]
			});
		}
	})
}

function getAdd() {
	$("#addO").click(function() {
		var omoney = $("#omoney").val();
		var oremark = $("#oremark").val();
		if (omoney.length == 0) {
			layer.tips("请输入充值金额", "#omoney", {
				tips : [ 1, 'red' ]
			});
		} else if (oremark.length == 0) {
			layer.tips("请输入充值备注", "#oremark", {
				tips : [ 1, 'red' ]
			});
		} else if (omoney < osmoney) {
			layer.tips("充值金额不能小于活动最低金额:" + ylessmoney, "#omoney", {
				tips : [ 1, 'red' ]
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajax({
					url : '../chong/insert',
					data : {
						"rid" : rid,
						"osmoney" : osmoney,
						"omoney" : omoney,
						"yid" : y_yid,
						"olastmoney" : olastmoney,
						"oremark" : oremark
					},
					datatype : 'json',
					type : 'post',
					success : function(data) {
						if (data == 1) {
							var index = parent.layer.getFrameIndex(window.name);
							parent.layer.close(index);
							parent.layer.msg('增加成功！', {
								icon : 1
							});
						}
					}
				})
			}, function(index) {
				layer.close(index);
			})
		}
	})
}
