$(function() {
	// 修改下拉列表的状态
	getStatus();
	// 创建表格
	getAll();
	// 外置增加
	myAdd();
})

function getStatus() {
	$("#chong").addClass("active");
	$("#huiyuan").addClass("open");
}

function getAll() {
	$("#tab").empty();
	$("#tab").bootstrapTable({
		url : '../chong/findAll', // 请求后台的URL（*）
		method : 'get', // 请求方式（*）
		dataType : 'json',
		toolbar : '#toolbar', // 工具按钮用哪个容器
		striped : true, // 是否显示行间隔色
		pagination : true, // 是否显示分页（*）
		queryParams : function(params) {
			var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
				pages : params.limit, // 页面大小(每一页要显示多少条)
				begin : params.offset
			// 从数据库第几条记录开始

			};
			return temp;
		},// 传递参数（*）
		sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
		height : 530,
		pageNumber : 1, // 初始化加载第一页，默认第一页
		pageSize : 10, // 每页的记录行数（*）
		pageList : [ 10, 25, 50, 100 ], // 可供选择的每页的行数（*）
		showColumns : true, // 是否显示所有的列
		showRefresh : true, // 是否显示刷新按钮
		minimumCountColumns : 1, // 最少允许的列数
		clickToSelect : true, // 是否启用点击选中行
		uniqueId : "oid", // 每一行的唯一标识，一般为主键列
		showToggle : true, // 是否显示详细视图和列表视图的切换按钮
		cardView : false, // 是否显示详细视图
		detailView : false, // 是否显示父子表
		columns : [ {
			field : 'oid',
			align : 'center',
			title : '编号',
			halign : 'center'
		}, {
			field : 'rcard',
			align : 'center',
			title : '会员卡号',
			halign : 'center'
		}, {
			field : 'omoney',
			align : 'center',
			title : '原始充值金额(元)',
			halign : 'center'
		}, {
			field : 'ytitle',
			align : 'center',
			title : '优惠内容',
			halign : 'center'
		}, {
			field : 'osmoney',
			align : 'center',
			title : '赠送金额(元)',
			halign : 'center'
		}, {
			field : 'olastmoney',
			align : 'center',
			title : '优惠后的金额',
			halign : 'center'
		}, {
			field : 'otime',
			align : 'center',
			title : '充值时间',
			formatter : function(value, row, index) {
				var e = '<span class="xname">' + value + '</span>';
				return e;
			}
		}, {
			field : 'urealname',
			align : 'center',
			title : '经办人',
			halign : 'center'
		}, {
			field : 'oremark',
			align : 'center',
			title : '备注',
			halign : 'center'
		} ]
	});
}

function myAdd() {
	$("#btn_add").click(function() {
		layer.open({
			type : 2,
			title : '充值',
			shadeClose : true,
			shade : 0.8,
			area : [ '70%', '90%' ],
			content : "../chong/toInsert",
			skin : 'blue_base',
			end : function(index) {
				$("#tab").bootstrapTable('refresh');
			}
		});
	});
}
