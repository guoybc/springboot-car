var rid;
var rjf;
var nncount;
var njf;

$(function() {

	getFocusRcard();

	getBlurRcard();

	getAlllipin();

	getLipinChange();

	getRrjf();

	getAdd();
})
function getRrjf() {

	$("#hcount").blur(function() {
		var hcount = $("#hcount").val();
		if (rjf == null) {
			layer.tips("请输入会员卡号后再操作!", "#rcard", {
				tips : [ 2, 'red' ]
			});
		} else {
			// 计算积分
			rrjf = rjf - hcount * njf;
			if (rrjf < 0) {
				layer.tips("积分不足,请修改数量!", "#rrjf", {
					tips : [ 2, 'red' ]
				});
				$("#rrjf").val(0);
			} else {
				$("#rrjf").val(rrjf);
			}
		}
	})

}

function getFocusRcard() {
	$("#rcard").focus();
}

function getBlurRcard() {
	$("#rcard").blur(function() {
		var rcard = $(this).val();
		if (rcard.length == 0 || rcard == '') {
			layer.tips("请输入会员卡号", "#rcard", {
				tips : [ 2, 'red' ]
			});
		} else {
			$.ajax({
				url : '../member/findOneByRcard',
				data : {
					"rcard" : rcard
				},
				datatype : 'json',
				type : 'post',
				success : function(data) {
					if (data == 0) {
						layer.tips("输入卡号有误,请重新输入", "#rcard", {
							tips : [ 2, 'red' ]
						});
						$(".rname").hide();
					} else {
						showMemberMes(data);
					}
				}
			})
		}
	});
}
function showMemberMes(obj) {
	$(".rname").show();
	rid = obj.rid;
	rjf = obj.rjf;
	$("#rname").val(obj.rname);
	$("#rjf").val(obj.rjf);
}

function getAlllipin() {
	$("#nid").empty();
	$("#nid").append("<option value='0'>---请选择兑换礼品---</option>");
	$.ajax({
		url : '../lipin/findAllNoPager',
		data : "",
		datatype : 'json',
		type : 'post',
		success : function(data) {
			$.each(data, function(index, x) {
				$("#nid").append("<option value=" + x.nid + " >" + x.nname + "</option>");
			})
		}
	})
}

function getLipinChange() {
	$("body").on("change", '#nid', function() {
		nid = $(this).val();
		$.ajax({
			url : '../lipin/findOneByNid?nid=' + nid,
			data : "",
			datatype : 'json',
			type : 'post',
			success : function(data) {
				$(".jf").show();
				nncount = data.nncount;
				njf = data.njf;
				$("#nncount").val(nncount);
				$("#njf").val(njf);
			}
		})
	})
}

function getAdd() {
	$("#add").click(function() {
		var rcard = $("#rcard").val();
		var nid = $("#nid").val();
		var hcount = $("#hcount").val();
		var rrjf = $("#rrjf").val();
		if (rcard.length == 0 || rcard == '') {
			layer.tips("请输入会员卡号", "#rcard", {
				tips : [ 2, 'red' ]
			});

		} else if (hcount > nncount) {
			layer.tips("库存不足", "#nncount", {
				tips : [ 2, 'red' ]
			});
		} else if (hcount * njf > rjf) {
			layer.tips("积分不足", "#rjf", {
				tips : [ 2, 'red' ]
			});
		} else if (rrjf == 0) {
			layer.tips("积分不足", "#rrjf", {
				tips : [ 2, 'red' ]
			});
		} else {
			layer.confirm('确定增加吗?', {
				btn : [ '确定', '取消' ]
			// 按钮
			}, function(index) {
				$.ajax({
					url : '../duihuan/insert',
					data : {
						'nid' : nid,
						'rid' : rid,
						'rrjf' : rrjf,
						'hcount':hcount
					},
					datatype : 'json',
					type : 'post',
					success : function(data) {
						// 关闭当前弹窗
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
						parent.layer.msg('增加成功！', {
							icon : 1
						});
					}
				})
			}, function(index) {
				layer.close(index);
			})
		}

	})
}